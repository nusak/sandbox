package main

import (
	"fmt"

	"github.com/asaskevich/govalidator"
)

func main() {
	creditcard := "4520340057868559"
	if govalidator.IsCreditCard(creditcard) {
		fmt.Println("valid")
		return
	}
	fmt.Println("invalid")
}
