package main

import (
	"fmt"

	"github.com/vishvananda/netlink"
)

func main() {
	links, err := netlink.LinkList()
	if err != nil {
		panic(err)
	}
	for _, link := range links {
		fmt.Println(link.Attrs().Name, link.Type())
		addrs, err := netlink.AddrList(link, netlink.FAMILY_V4)
		if err != nil {
			panic(err)
		}
		for _, addr := range addrs {
			fmt.Println(" ", addr.IPNet)
		}
	}
}
