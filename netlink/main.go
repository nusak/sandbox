package main

import (
	"fmt"
	"log"
	"net"
	"time"

	"github.com/vishvananda/netlink"
	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

type Wireguard struct {
	Name    string
	MTU     int
	Address netlink.Addr
	Config  wgtypes.Config
	Routes  []netlink.Route
}

func (w Wireguard) Attrs() *netlink.LinkAttrs {
	attr := netlink.NewLinkAttrs()
	attr.Name = w.Name
	attr.MTU = w.MTU
	return &attr
}

func (w Wireguard) Type() string {
	return "wireguard"
}

func (w Wireguard) ApplyConfig() error {
	wg, err := wgctrl.New()
	if err != nil {
		return err
	}
	defer wg.Close()
	return wg.ConfigureDevice(w.Name, w.Config)
}

func (w Wireguard) Up() error {
	if err := netlink.LinkAdd(w); err != nil {
		return fmt.Errorf("link add %v", err)
	}
	if err := netlink.AddrAdd(w, &w.Address); err != nil {
		return fmt.Errorf("add address %v", err)
	}
	if err := netlink.LinkSetUp(w); err != nil {
		return fmt.Errorf("link set up %v", err)
	}
	return w.ApplyConfig()
}

func main() {
	key, err := wgtypes.GeneratePrivateKey()
	if err != nil {
		log.Fatal("generate key error", err)
	}
	peerPublicKey, err := wgtypes.ParseKey("uREcerxMksoD3K0dy1ciJDRGzGCJ8jvIzJ5r9jWApXY=")
	if err != nil {
		log.Fatal("parse key error", err)
	}
	port := 51820
	keepalive := time.Second * 25

	plexus0 := Wireguard{
		Name: "plexus0",
		MTU:  1420,
		Address: netlink.Addr{
			IPNet: &net.IPNet{
				IP:   net.ParseIP("10.10.10.1"),
				Mask: net.CIDRMask(24, 32),
			},
		},
		Config: wgtypes.Config{
			PrivateKey: &key,
			ListenPort: &port,
			//FirewallMark: &firewallMark,
			ReplacePeers: true,
			Peers: []wgtypes.PeerConfig{
				{
					PublicKey:         peerPublicKey,
					ReplaceAllowedIPs: true,
					AllowedIPs: []net.IPNet{
						{
							IP:   net.ParseIP("10.10.10.2"),
							Mask: net.CIDRMask(32, 32),
						},
						{
							IP:   net.ParseIP("192.168.100.0"),
							Mask: net.CIDRMask(24, 32),
						},
					},
					Endpoint: &net.UDPAddr{
						IP:   net.ParseIP("159.203.23.147"),
						Port: 51821,
					},
					PersistentKeepaliveInterval: &keepalive,
				},
				{
					PublicKey:         peerPublicKey,
					ReplaceAllowedIPs: true,
					AllowedIPs: []net.IPNet{
						{
							IP:   net.ParseIP("10.10.10.3"),
							Mask: net.CIDRMask(32, 32),
						},
						{
							IP:   net.ParseIP("192.168.200.0"),
							Mask: net.CIDRMask(24, 32),
						},
					},
					Endpoint: &net.UDPAddr{
						IP:   net.ParseIP("159.203.23.147"),
						Port: 51821,
					},
					PersistentKeepaliveInterval: &keepalive,
				},
			},
		},
	}
	//if err := netlink.LinkAdd(plexus0); err != nil {
	//	log.Fatal("add link error ", err)
	//}
	//plexus0.ApplyConfig()

	//fmt.Println("type", link.Type())
	//fmt.Println(link.Attrs())
	fmt.Println(plexus0.Attrs())
	if err := plexus0.Up(); err != nil {
		log.Fatal("up error ", err)
	}
	link, err := netlink.LinkByName("plexus0")
	if err != nil {
		log.Fatal("get link error ", err)
	}
	routes, err := netlink.RouteList(link, netlink.FAMILY_V4)
	if err != nil {
		log.Fatal("route list error ", err)
	}
	for i, route := range routes {
		fmt.Println("route", i, route.Dst)
	}
	fmt.Println("del route")
	newRoute := routes[0]
	if err := netlink.RouteDel(&routes[0]); err != nil {
		log.Fatal("route replace error ", err)
	}
	for _, peer := range plexus0.Config.Peers {
		for _, allowed := range peer.AllowedIPs {
			newRoute.Dst = &allowed
			fmt.Println("route add", newRoute.Dst)
			if err := netlink.RouteAdd(&newRoute); err != nil {
				log.Fatal("route add error ", err)
			}
		}
	}
	routes, err = netlink.RouteList(link, netlink.FAMILY_V4)
	if err != nil {
		log.Fatal("route list error ", err)
	}
	for i, route := range routes {
		fmt.Println("route", i, route.Dst)
	}
}
