package main

import (
	"fmt"
	"log/slog"
	"net"

	"github.com/vishvananda/netlink"
	"github.com/vishvananda/netlink/nl"
	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

// wireguard is a netlink compatible representation of wireguard interface
type wireguard struct {
	Name         string
	MTU          int
	Address      netlink.Addr
	Config       wgtypes.Config
	Destinations []net.IPNet
}

// Attrs satisfies netlink Link interface
func (w *wireguard) Attrs() *netlink.LinkAttrs {
	attr := netlink.NewLinkAttrs()
	attr.Name = w.Name
	attr.MTU = w.MTU
	if link, err := netlink.LinkByName(w.Name); err == nil {
		attr.Index = link.Attrs().Index
	}
	return &attr
}

// Type satisfies netlink Link interface
func (w *wireguard) Type() string {
	return "wireguard"
}

// Apply apply configuration to wireguard device
func (w *wireguard) Apply() error {
	wg, err := wgctrl.New()
	if err != nil {
		return err
	}
	defer wg.Close()
	//link, err := netlink.LinkByName(w.Name)
	//if err != nil {
	//	return fmt.Errorf("get link %v", err)
	//}
	//newRoute := netlink.Route{
	//	LinkIndex: link.Attrs().Index,
	//	Scope:     netlink.SCOPE_LINK,
	//	Src:       w.Address.IP,
	//}
	//routes, err := netlink.RouteList(link, netlink.FAMILY_V4)
	//if err != nil {
	//	return fmt.Errorf("get routes %v", err)
	//}
	//for _, route := range routes {
	//	slog.Debug("wireguard apply, deleting routes", "destination", route.Dst)
	//	if err := netlink.RouteDel(&route); err != nil {
	//		slog.Error("delete route", "destination", route.Dst, "error", err)
	//	}
	//}
	//for _, route := range w.Destinations {
	//	slog.Debug("wireguard apply, adding routes", "destination", route)
	//	newRoute.Dst = &route
	//	slog.Info("adding route", "route", newRoute)
	//	if err := netlink.RouteAdd(&newRoute); err != nil {
	//		slog.Error("add route", "destination", newRoute.Dst, "error", err)
	//	}
	//}
	return wg.ConfigureDevice(w.Name, w.Config)
}

// Up brings a wireguard interface up
func (w *wireguard) Up() error {
	if err := netlink.LinkAdd(w); err != nil {
		return fmt.Errorf("link add %v", err)
	}
	if err := netlink.AddrAdd(w, &w.Address); err != nil {
		return fmt.Errorf("add address %v", err)
	}
	if err := netlink.LinkSetUp(w); err != nil {
		return fmt.Errorf("link up %v", err)
	}
	return w.Apply()
}

// Down removes a wireguard interface
func (w *wireguard) Down() error {
	link, err := netlink.LinkByName(w.Name)
	if err != nil {
		return err
	}
	return netlink.LinkDel(link)
}

// New returns a new wireguard interface
func New(name string, mtu int, address netlink.Addr, destinations []net.IPNet, config wgtypes.Config) *wireguard {
	wg := &wireguard{
		Name:         name,
		MTU:          mtu,
		Address:      address,
		Config:       config,
		Destinations: destinations,
	}
	slog.Info("new wireguard interface", "wg", wg)
	return wg
}

// GetDevice returns a wireguard device as wgtype.Device
func GetDevice(name string) (*wgtypes.Device, error) {
	client, err := wgctrl.New()
	if err != nil {
		return nil, err
	}
	return client.Device(name)
}

// Get returns an existing wireguard interface as a plexus.Wireguard
func Get(name string) (*wireguard, error) {
	link, err := netlink.LinkByName(name)
	if err != nil {
		return nil, err
	}
	device, err := GetDevice(name)
	if err != nil {
		return nil, err
	}
	addrs, err := netlink.AddrList(link, nl.FAMILY_V4)
	if err != nil {
		return nil, err
	}
	wg := &wireguard{
		Name:    name,
		MTU:     link.Attrs().MTU,
		Address: addrs[0],
		Config: wgtypes.Config{
			PrivateKey: &device.PrivateKey,
			ListenPort: &device.ListenPort,
			Peers:      convertPeers(device.Peers),
		},
	}
	routes, err := netlink.RouteList(link, netlink.FAMILY_V4)
	if err != nil {
		return nil, err
	}
	for _, route := range routes {
		wg.Destinations = append(wg.Destinations, *route.Dst)
	}
	return wg, nil
}

func convertPeers(input []wgtypes.Peer) []wgtypes.PeerConfig {
	output := []wgtypes.PeerConfig{}
	for _, peer := range input {
		newpeer := wgtypes.PeerConfig{
			PublicKey:                   peer.PublicKey,
			Endpoint:                    peer.Endpoint,
			PersistentKeepaliveInterval: &peer.PersistentKeepaliveInterval,
			ReplaceAllowedIPs:           true,
			AllowedIPs:                  peer.AllowedIPs,
		}
		output = append(output, newpeer)
	}
	return output
}

func (wg *wireguard) DeleteDestination(destination net.IPNet) {
	newRoutes := []net.IPNet{}
	for _, route := range wg.Destinations {
		if route.IP.Equal(destination.IP) {
			continue
		}
		newRoutes = append(newRoutes, route)
	}
	wg.Destinations = newRoutes
}

func (wg *wireguard) AddDestination(destination net.IPNet) {
	wg.Destinations = append(wg.Destinations, destination)
}

func (wg *wireguard) ReplacePeer(key string, endpoint *net.UDPAddr, allowed []net.IPNet) {
	for _, peer := range wg.Config.Peers {
		if peer.PublicKey.String() != key {
			continue
		}
		peer.AllowedIPs = allowed
		peer.Endpoint = endpoint
		peer.ReplaceAllowedIPs = true
		break
	}
}

func (wg *wireguard) DeletePeer(key string) {
	for _, peer := range wg.Config.Peers {
		if peer.PublicKey.String() == key {
			peer.Remove = true
			break
		}
	}
}

func (wg *wireguard) AddPeer(newPeer wgtypes.PeerConfig) {
	wg.Config.Peers = append(wg.Config.Peers, newPeer)
}
