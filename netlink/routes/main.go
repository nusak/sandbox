package main

import (
	"log"
	"net"
	"time"

	"github.com/vishvananda/netlink"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

func main() {
	wgkey, err := wgtypes.GeneratePrivateKey()
	if err != nil {
		log.Fatal(err)
	}
	peerkey, err := wgtypes.GeneratePrivateKey()
	if err != nil {
		log.Fatal(err)
	}
	port := 51830
	PersistentKeepaliveInterval := time.Second * 25
	config := wgtypes.Config{
		PrivateKey:   &wgkey,
		ListenPort:   &port,
		ReplacePeers: true,
		Peers: []wgtypes.PeerConfig{
			{
				PublicKey:                   peerkey.PublicKey(),
				Endpoint:                    &net.UDPAddr{IP: net.IPv4(10, 10, 100, 3), Port: 51830},
				AllowedIPs:                  []net.IPNet{net.IPNet{IP: net.IPv4(10, 10, 100, 3), Mask: net.CIDRMask(24, 32)}},
				PersistentKeepaliveInterval: &PersistentKeepaliveInterval,
			},
		},
	}
	wg := New("wg0", 1420, netlink.Addr{
		IPNet: &net.IPNet{
			IP:   net.IPv4(10, 10, 100, 2),
			Mask: net.CIDRMask(24, 32),
		}},
		[]net.IPNet{}, config)
	if err := wg.Up(); err != nil {
		log.Fatal(err)
	}

	link, err := netlink.LinkByName("wg0")
	if err != nil {
		log.Fatal(err)
	}
	route := netlink.Route{
		Src:       net.IPv4(10, 10, 100, 2),
		LinkIndex: link.Attrs().Index,
		Scope:     netlink.SCOPE_LINK,
		Protocol:  2,
		Dst:       &net.IPNet{IP: net.IPv4(10, 255, 100, 0), Mask: net.CIDRMask(24, 32)},
	}
	log.Println("Route to add:", route.String())

	if err := netlink.RouteAdd(&route); err != nil {
		log.Fatal(err)
	}
	routes, err := netlink.RouteList(link, netlink.FAMILY_V4)
	if err != nil {
		log.Fatal(err)
	}
	for _, r := range routes {
		log.Println("Route:", r.String(), r.Protocol)
	}

}
