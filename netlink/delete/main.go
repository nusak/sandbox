package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/vishvananda/netlink"
	"golang.zx2c4.com/wireguard/wgctrl"
)

func main() {
	run()
}

func run() {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	wg := &sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	wg.Add(10)
	for _ = range 10 {
		go func() {
			defer wg.Done()
			select {
			case <-ctx.Done():
				return
			}
		}()
	}
	for {
		select {
		case <-quit:
			fmt.Println("Shutting down")
			cancel()
			delete()
			wg.Wait()
			fmt.Println("done")
			os.Exit(0)
		}
	}
}

func delete() {
	client, err := wgctrl.New()
	if err != nil {
		log.Fatal(err)
	}
	defer client.Close()
	devices, err := client.Devices()
	if err != nil {
		log.Fatal(err)
	}
	for _, device := range devices {
		fmt.Println(device.Name)
		link, err := netlink.LinkByName(device.Name)
		if err != nil {
			log.Fatal(err)
		}
		if err := netlink.LinkDel(link); err != nil {
			log.Fatal(err)
		}
	}
}
