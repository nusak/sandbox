package main

import (
	"log"
	"os/exec"
)

func main() {
	path, err := exec.LookPath("docker-compose")
	if err != nil {
		log.Fatal(err)
	}
	log.Println(path)
}
