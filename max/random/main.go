package main

import (
	"math/rand"
)

type Person struct {
	Name string
	ID   int
}

func main() {
	//n := int(10e6)
	people := make(map[int]*Person)
	for {
		person := RandPerson()
		people[person.ID] = person
	}
	//println(len(people), "#####", n)
}

func RandPerson() *Person {
	name := make([]byte, 50)
	for i := 0; i < 50; i++ {
		name[i] = byte(65 + rand.Intn(25))
	}
	id := rand.Intn(10e6)
	return &Person{
		Name: string(name),
		ID:   id,
	}
}
