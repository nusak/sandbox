package main

import (
	"fmt"
	"sync"

	"golang.org/x/sync/errgroup"
)

func main() {
	input := make(chan int)
	result := make(chan int)

	g := new(errgroup.Group)
	wg1 := new(sync.WaitGroup)
	wg2 := new(sync.WaitGroup)
	for _ = range 3 {
		g.Go(func() error {
			return hello(input, result)
		})
	}
	wg1.Add(1)
	go func() {
		defer wg1.Done()
		for i := range 9 {
			input <- i
		}
	}()
	wg2.Add(1)
	total := 0
	go func() {
		defer wg2.Done()
		for i := range result {
			total += i
		}
	}()
	if err := g.Wait(); err != nil {
		fmt.Println(err)
	}
	wg1.Wait()
	close(input)
	fmt.Println(total)
}

func hello(input chan int, result chan int) error {
	count := 0
	i, ok := <-input
	if !ok {
		return fmt.Errorf("input is closed")
	}
	result <- i
	count += i
	if count > 3 {
		return fmt.Errorf("count > 3")
	}
	fmt.Println("goroutine done")
	return nil
}
