package main

import (
	"errors"
	"fmt"
	"os"
	"time"

	"golang.org/x/sync/errgroup"
)

func main() {
	in := make(chan int)
	errGroup := new(errgroup.Group)
	fmt.Println("starting goroutines")
	for i := range 3 {
		errGroup.Go(func() error {
			return hello(in, i)
		})
	}
	time.Sleep(3)
	fmt.Println("sending")
	for i := range 9 {
		fmt.Println("sending input", i)
		in <- i
	}
	fmt.Println("waiting")
	if err := errGroup.Wait(); err != nil {
		fmt.Println("error", err)
		os.Exit(1)
	}
	fmt.Println("done")
}

func hello(in chan int, goRoutine int) error {
	defer fmt.Println("goroutine", goRoutine, "done")
	ticker := time.NewTicker(time.Second * 10)
	j := 0
	for {
		select {
		case i := <-in:
			j = j + 1
			fmt.Println("received i", i, "goRoutine", goRoutine, "total", j)
			if j == 3 {
				ticker.Stop()
				fmt.Println("goroutine done, recieved 3 inputs", goRoutine, j)
				return nil
			}
		case now := <-ticker.C:
			fmt.Println("ticker", now)
			return errors.New("timeout")
		}
	}
}
