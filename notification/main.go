package main

import (
	"log"

	"github.com/esiqveland/notify"
	"github.com/godbus/dbus/v5"
)

func main() {
	note := notify.Notification{Body: "Hello  world"}
	conn, err := dbus.ConnectSessionBus()
	if err != nil {
		log.Fatal(err)
	}

	if _, err := notify.SendNotification(conn, note); err != nil {
		log.Fatal(err)
	}
}
