package main

import (
	"log"

	"golang.zx2c4.com/wireguard/wgctrl"
)

func main() {
	port, err := GetLocalListenPort("wg0")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("port:", port)
}

func GetLocalListenPort(ifacename string) (int, error) {
	client, err := wgctrl.New()
	if err != nil {
		return 0, err
	}
	defer client.Close()
	device, err := client.Device(ifacename)
	if err != nil {
		return 0, err
	}
	return device.ListenPort, nil
}
