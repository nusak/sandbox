package main

import (
	"fmt"
	"log"
	"os/exec"
	"strings"
	"sync"

	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

func main() {

	wgMutex := sync.Mutex{}
	client, err := wgctrl.New()
	if err != nil {
		log.Fatal(err)
	}
	defer client.Close()
	//create
	ifconfig, _ := exec.LookPath("ifconfig")
	if _, err := RunCmd(ifconfig+" wg0", false); err == nil {
		if _, err := RunCmd(ifconfig+" wg0 destroy", false); err != nil {
			log.Fatal("deleting interface", err)
		}
	}
	if out, err := RunCmd(ifconfig+" wg create name wg0", true); err != nil {
		log.Println(out)
		log.Fatal("ifconfig ", err)
	}

	//NewIface
	key, err := wgtypes.GeneratePrivateKey()
	if err != nil {
		log.Fatal("key gen", err)
	}
	port := 51830
	//mtu := 1420

	config := wgtypes.Config{
		PrivateKey: &key,
		ListenPort: &port,
	}

	//configure
	wgMutex.Lock()
	defer wgMutex.Unlock()
	//wg0.SetMTU()
	if err := client.ConfigureDevice("wg0", config); err != nil {
		log.Fatal("apply --> ", err)
	}

	// getlistenport
	listen, err := GetLocalListenPort("wg0")
	if err != nil {
		log.Fatal("get listenport", err)
	}
	log.Println("listenport", listen)
}

func GetLocalListenPort(ifacename string) (int, error) {
	client, err := wgctrl.New()
	if err != nil {
		return 0, err
	}
	defer client.Close()
	device, err := client.Device(ifacename)
	if err != nil {
		return 0, err
	}
	return device.ListenPort, nil
}

// RunCmd - runs a local command
func RunCmd(command string, printerr bool) (string, error) {
	args := strings.Fields(command)
	cmd := exec.Command(args[0], args[1:]...)
	cmd.Wait()
	out, err := cmd.CombinedOutput()
	if err != nil && printerr {
		log.Println(fmt.Sprintf("error running command: %s", command))
		log.Println(0, strings.TrimSuffix(string(out), "\n"))
	}
	return string(out), err
}
