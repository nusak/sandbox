package main

import (
	"fmt"
	"time"

	"github.com/fatih/color"
)

func main() {
	//now := time.Now()
	//fmt.Println("Hello, World!")
	//if time.Since(now) > 1*time.Second {
	//	fmt.Println("It took more than 1 second to print this message")
	//}
	//time.Sleep(10 * time.Second)
	//if time.Since(now) > 10*time.Second {
	//	fmt.Println("It took more than 10 seconds to print this message")
	//}
	//when := time.Now().Sub(time.Second * 10)
	d := time.Since(time.Date(2024, 4, 24, 18, 0, 0, 0, time.UTC))

	hour := int(d.Hours())
	minute := int(d.Minutes()) % 60
	second := int(d.Seconds()) % 60
	fmt.Println(hour, minute, second)
	if hour == 0 {
		if minute == 0 {
			fmt.Printf("%d %s ago\n", second, color.GreenString("seconds"))
		}
		fmt.Printf("%d %s %d %s ago\n", minute, color.GreenString("minutes"), second, color.GreenString("seconds"))
	} else {
		fmt.Printf("%d hours %d minutes %d seconds ago\n", hour, minute, second)
	}
	//fmt.Println(time.Now().Clock())
}
