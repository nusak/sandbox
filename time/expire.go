package main

import (
	"time"

	"github.com/kr/pretty"
)

func main() {
	start := time.Now().Add(-36 * time.Hour)
	end := time.Now()
	pretty.Println(start, end)
}
