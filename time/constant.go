package main

import (
	"fmt"
	"time"
)

const junk = 1

var seconds int

func main() {
	fmt.Println("start")
	seconds = 2

	time.Sleep(time.Second * time.Duration(junk))
	fmt.Println("done")
	time.Sleep(time.Second * time.Duration(seconds))
	fmt.Println("hello")
}
