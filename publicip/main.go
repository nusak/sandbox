package main

import (
	"fmt"

	"github.com/gravitl/netmaker/netclient/ncutils"
)

func main() {
	ip, err := ncutils.GetPublicIP()
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(ip)
}
