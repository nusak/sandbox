package main

import (
	"log"
	"sync"
	"time"
)

func main() {
	names := []string{"one", "two", "three"}
	wg := sync.WaitGroup{}
	for _, name := range names {
		wg.Add(1)
		go func(name string) {
			for i := 0; i < 10; i++ {
				log.Println(i, name)
				time.Sleep(time.Second)
			}
			wg.Done()
		}(name)
	}
	wg.Wait()
	log.Println("done")
}
