package main

import (
	"os"

	"gopkg.in/yaml.v3"
)

const (
	serverDataPath = "data.yml"
)

var (
	servers = make(serverSettings)
)

type serverSettings map[string]*Server

// Server - the settings of a server
type Server struct {
	Name        string
	Version     string
	API         string
	CoreDNSAddr string
	Broker      string
	MQPort      string
	MQID        string
	Password    string
	DNSMode     bool
	Is_EE       bool
	Nodes       []string
}

// Set - overwrites a server's settings in memory based on key and new server struct
func Set(k string, serv *Server) {
	servers[k] = serv
}

// Del - deletes a server's settings based on key
func Del(k string) {
	delete(servers, k)
}

// Get - returns a server's settings based on key
func Get(k string) *Server {
	return servers[k]
}

// Save - writes the servers map to disk as a yaml file
func Save() error {
	data, err := yaml.Marshal(servers)
	if err != nil {
		return err
	}

	return os.WriteFile(serverDataPath, data, os.ModePerm)
}

// Load - load's from file
func Load() error {
	data, err := os.ReadFile(serverDataPath)
	if err != nil {
		return err
	}
	return yaml.Unmarshal(data, &servers)
}
