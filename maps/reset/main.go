package main

import "github.com/kr/pretty"

var myMap map[string]string

func main() {
	myMap = make(map[string]string)
	myMap["name"] = "John"
	myMap["age"] = "30"
	myMap["city"] = "New York"
	myMap["country"] = "USA"
	myMap["phone"] = "1234567890"
	pretty.Println(myMap)
	myMap = make(map[string]string)
	myMap["name"] = "helloworld"
	pretty.Println(myMap)
}
