package main

import "fmt"

type visit struct {
	ID   string
	Data string
}

func main() {
	visits := []visit{
		{
			ID:   "one",
			Data: "testing",
		},
		{
			ID:   "one",
			Data: "testing2",
		},
		{
			ID:   "two",
			Data: "two",
		},
	}
	junk := make(map[string]int)
	for _, v := range visits {
		junk[v.ID] = junk[v.ID] + 1
	}
	fmt.Println(junk)
}
