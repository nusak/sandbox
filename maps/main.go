package main

import "fmt"

var junk Junk = make(Junk)

type Junk map[string]string

func main() {
	one := get("hello")
	two := "world"
	update("hello", "world")
	three := get("hello")
	fmt.Println(one, two, *three)
}

func get(k string) *string {
	data, ok := junk[k]
	if ok {
		return &data
	}
	return nil
}

func update(k, v string) {
	junk[k] = v
}
