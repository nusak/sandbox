package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors/version"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var addr = flag.String("listen-address", ":8080", "The address to listen on for HTTP requests")

func main() {
	flag.Parse()
	//recordMetrics()
	reg := prometheus.NewRegistry()

	reg.MustRegister(
		version.NewCollector("example"),
	)

	http.Handle("/metrics", promhttp.HandlerFor(
		reg,
		promhttp.HandlerOpts{},
	))
	fmt.Println("Hello World from new Version collector!")
	log.Fatal(http.ListenAndServe(*addr, nil))
}
