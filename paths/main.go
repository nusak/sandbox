package main

import (
	"log"
	"os/exec"
)

func main() {
	path, err := exec.LookPath("id")
	if err != nil {
		log.Fatal("could not find path for id ", err)
	}
	log.Println("path for id ", path)
	out, err := exec.Command("id", "-u").CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(out))

}
