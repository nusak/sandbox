package main

import (
	"fmt"
	"os"

	"github.com/devilcove/plexus"
	"github.com/kr/pretty"
)

func main() {
	wg, err := plexus.Get("plexus0")
	if err != nil {
		panic(err)
	}
	for i, peer := range wg.Config.Peers {
		if peer.PublicKey.String() == os.Args[1] {
			fmt.Println("found peer to delete")
			wg.Config.Peers[i].Remove = true
			break
		}
	}
	pretty.Println(wg.Config)
	if err := wg.Apply(); err != nil {
		panic(err)
	}
}
