package main

import (
	"log"

	"github.com/kr/pretty"
	"gopkg.in/ini.v1"
)

type Interface struct {
	Address    string
	PrivateKey string
}

type Peer struct {
	Publickey  string
	AllowedIPs string
	Endpoint   string
	//PersistentKeepalive string
}

func main() {
	options := ini.LoadOptions{
		AllowNonUniqueSections: true,
		AllowShadows:           true,
	}
	wireguard, err := ini.LoadSources(options, "./wg0.conf")
	if err != nil {
		log.Println(err)
	}
	pretty.Println(wireguard)
	var iface Interface
	if err := wireguard.Section("Interface").StrictMapTo(&iface); err != nil {
		log.Println(err)
	}
	pretty.Println(iface)
	var peer Peer
	if err := wireguard.Section("Peer").StrictMapTo(&peer); err != nil {
		log.Println(err)
	}
	pretty.Println(peer)
}
