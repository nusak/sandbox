package main

import (
	"fmt"
	"log"

	"golang.zx2c4.com/wireguard/wgctrl"
)

func main() {
	client, err := wgctrl.New()
	if err != nil {
		log.Fatalf("failed to open client: %v", err)
	}
	defer client.Close()
	devices, err := client.Devices()
	if err != nil {
		log.Fatalf("failed to get devices: %v", err)
	}
	for _, device := range devices {
		fmt.Printf("interface: %s\n", device.Name)
		fmt.Printf("  public key: %s\n", device.PublicKey)
		fmt.Printf("  private key: %s\n", device.PrivateKey)
		fmt.Printf("  listen port: %d\n", device.ListenPort)
		fmt.Printf("  fwmark: %d\n", device.FirewallMark)
		fmt.Printf("  peers:\n")
		for _, peer := range device.Peers {
			fmt.Printf("    public key: %s\n", peer.PublicKey)
			fmt.Printf("    preshared key: %s\n", peer.PresharedKey)
			fmt.Printf("    endpoint: %s\n", peer.Endpoint)
			fmt.Printf("    allowed ips: %s\n", peer.AllowedIPs)
			fmt.Printf("    latest handshake: %s\n", peer.LastHandshakeTime)
		}
	}
}
