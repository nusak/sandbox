package main

import (
	"fmt"
	"strings"

	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

func main() {
	count := 0
	for {
		count++
		priv, err := wgtypes.GeneratePrivateKey()
		if err != nil {
			panic(err)
		}
		pub := priv.PublicKey()
		if strings.Contains(pub.String(), "/") {
			fmt.Println("found / in pubkey", count)
		} else {
			fmt.Println(count, pub.String())
			fmt.Println(len(pub), len(pub.String()))
			return
		}
	}
}
