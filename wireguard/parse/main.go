package main

import (
	"fmt"
	"log"
	"strings"

	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

func main() {
	key, pub, err := generateKey()
	if err != nil {
		log.Fatal(err)
	}
	secondPub := key.PublicKey().String()

	thirdpub, err := wgtypes.ParseKey(secondPub)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(key, pub, secondPub, thirdpub)
}

func generateKey() (*wgtypes.Key, *wgtypes.Key, error) {
	for {
		key, err := wgtypes.GenerateKey()
		if err != nil {
			return nil, nil, err
		}
		pub := key.PublicKey()
		if !strings.Contains(pub.String(), "/") {
			return &key, &pub, nil
		}
	}
}
