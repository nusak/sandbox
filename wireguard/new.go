package main

import (
	"log"

	"github.com/kr/pretty"
	"golang.zx2c4.com/wireguard/wgctrl"
)

func main() {

	client, err := wgctrl.New()
	if err != nil {
		log.Fatal(err)
	}
	devices, err := client.Devices()
	if err != nil {
		log.Fatal(err)
	}
	for _, device := range devices {
		pretty.Println(device)
	}
}
