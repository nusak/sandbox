package main

import (
	"github.com/devilcove/boltdb"
	"github.com/devilcove/plexus"
	"github.com/kr/pretty"
)

func main() {
	boltdb.Initialize("/home/mkasun/.local/share/plexus/plexus-agent.db", []string{"devices", "networks"})
	defer boltdb.Close()
	network, err := boltdb.Get[plexus.Network]("connectivity", "networks")
	if err != nil {
		panic(err)
	}
	pretty.Println(network)
}
