package main

import (
	"errors"
	"fmt"
	"os"
	"time"
	_ "unsafe"
)

//go:linkname fmtPrintln fmt.Println
func fmtPrintln(a ...any) (int, error) {
	fmt.Printf("Hello, World!\n")
	return 0, nil
}

//go:linkname timeNow time.Now
func timeNow() time.Time {
	return time.Date(2028, 1, 4, 8, 5, 20, 0, time.UTC)
}

//go:linkname osReadFile os.ReadFile
func osReadFile(name string) ([]byte, error) {
	return nil, errors.New("osOpen not implemented")
}

func main() {
	n, err := fmt.Printf("vim-go\n")
	fmt.Print(n, err, "\n")
	n, err = fmt.Println("123")
	fmt.Print(n, err, "\n")
	fmt.Print(time.Now(), "\n")
	f, err := os.ReadFile("./main.go")
	fmt.Print(err, "\n")
	fmt.Print(string(f), "\n")
}
