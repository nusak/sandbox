package hello

import (
	"testing"
)

func TestHelloWorld(t *testing.T) {
	t.Run("HelloWorld", func(t *testing.T) {
		returned := HelloWorld()
		expected := "Hello World"

		compare(expected, returned, t)
	})
	t.Run("HelloWorldBroken", func(t *testing.T) {
		returned := HelloWorldBroken()
		expected := "BROKEN"

		compare(expected, returned, t)
	})
}

func TestHelloWorldBroken(t *testing.T) {
	returned := HelloWorldBroken()
	expected := "Hello, world!"

	compare(returned, expected, t)
	compare(HelloWorldBroken(), "Hello world", t)
}

func compare(expected, returned string, t *testing.T) {
	// HERE! Mark as a HELPER function
	t.Helper()

	if returned != expected {
		t.Errorf("Returned: [%v], Expected: [%v]", returned, expected)
	}
}
