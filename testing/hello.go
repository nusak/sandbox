package hello

func HelloWorld() string {
	return "Hello World"
}

func HelloWorldBroken() string {
	return "BROKEN"
}
