package main

import (
	"fmt"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/widget"
)

func main() {
	myApp := app.New()
	//myWindow := myApp.NewWindow("List Data")
	w := myApp.NewWindow("list")
	//text := widget.NewTextGridFromString("Hello World!")
	//text2 := widget.NewTextGridFromString("this is a longer text!")
	//text3 := widget.NewTextGridFromString("this is some text!\nwith a new line!")
	//text4 := widget.NewLabel("List Data")
	//str := binding.NewString()
	//str.Set("Hi!")

	//label := container.NewVBox(
	//	widget.NewLabelWithData(str),
	//	widget.NewEntryWithData(str),
	//)
	//grid := container.NewGridWithColumns(2, text, text2, text3, text4, label)
	//center := container.NewCenter()
	data := binding.BindStringList(
		&[]string{"Item 1", "Item 2", "Item 3"},
	)

	list := widget.NewListWithData(data,
		func() fyne.CanvasObject {
			return widget.NewLabel("template")
		},
		func(i binding.DataItem, o fyne.CanvasObject) {
			o.(*widget.Label).Bind(i.(binding.String))
		})

	add := widget.NewButton("Append", func() {
		val := fmt.Sprintf("Item %d", data.Length()+1)
		data.Append(val)
	})
	add.Resize(fyne.NewSize(150, 30))

	go func() {
		time.Sleep(time.Second * 5)
		list, _ := data.Get()
		list[0] = "Item 0"
		data.Set(list)
	}()

	//myWindow.SetContent(container.NewBorder(center, add, grid, list, text))
	//center.Add(container.NewVBox(list, add))
	w.SetContent(container.NewGridWithRows(2, container.NewVScroll(list), add))
	//myWindow.Show()
	w.Show()
	myApp.Run()
}
