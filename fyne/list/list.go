package main

import (
	"fmt"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

var data = []string{"a", "string", "list"}

func main() {
	myApp := app.New()
	myWindow := myApp.NewWindow("List Data")

	//data := binding.BindStringList(
	//	&[]string{},
	//)

	list := widget.NewList(
		func() int {
			return len(data)
		},
		func() fyne.CanvasObject {
			return widget.NewLabel("template")
		},
		func(i widget.ListItemID, o fyne.CanvasObject) {
			o.(*widget.Label).SetText(data[i])
		})

	add := widget.NewButton("Append", func() {
		val := fmt.Sprintf("Item %d", len(data)+1)
		//data.Append(val)
		data = append(data, val)
	})
	listCon := container.New(layout.NewGridLayout(len(data)), list)
	//listCon.Add(list)
	//list.Resize(fyne.Size{Height: 200})
	myWindow.SetContent(container.NewVBox(add, listCon))
	myWindow.ShowAndRun()
}
