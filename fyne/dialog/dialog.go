package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

var myApp fyne.App

func main() {
	myApp = app.New()
	myWindow := myApp.NewWindow("test")

	yes := widget.NewButton("Yes", closeCallback)
	yes.Importance = widget.HighImportance
	myWindow.SetContent(container.NewVBox(
		widget.NewLabel("Are you sure you want to interact with this test dialog?"),
		container.NewHBox(layout.NewSpacer(),
			widget.NewButton("No", closeCallback), yes,
			layout.NewSpacer())))

	myWindow.ShowAndRun()
}

func closeCallback() {
	myApp.Quit()
}
