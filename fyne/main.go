package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

func main() {
	a := app.New()
	w := a.NewWindow("Image")

	image := canvas.NewImageFromResource(fyne.NewStaticResource("logo", SmallLogo))
	image.FillMode = canvas.ImageFillOriginal

	c := container.NewVBox()
	c.Add(widget.NewTextGridFromString("Hello Fyne!"))
	c.Add(image)
	c.Add(widget.NewButtonWithIcon("Quit", theme.CancelIcon(), func() {
		a.Quit()
	}))
	w.SetContent(c)
	//w.SetContent(image)
	w.ShowAndRun()
}
