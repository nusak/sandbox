package main

import (
	"log"

	"fyne.io/fyne/app"
	"fyne.io/fyne/widget"
)

func main() {
	data := struct {
		Name  string
		Age   int
		Email string
	}{
		Name:  "John Doe",
		Age:   42,
		Email: "john@example.com",
	}
	myApp := app.New()
	myWindow := myApp.NewWindow("Form Widget")

	entry := widget.NewLabelWithData(&data, "Name")
	textArea := widget.NewMultiLineEntry()

	form := &widget.Form{
		Items: []*widget.FormItem{ // we can specify items in the constructor
			{Text: "Entry", Widget: entry}},
		OnSubmit: func() { // optional, handle form submission
			log.Println("Form submitted:", entry.Text)
			log.Println("multiline:", textArea.Text)
			myWindow.Close()
		},
	}

	// we can also append items
	form.Append("Text", textArea)

	myWindow.SetContent(form)
	myWindow.ShowAndRun()
}
