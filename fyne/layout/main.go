package main

import (
	"fmt"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

func main() {
	a := app.New()
	w := a.NewWindow("Hello")

	startString := binding.NewString()
	startString.Set(time.Now().Format("2006-01-02"))
	start := widget.NewEntryWithData(startString)

	startButton := widget.NewButtonWithIcon("", theme.FolderOpenIcon(), func() {})

	text := widget.NewLabel("start time:")
	date := container.New(&datePicker{}, text, start, startButton)
	box := container.NewVBox(container.NewCenter(date), container.NewCenter(widget.NewLabel("hello")))
	fmt.Println(date.MinSize(), text.MinSize(), start.MinSize(), startButton.MinSize())
	w.Resize(fyne.Size{Width: 400, Height: 400})
	w.SetContent(box)
	w.ShowAndRun()

}

type datePicker struct{}

func (d *datePicker) MinSize(objects []fyne.CanvasObject) fyne.Size {
	w, h := float32(0), float32(0)
	for _, o := range objects {
		childSize := o.MinSize()
		w = w + childSize.Width
		h = max(h, childSize.Height)
	}
	return fyne.NewSize(w, h)
}

func (d *datePicker) Layout(objects []fyne.CanvasObject, size fyne.Size) {
	pos := fyne.NewPos(0, 0)
	for i, o := range objects {
		o.Move(pos)
		if i == 1 {
			o.Resize(fyne.Size{o.MinSize().Width + 60, o.MinSize().Height})
			pos = pos.Add(fyne.NewPos(o.MinSize().Width+60, 0))
		} else {
			o.Resize(o.MinSize())
			pos = pos.Add(fyne.NewPos(o.MinSize().Width, 0))
		}
	}
}
