package main

import (
	"log"

	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

func main() {
	myApp := app.New()
	myWindow := myApp.NewWindow("Choice Widgets")

	one := widget.NewCheck("one", func(value bool) {
		log.Println("Check one set to", value)
	})

	two := widget.NewCheck("two", func(value bool) {
		log.Println("Check two set to", value)
	})

	All := widget.NewCheck("Select All", func(value bool) {
		log.Println("Selected all")
		one.SetChecked(value)
		two.SetChecked(value)
	})

	myWindow.SetContent(container.NewVBox(All, one, two))
	myWindow.ShowAndRun()
}
