package main

import (
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	xwidget "fyne.io/x/fyne/widget"
)

func main() {
	a := app.New()
	w := a.NewWindow("Test")
	i := widget.NewLabel("Select a date")
	i.Alignment = fyne.TextAlignCenter
	l := widget.NewLabel("No date selected")
	l.Alignment = fyne.TextAlignCenter
	d := &date{instruction: i, dateChosen: l}
	c := xwidget.NewCalendar(time.Now(), d.onSelected)
	var modal *widget.PopUp
	w.SetContent(container.NewVBox(
		i,
		l,
		widget.NewButton("foo", func() { modal = runPopUp(w, c, d) }),
		widget.NewButton("Close", func() {
			if modal != nil {
				modal.Hide()
			}
		}),
	))
	w.Resize(fyne.NewSize(1024, 768))
	w.ShowAndRun()
}

func runPopUp(w fyne.Window, c *xwidget.Calendar, d *date) (modal *widget.PopUp) {
	modal = widget.NewModalPopUp(
		container.NewVBox(
			c,
			widget.NewButton("Close", func() { modal.Hide() }),
		),
		w.Canvas(),
	)
	modal.Show()
	return modal
}

type date struct {
	instruction *widget.Label
	dateChosen  *widget.Label
}

func (d *date) onSelected(t time.Time) {
	d.instruction.SetText("Date Selected:")
	d.dateChosen.SetText(t.Format("Monday, 2 Jan 2006"))
}
