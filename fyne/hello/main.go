package main

import (
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	xwidget "fyne.io/x/fyne/widget"
)

func main() {
	a := app.New()
	w := a.NewWindow("Calendar")

	i := widget.NewLabel("Please Choose a Date")
	i.Alignment = fyne.TextAlignCenter
	l := widget.NewLabel("")
	l.Alignment = fyne.TextAlignCenter
	d := &date{instruction: i, dateChosen: l}
	// Defines which date you would like the calendar to start
	startingDate := time.Now()
	calendar := xwidget.NewCalendar(startingDate, d.onSelected)
	var modal *widget.PopUp
	w.SetCloseIntercept(container.NewVBox(
		widget.NewButton("Show Calendar", func() { modal = runPopUp(w, calendar, d) }),
		i,
		l,
	))

	//e := container.NewVBox(i, l)

	w.ShowAndRun()
}

type date struct {
	instruction *widget.Label
	dateChosen  *widget.Label
}

func (d *date) onSelected(t time.Time) {
	// use time object to set text on label with given format
	d.instruction.SetText("Date Selected:")
	d.dateChosen.SetText(t.Format("Mon 02 Jan 2006"))
}

func runPopUp(w fyne.Window, c *xwidget.Calendar, d *date) (modal *widget.PopUp) {
	modal = widget.NewModalPopUp(
		container.NewVBox(
			c,
		),
		w.Canvas(),
	)
	modal.Show()
	return modal
}
