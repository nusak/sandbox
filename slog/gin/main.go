package main

import (
	"fmt"
	"log/slog"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	sloggin "github.com/samber/slog-gin"
)

func main() {
	if len(os.Args) < 2 {
		panic("Please provide a log level")
	}
	defaultLevel := slog.LevelInfo
	logLevel := &slog.LevelVar{}
	switch os.Args[1] {
	case "debug":
		logLevel.Set(slog.LevelDebug)
		defaultLevel = slog.LevelDebug
	case "info":
		logLevel.Set(slog.LevelInfo)
		defaultLevel = slog.LevelInfo
	case "warn":
		logLevel.Set(slog.LevelWarn)
		defaultLevel = slog.LevelWarn
	case "error":
		logLevel.Set(slog.LevelError)
		defaultLevel = slog.LevelError
	default:
		logLevel.Set(slog.LevelDebug)
		defaultLevel = slog.LevelDebug
	}

	fmt.Println("Log level: ", logLevel)
	logLevel.Set(defaultLevel)
	logger := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: logLevel}))
	config := sloggin.Config{
		DefaultLevel:     slog.LevelDebug,
		ClientErrorLevel: slog.LevelWarn,
		ServerErrorLevel: slog.LevelError,
	}
	slog.SetDefault(logger)
	router := gin.New()
	router.Use(sloggin.NewWithConfig(logger, config))
	router.GET("/", func(c *gin.Context) {
		slog.Debug("debug")
		slog.Info("info")
		slog.Warn("warn")
		slog.Error("error")
		c.String(http.StatusOK, "Hello World")
	})
	router.GET("/error", func(c *gin.Context) {
		c.String(http.StatusInternalServerError, "Error")
	})
	router.GET("/warn", func(c *gin.Context) {
		c.String(http.StatusTeapot, "TeaPot")
	})

	router.Run(":8000")
}
