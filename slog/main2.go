package main

import (
	"log"
	"log/slog"
	"os"
	"path/filepath"
	"sync"

	"github.com/spf13/cobra"
)

func main() {
	slog.Info("testing")
	slog.Info("testing", "key", "value")
	log.Println("testing")
	logger := setupLogging("slog")
	logger2 := setupLogging2()
	slog.SetDefault(logger2)
	slog.Info("testing")
	slog.Info("testing", "key", "value")
	log.Println("log.Println")
	wg := &sync.WaitGroup{}
	for i := 1; i < 3; i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup, num int, logger *slog.Logger) {
			defer wg.Done()
			logger2 := logger.With("goroutine", num)
			slog.SetDefault(logger2)
			for i := 1; i < 10; i++ {
				slog.Info("testing", "routine", num)
			}
			slog.SetDefault(logger)
		}(wg, i, logger)
	}
	wg.Wait()
	slog.Info("done")

}

func setupLogging2() *slog.Logger {
	f, err := os.Create(os.TempDir() + "/temp.log")
	cobra.CheckErr(err)
	//defer f.Close() -- don't close file here
	//replace := func(groups []string, a slog.Attr) slog.Attr {
	//	if a.Key == slog.SourceKey {
	//		a.Value = slog.StringValue(filepath.Base(a.Value.String()))
	//	}
	//	return a
	//}
	logger := slog.New(slog.NewTextHandler(f, &slog.HandlerOptions{}))
	slog.SetDefault(logger)
	return logger
}

func setupLogging(name string) *slog.Logger {
	// setup logging
	//f, err := os.Create(os.TempDir() + "/" + name + ".log")
	//cobra.CheckErr(err)
	//defer f.Close() -- don't close file here
	logLevel := &slog.LevelVar{}
	replace := func(groups []string, a slog.Attr) slog.Attr {
		if a.Key == slog.SourceKey {
			a.Value = slog.StringValue(filepath.Base(a.Value.String()))
		}
		return a
	}
	logger := slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{AddSource: true, ReplaceAttr: replace, Level: logLevel}))
	logger2 := logger.With("testname", name)
	slog.SetDefault(logger2)
	return logger2
}
