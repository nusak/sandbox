package main

import (
	"bytes"
	"io"
	"log"
	"os"

	"golang.org/x/exp/slog"
)

func main() {
	//defer func(l *slog.Logger) { slog.SetDefault(l) }(slog.Default())
	f, err := os.OpenFile("access.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	var foo bytes.Buffer
	//mw := io.MultiWriter(f, os.Stdout, &foo)

	//replace := func(groups []string, a slog.Attr) slog.Attr {
	//	if a.Key == slog.SourceKey {
	//		a.Value = slog.StringValue(filepath.Base(a.Value.String()))
	//	}
	//	return a
	//}
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	logLevel := &slog.LevelVar{}
	log.Printf("log this")
	//logger := slog.New(slog.HandlerOptions{AddSource: true, ReplaceAttr: replace}.NewJSONHandler(io.MultiWriter(os.Stdout, &foo, f)))
	logger := slog.New(slog.HandlerOptions{Level: logLevel}.NewTextHandler(io.MultiWriter(os.Stdout, &foo, f)))
	slog.SetDefault(logger)
	//logger.Info("this is a test")
	//logger.Error("this is an error")
	//logger.Warn("this is a warning")
	slog.Info("info message")
	log.Println("testing log")
	slog.Debug("debug message")
	slog.Warn("warning message")
	logLevel.Set(slog.LevelDebug)
	slog.Debug("debug2 message")
	logLevel.Set(slog.LevelInfo)
	slog.Debug("debug3 message")
	junk := os.ErrNotExist
	//slog.Error("error message", slog.Error(junk.Error()))
	slog.Error("error string" + junk.Error())
	logger.Error("error string", "err:", junk)

}
