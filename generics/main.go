package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type Endpoint[T comparable] struct {
	URL           string
	Route         string
	Method        string
	Authorization string
	Data          any
	Response	T 
}

var Client http.Client
var Response any

func main() {
	request := struct {
		User string
		Pass string
	}{
		User: "demo",
		Pass: "pass",
	}
	response := struct {
		JWT string
	}{}
	endpoint := Endpoint[struct{JWT string}]{
		URL:      "https://firefly.nusak.ca",
		Route:    "/login",
		Method:   http.MethodPost,
		Data:     request,
		Response: response,
	}

	log.Println("call function")
	Response := struct{ JWT string }{}
	_=Response
	answer, err := JSON(request, response, http.MethodPost, "https://firefly.nusak.ca/login", "")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%T\n", answer)
	fmt.Println()
	log.Println("call method")
	Response = struct{ JWT string }{}
	answer, err = endpoint.JSON(response)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%T\n", answer)
	fmt.Println()
	log.Println("call second method")
	Response = struct{ JWT string }{}
	answer, err = endpoint.JSON2(response)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%T\n", answer)
	fmt.Println()
	log.Println("call function with method params")
	Response = struct{ JWT string }{}
	answer, err = JSON(endpoint.Data, endpoint.Response, endpoint.Method, endpoint.URL+endpoint.Route, "")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%T\n", answer)

}

// JSON return JSON response from http request
func JSON[T any](data any, answer T, method, url, auth string) (any, error) {
//func JSON[T any](data any, answer T, method, url, auth string) (T, error) {

	response, err := API(data, method, url, auth)
	if err != nil {
	//return answer, nil
		return nil, err
	}
	fmt.Printf("before decoding %T\n", answer)
	defer response.Body.Close()
	if err := json.NewDecoder(response.Body).Decode(&answer); err != nil {
	//return answer, nil
		return nil, err
	}
	fmt.Printf("after decoding %T\n", answer)
	return answer, nil
}

// API returns respnse from http request to url
func API(data any, method, url, auth string) (*http.Response, error) {
	var request *http.Request
	var response *http.Response
	var err error
	if data != "" {
		payload, err := json.Marshal(data)
		if err != nil {
			return response, fmt.Errorf("error encoding data %w", err)
		}
		request, err = http.NewRequest(method, url, bytes.NewBuffer(payload))
		if err != nil {
			return response, fmt.Errorf("error creating http request %w", err)
		}
		request.Header.Set("Content-Type", "application/json")
	} else {
		request, err = http.NewRequest(method, url, nil)
		if err != nil {
			return response, fmt.Errorf("error creating http request %w", err)
		}
	}
	if auth != "" {
		request.Header.Set("Authorization", auth)
	}
	return Client.Do(request)
}

func (e Endpoint[T]) JSON(response T) (any, error) {
	return JSON(e.Data, response, e.Method, e.URL+e.Route, e.Authorization)
}

func (e Endpoint[T]) JSON2(myResponse T) (any, error) {
	//make cp of e.Response
	//myResponse := e.Response
	response, err := API(e.Data, e.Method, e.URL+e.Route, e.Authorization)
	if err != nil {
		return nil, err
	}
	fmt.Printf("before decoding %T\n", myResponse)
	defer response.Body.Close()
	if err := json.NewDecoder(response.Body).Decode(&myResponse); err != nil {
		return nil, err
	}
	fmt.Printf("after decoding %T\n", myResponse)
	return myResponse, nil
}
