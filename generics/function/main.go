package main

import "fmt"

type Command struct {
	Command string
	Data    any
}

func main() {
	// Create a new command
	cmd := Command{"print", "Hello, World!"}
	// Print the command
	fmt.Println(cmd)
	// Print the command's data
	fmt.Println(cmd.Data)
	process(cmd.Data.(string))

	hello := "leave.Junk"
	fmt.Println(hello[6:])
}

func process(data string) {
	fmt.Println(data)
}
