package main

import (
	"context"
	"sync"
	"time"

	"github.com/sagikazarmark/slog-shim"
)

var (
	keepRunning = true
	fallticker  *time.Ticker
	otherticker *time.Ticker
	thirdticker *time.Ticker
)

func main() {
	fallticker = time.NewTicker(1 * time.Second)
	otherticker = time.NewTicker(5 * time.Second)
	thirdticker = time.NewTicker(10 * time.Second)

	wg := sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	wg.Add(2)
	go fallback(ctx, &wg)
	go timer(ctx, &wg)
	go func(context.CancelFunc) {
		time.Sleep(25 * time.Second)
		cancel()
	}(cancel)
	wg.Wait()
}

func fallback(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		select {
		case <-ctx.Done():
			fallticker.Stop()
			return
		case <-fallticker.C:
			if keepRunning {
				println("fallback", keepRunning)
				continue
			} else {
				println("fallback skipping", keepRunning)
			}
		}
	}
}

func timer(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		select {
		case <-ctx.Done():
			otherticker.Stop()
			thirdticker.Stop()
			return
		case <-otherticker.C:
			keepRunning = false
			println("timer stop", keepRunning)
		case <-thirdticker.C:
			keepRunning = true
			println("timer start", keepRunning)
		}
	}
}

func MQFallback(ctx contex.Context, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		select {
		case <-ctx.Done():
			fallbackTicker.Stop()
			return
		case <-fallbackTicker.C:
			if Mqclient == nil || !Mqclient.IsConnected() {
				response, err := pull(false)
				if err != nil {
					slog.Error("pull failed", "error", err)
				} else {
					MQFallbackPull(response)
				}
			}
		}
	}
}
