package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/cloverstd/tcping/ping"
)

func main() {
	wg := sync.WaitGroup{}
	quit := make(chan os.Signal, 1)
	restart := make(chan os.Signal, 1)
	//restart := make(chan int)
	signal.Notify(quit, syscall.SIGTERM, os.Interrupt)
	signal.Notify(restart, syscall.SIGHUP)
	_, cancel := StartChildren(&wg)
	for {
		select {
		case <-quit:
			log.Println("shutting down netclient daemon")
			cancel()
			wg.Wait()
			log.Println("shutdown complete")
			return
		case <-restart:
			log.Println("received restart")
			cancel()
			wg.Wait()
			log.Println("children have stopped")
			_, cancel = StartChildren(&wg)
		}
	}
}

func Checkin(ctx context.Context, wg *sync.WaitGroup, tick *time.Ticker) {
	defer wg.Done()
	//childwg := sync.WaitGroup{}
	//_, cancel := StartChildren(&childwg)
	for {
		select {
		case <-ctx.Done():
			log.Println("checkin routine closed")
			return
			//delay should be configuraable -> use cfg.Node.NetworkSettings.DefaultCheckInInterval ??
		case <-tick.C:
			p, err := os.FindProcess(os.Getpid())
			if err != nil {
				log.Println("failed to get process", err)
			}
			if err := p.Signal(syscall.SIGHUP); err != nil {
				log.Println("SIGHUP failed", err)
			}
		}
	}
}

func Child(ctx context.Context, wg *sync.WaitGroup, count int, tick *time.Ticker) {
	defer wg.Done()
	for {
		select {
		case <-ctx.Done():
			log.Println(count, "is done")
			return
		case <-tick.C:
			log.Println(count)
			pinger := ping.NewTCPing()
			pinger.SetTarget(&ping.Target{
				Protocol: ping.TCP,
				Host:     "10.10.10.1",
				Port:     22,
				Counter:  3,
				Interval: 1 * time.Second,
				Timeout:  2 * time.Second,
			})
			pingerDone := pinger.Start()
			<-pingerDone
			if pinger.Result().SuccessCounter == 0 {
				log.Println("ping failed")
			}
		}
	}
}

func StartChildren(wg *sync.WaitGroup) (context.Context, context.CancelFunc) {
	ctx, cancel := context.WithCancel(context.Background())
	wg.Add(1)
	timer1 := time.NewTicker(time.Second * 5)
	timer2 := time.NewTicker(time.Second * 30)
	go Checkin(ctx, wg, timer2)
	for i := 1; i < 4; i++ {
		wg.Add(1)
		go Child(ctx, wg, i, timer1)
	}
	return ctx, cancel
}
