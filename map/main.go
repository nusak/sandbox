package main

import (
	"fmt"
	"slices"

	"github.com/kr/pretty"
)

type ClientMap map[string]bool

var set map[string]bool

func main() {
	set = make(map[string]bool)
	pretty.Println(set)

	set["one"] = true
	pretty.Println(set)
	set = make(map[string]bool)
	pretty.Println(set)

	suffix := 0
	interfaces := []int{}
	for i := range 100 {
		taken := slices.Contains(interfaces, i)
		fmt.Println("checking", i, interfaces, taken)
		if !taken {
			suffix = i
			break
		}
	} //name, ok := set.GetClient("one")
	fmt.Println(suffix) //if ok {
	//	fmt.Println(name)
	//}

}

//func (c ClientMap) AddClient(name string) {
//	c[name] = true
//}
//
//func (c ClientMap) GetClient(name string) (ClientMap, bool) {
//	return c[name]
//}
