package main

import "fmt"

type output struct {
	Network string
	Peers   []peerout
}

type peerout struct {
	Name string
}

func main() {
	list(false)
	fmt.Println("--------------")
	list(true)
}

func list(long bool) {
	nodes := []string{"two"}
	peers := []string{"x", "y"}
	found := false
	list := []output{}
	for _, node := range nodes {
		if node == "one" {
			continue
		}
		found = true
		data := output{
			Network: node,
			Peers:   []peerout{},
		}
		fmt.Println(1)
		if long {
			for _, peer := range peers {
				p := peerout{
					Name: peer,
				}
				data.Peers = append(data.Peers, p)
			}
		}
		fmt.Println(2)
		list = append(list, data)
		fmt.Println(3)
	}
	if !found {
		fmt.Println("not found")
	} else {
		fmt.Println(list)
		fmt.Println(4)
	}
}
