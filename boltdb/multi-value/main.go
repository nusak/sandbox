package main

import (
	"fmt"
	"log"
	"strconv"

	"github.com/devilcove/boltdb"
)

type Node struct {
	ID       int
	NodeName string
	Address  string
	Junk     int
}

type User struct {
	Name  string
	Pass  string
	Email string
	Phone string
}

func main() {
	if err := boltdb.Initialize("my.db", []string{"table"}); err != nil {
		log.Fatal(err)
	}
	defer boltdb.Close()
	if err := boltdb.Save(Node{
		ID:       1,
		NodeName: "Node1",
		Address:  "Address1",
		Junk:     1,
	}, strconv.Itoa(1), "table"); err != nil {
		log.Fatal(err)
	}
	if err := boltdb.Save(User{
		Name:  "User1",
		Pass:  "Pass1",
		Email: "Email1",
		Phone: "Phone1",
	}, "User1", "table"); err != nil {
		log.Fatal(err)
	}
	node, err := boltdb.Get[Node]("1", "table")
	if err != nil {
		log.Fatal(err)
	}
	user, err := boltdb.Get[User]("User1", "table")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(node)
	fmt.Println(user)
	users, err := boltdb.GetAll[User]("table")
	if err != nil {
		log.Fatal(err)
	}
	for i, user := range users {
		fmt.Println(i, user)
	}
	nodes, err := boltdb.GetAll[Node]("table")
	if err != nil {
		log.Fatal(err)
	}
	for i, node := range nodes {
		fmt.Println(i, node)
	}
	if err := boltdb.Update(Node{
		ID:       2,
		NodeName: "Node1",
		Address:  "Address1",
		Junk:     2,
	}, "User1", "table"); err != nil {
		log.Fatal(err)
	}
	fmt.Println("users after update")
	users, err = boltdb.GetAll[User]("table")
	if err != nil {
		log.Fatal(err)
	}
	for i, user := range users {
		fmt.Println(i, user)
	}

}
