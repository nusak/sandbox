package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/devilcove/boltdb"
	bolt "go.etcd.io/bbolt"
)

type Person struct {
	ID        int
	FirstName string
	LastName  string
	Random    int
}

func main() {
	if err := boltdb.Initialize("./data.db", []string{"people"}); err != nil {
		panic(err)
	}
	defer boltdb.Close()
	//for i := 0; i < 10000; i++ {
	//	person := Person{ID: i, FirstName: "John", LastName: "Doe", Random: rand.Intn(5000)}
	//	if err := boltdb.Save(person, strconv.Itoa(person.ID), "people"); err != nil {
	//		panic(err)
	//	}
	//}
	start1 := time.Now()
	person := find1()
	fmt.Println("Time to search 1000 records: ", time.Since(start1), person)
	start2 := time.Now()
	person = find2()
	fmt.Println("Time to search 1000 records: ", time.Since(start2), person)
	start3 := time.Now()
	person = find3()
	fmt.Println("Time to search 1000 records: ", time.Since(start3), person)
	start4 := time.Now()
	person = find4()
	fmt.Println("Time to search 1000 records: ", time.Since(start4), person)
	find5()
	db := boltdb.Connection()
	db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("people"))
		c := b.Cursor()
		k, v := c.Seek([]byte("9323"))
		fmt.Println(string(k), string(v))
		return nil
	})
}

func find1() Person {
	people, _ := boltdb.GetAll[Person]("people")
	for _, person := range people {
		if person.Random == 1234 {
			return person
		}
	}
	return Person{}
}

func find2() Person {
	db := boltdb.Connection()
	if db == nil {
		panic("db is nil")
	}
	person := Person{}
	found := false
	if err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("people"))
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			if err := json.Unmarshal(v, &person); err != nil {
				return err
			}
			if person.Random == 1234 {
				found = true
				return nil
			}
		}
		return nil
	}); err != nil {
		return Person{}
	}
	if found {
		return person
	}
	return Person{}
}

func find3() Person {
	db := boltdb.Connection()
	if db == nil {
		panic("db is nil")
	}
	person := Person{}
	found := false
	if err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("people"))
		c := b.Cursor()
		for k, v := c.Seek([]byte("0")); k != nil; k, v = c.Next() {
			if err := json.Unmarshal(v, &person); err != nil {
				return err
			}
			if person.Random == 1234 {
				found = true
				return nil
			}
		}
		return nil
	}); err != nil {
		return Person{}
	}
	if found {
		return person
	}
	return Person{}
}

func find4() Person {
	db := boltdb.Connection()
	if db == nil {
		panic("db is nil")
	}
	person := Person{}
	found := Person{}
	if err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("people"))
		if err := b.ForEach(func(k, v []byte) error {
			if err := json.Unmarshal(v, &person); err != nil {
				return err
			}
			if person.Random == 1234 {
				found = person
				return nil
			}
			return nil
		}); err != nil {
			return err
		}
		return nil
	}); err != nil {
		return Person{}
	}
	return found
}

func find5() {
	people, _ := boltdb.GetAll[Person]("people")
	for _, person := range people {
		if person.Random == 1234 {
			fmt.Println(person)
		}
	}
}
