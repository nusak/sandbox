package main

import (
	"encoding/json"
	"fmt"

	"github.com/devilcove/boltdb"
	"go.etcd.io/bbolt"
)

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
	IsAdmin  bool   `json:"isadmin"`
}

func AdminExists() bool {
	var user User
	var found bool
	db := boltdb.Connection()
	if db == nil {
		return found
	}
	if err := db.View(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte("users"))
		if b == nil {
			return boltdb.ErrNoResults
		}
		_ = b.ForEach(func(k, v []byte) error {
			if err := json.Unmarshal(v, &user); err != nil {
				return err
			}
			if user.IsAdmin {
				found = true
			}
			return nil
		})
		return nil
	}); err != nil {
		return false
	}
	return found
}

func main() {
	boltdb.Initialize("./test.db", []string{"users"})
	if AdminExists() {
		println("Admin user exists")
	} else {
		println("Admin user does not exist")
	}
	for i := range 10 {
		fmt.Println(i)
	}
}
