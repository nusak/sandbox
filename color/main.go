package main

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"github.com/lmittmann/tint"
	"github.com/sagikazarmark/slog-shim"
)

func main() {
	//logger := slog.New(tint.NewHandler(os.Stderr, nil))
	replace := func(groups []string, a slog.Attr) slog.Attr {
		if a.Key == slog.SourceKey {
			source, ok := a.Value.Any().(*slog.Source)
			if ok {
				source.File = filepath.Base(source.File)
				source.Function = filepath.Base(source.Function)
			}
		}
		if a.Key == slog.TimeKey {
			a.Value = slog.StringValue(time.Now().Format(time.DateTime))
		}
		return a
	}

	// set global logger with custom options
	slog.SetDefault(slog.New(tint.NewHandler(os.Stderr, &tint.Options{
		Level:       slog.LevelDebug,
		TimeFormat:  time.Kitchen,
		AddSource:   true,
		ReplaceAttr: replace,
	})))

	slog.Info("Starting server", "addr", ":8080", "env", "production")
	slog.Debug("Connected to DB", "db", "myapp", "host", "localhost:5432")
	slog.Warn("Slow request", "method", "GET", "path", "/users", "duration", 497*time.Millisecond)
	slog.Error("DB connection lost", tint.Err(errors.New("connection reset")), "db", "myapp")
	// Output:

	fmt.Println("\033[31mvim-go\033[0m")
	slog.Debug("hello")
	slog.Info("hello")
	slog.Warn("hello")
	slog.Error("hello")

}
