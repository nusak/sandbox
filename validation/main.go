package main

import (
	"fmt"
	"log"
	"regexp"

	validator "github.com/go-playground/validator/v10"
)

type Entry struct {
	Name string `validate:"min=1,max=20"`
}

func main() {
	match, _ := regexp.MatchString(`\s`, "ju nk")
	fmt.Println("match", match)
	word := Entry{Name: "white space"}
	step1(word)
	word.Name = "good"
	step1(word)
	word.Name = "1234567890123456789012345"
	step1(word)
}

func step1(word Entry) {
	if err := validate(word); err != nil {
		log.Fatal(err)
	}
	fmt.Println("good")

	//fmt.Println(!regexp.MustCompile(`\s`).MatchString(word.Name))

}

func validate(word Entry) error {
	v := validator.New()
	_ = v.RegisterValidation("whitespace", validator.Func(f1 validator.FieldLevel) bool {
		match, err := regexp.MatchString(`\s`, word.Name)
		fmt.Println("match in validation is ", match, word.Name)
		return err == nil && !match
		//return !regexp.MustCompile(`\s`).MatchString(word.Name)
	})
	err := v.Struct(word)
	//pretty.Println(err)
	if err != nil {
		for _, e := range err.(validator.ValidationErrors) {
			log.Println(e.Error())
		}
	}
	return err
}
