package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	path, err := os.Executable()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(path)
}
