package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/godbus/dbus/v5"
	"github.com/holoplot/go-avahi"
	"github.com/kr/pretty"
)

func main() {
	conn, err := dbus.SystemBus()
	if err != nil {
		log.Fatalf("Cannot get system bus: %v", err)
	}

	server, err := avahi.ServerNew(conn)
	if err != nil {
		log.Fatalf("Avahi new failed: %v", err)
	}
	fmt.Println("name is ", getName(server, "192.168.1.40"))
}

func getName(server *avahi.Server, ip string) string {
	ctx, _ := context.WithTimeout(context.Background(), time.Millisecond*500)
	ad, err := server.AddressResolverNew(avahi.InterfaceUnspec, avahi.ProtoUnspec, ip, 0)
	if err != nil {
		log.Fatalf("addressreolver %v", err)
	}
	select {
	case <-ctx.Done():
		log.Println("timeout")
		return ""
	case address := <-ad.FoundChannel:
		pretty.Println("address", address)
		name := strings.Split(address.Name, ".")
		fmt.Println(name[0])
		fmt.Println(strings.Split(address.Name, ".")[0])
		server.AddressResolverFree(ad)
		return name[0]
	}
}
