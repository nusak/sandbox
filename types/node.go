package main

import (
	"log"
	"net"
	"time"

	"github.com/google/uuid"
	"github.com/gravitl/netmaker/models"
	"github.com/kr/pretty"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

type Node struct {
	ID                  uuid.UUID
	Name                string
	Network             string
	Address             net.IPAddr
	Address6            net.IPAddr
	LocalAddress        []net.IPAddr
	ListenPort          int
	LocalListenPort     int
	PublicKey           wgtypes.Key
	Endpoint            net.UDPAddr
	PostUp              string
	PostDown            string
	AllowedIPs          []net.IPAddr
	PersistentKeepAlive int
	IsHub               bool
	AccessKey           string
	Interface           net.Interface
	LastModified        time.Time
	ExpirationTime      time.Time
	LastPeerUpdate      time.Time
	LastCheckin         time.Time
	MacAddress          net.HardwareAddr
	Password            string
	IsRelayed           bool
	IsPending           bool
	IsRelay             bool
	IsDocker            bool
	IsK8S               bool
	IsEgressGateway     bool
	IsIngressGateway    bool
	EgressGatewayRanges []net.IPAddr
	RelayAddrs          []net.IPAddr
	IsStatic            bool
	UDPHolePunch        bool
	IsServer            bool
	Action              string //ENUM??
	IsLocal             bool
	LocalRange          net.IPAddr
	IPForwarding        bool //don't think it is needed
	OS                  string
	MTU                 int
	Version             string
	TrafficKeys         models.TrafficKeys
	InternetGateway     []net.UDPAddr
	Connected           bool
}

type NodePlus struct {
	Node
	Additional string
}

func main() {
	var node Node
	var nodePlus NodePlus
	node.ID = uuid.New()
	nodePlus.Node = node
	nodePlus.Additional = "this is extra"
	pretty.Println(nodePlus)
	log.Println(nodePlus)
	log.Println(node)
	log.Println(nodePlus.ID, nodePlus.Additional)

	log.Println(node.Address.String())
}
