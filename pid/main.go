package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/kr/pretty"
)

func main() {
	savePID()
	pid := readPID()
	fmt.Printf("pid is %d %t\n", pid, pid)
	pretty.Println(pid)
}

func savePID() {
	pid := os.Getpid()
	if err := os.WriteFile("/var/run/pid.pid", []byte(fmt.Sprintf("%d", pid)), 0666); err != nil {
		log.Fatal("could not write pid to file", err)
	}
}

func readPID() int {
	bytes, err := os.ReadFile("/var/run/pid.pid")
	if err != nil {
		log.Fatal("could not read pid file", err)
	}
	pid, err := strconv.Atoi(string(bytes))
	if err != nil {
		log.Fatal("invalid pid file contents", err)
	}
	return pid
}
