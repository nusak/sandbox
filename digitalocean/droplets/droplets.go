package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/digitalocean/godo"
)

func main() {
	token := os.Getenv("DIGITALOCEAN_TOKEN")

	client := godo.NewFromToken(token)
	ctx := context.TODO()

	opt := &godo.ListOptions{
		Page:    1,
		PerPage: 200,
	}

	droplets, _, err := client.Droplets.List(ctx, opt)
	if err != nil {
		log.Fatal(err)
	}
	for _, droplet := range droplets {
		fmt.Print(droplet.Name)
		for _, network := range droplet.Networks.V4 {
			if network.Type == "public" {
				fmt.Println("\t", network.IPAddress)
			}
		}
	}
}
