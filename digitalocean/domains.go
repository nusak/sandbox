package main

import (
	"context"
	"log"
	"os"

	"github.com/digitalocean/godo"
	"github.com/kr/pretty"
)

func main() {
	token := os.Getenv("DIGITALOCEAN_TOKEN")

	client := godo.NewFromToken(token)
	ctx := context.TODO()

	opt := &godo.ListOptions{
		Page:    1,
		PerPage: 200,
	}

	domains, response, err := client.Domains.Records(ctx, "clustercat.com", opt)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("response: \n", response, "\n")

	for _, domain := range domains {
		pretty.Println(domain)
	}
}
