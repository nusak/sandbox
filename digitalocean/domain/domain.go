package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/digitalocean/godo"
)

func main() {
	token := os.Getenv("DIGITALOCEAN_TOKEN")

	client := godo.NewFromToken(token)
	ctx := context.TODO()

	opt := &godo.ListOptions{
		Page:    1,
		PerPage: 200,
	}

	domains, _, err := client.Domains.Records(ctx, "clustercat.com", opt)
	if err != nil {
		log.Fatal(err)
	}
	droplets, _, err := client.Droplets.List(ctx, opt)
	if err != nil {
		log.Fatal(err)
	}
	for _, domain := range domains {
		found := false
		if domain.Type == "A" {
			for _, droplet := range droplets {
				for _, network := range droplet.Networks.V4 {
					if network.IPAddress == domain.Data {
						found = true
						break
					}
				}
			}
			if !found {
				fmt.Println(domain)
			}
		}
	}

}
