package main

import (
	"fmt"
	"net"
)

func main() {
	ip, _, _ := net.ParseCIDR("10.0.300.1/24")
	if ip == nil {

		fmt.Println("ip is nil")
	} else {
		fmt.Println("ip has value")
	}
}
