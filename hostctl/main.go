package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/guumaster/hostctl/pkg/file"
	"github.com/guumaster/hostctl/pkg/parser"
	"github.com/guumaster/hostctl/pkg/types"
)

func main() {
	network := "nm-netmaker"
	dnsbytes := []byte{49, 48, 46, 48, 46, 49, 48, 46, 50, 53, 52, 32, 110, 101, 116, 109, 97, 107, 101, 114, 45, 49, 50, 46, 110, 101, 116, 109, 97, 107, 101, 114, 10, 49, 48, 46, 48, 46, 49, 48, 46, 49, 32, 101, 110, 100, 101, 97, 118, 111, 117, 114, 46, 110, 101, 116, 109, 97, 107, 101, 114, 10}
	dns := string(dnsbytes)
	log.Println(dns)
	if err := setHostDNS(dns, network, false); err != nil {
		log.Fatal(err)
	}
	dns =
		`10.0.10.254 netmaker-12.netmaker
10.0.10.1 hello.netmaker
`
	if err := setHostDNS(dns, network, false); err != nil {
		log.Fatal(err)
	}
	fmt.Println("vim-go")
}

func setHostDNS(dns, iface string, windows bool) error {
	etchosts := "/etc/hosts"
	if windows {
		etchosts = "c:\\windows\\system32\\drivers\\etc\\hosts"
	}
	dnsdata := strings.NewReader(dns)
	profile, err := parser.ParseProfile(dnsdata)
	if err != nil {
		return err
	}
	hosts, err := file.NewFile(etchosts)
	if err != nil {
		return err
	}
	profile.Name = strings.ToLower(iface)
	profile.Status = types.Enabled
	if err := hosts.ReplaceProfile(profile); err != nil {
		return err
	}
	if err := hosts.Flush(); err != nil {
		return err
	}
	return nil
}

func removeHostDNS(iface string, windows bool) error {
	etchosts := "/etc/hosts"
	if windows {
		etchosts = "c:\\windows\\system32\\drivers\\etc\\hosts"
	}
	hosts, err := file.NewFile(etchosts)
	if err != nil {
		return err
	}
	if err := hosts.RemoveProfile(strings.ToLower(iface)); err != nil {
		if err == types.ErrUnknownProfile {
			return nil
		}
		return err
	}
	if err := hosts.Flush(); err != nil {
		return err
	}
	return nil
}
