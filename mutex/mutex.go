package main

import (
	"fmt"
	"sync"

	"github.com/gravitl/netmaker/models"
	"golang.org/x/exp/slog"
)

var (
	dependencies []string
	update       string
	install      string
)

var (
	extClientCacheMutex = &sync.RWMutex{}
	extClientCacheMap   = make(map[string]models.ExtClient)
)

func main() {
	client := models.ExtClient{
		ClientID:   "road-warrior",
		PrivateKey: "kaxjcchadlj",
		PublicKey:  "xcjhalgs",
		Network:    "devops",
		Enabled:    true,
	}

	for i := 0; i < 100; i++ {
		fmt.Println(i)
		storeExtClientInCache(client.ClientID+"###"+client.Network, client)
		//time.Sleep(100 * time.Millisecond)
		getAllExtClientsFromCache()
		fmt.Println(getAllExtClientsFromCache())
		deleteExtClientFromCache(client.ClientID + "###" + client.Network)
		clients := getAllExtClientsFromCache()
		if len(clients) > 0 {
			slog.Error("cache not empty", "count", i, "clients", clients)
		}
		fmt.Println("clients", clients)
	}
}

func getAllExtClientsFromCache() (extClients []models.ExtClient) {
	extClientCacheMutex.RLock()
	for _, extclient := range extClientCacheMap {
		extClients = append(extClients, extclient)
	}
	extClientCacheMutex.RUnlock()
	return
}

func deleteExtClientFromCache(key string) {
	extClientCacheMutex.Lock()
	slog.Debug("deleting extclient from cache", "key", key, "TAG", "junk")
	delete(extClientCacheMap, key)
	extClientCacheMutex.Unlock()
}

func getExtClientFromCache(key string) (extclient models.ExtClient, ok bool) {
	extClientCacheMutex.RLock()
	defer extClientCacheMutex.RUnlock()
	extclient, ok = extClientCacheMap[key]
	return
}

func storeExtClientInCache(key string, extclient models.ExtClient) {
	extClientCacheMutex.Lock()
	extClientCacheMap[key] = extclient
	extClientCacheMutex.Unlock()
}
