package main

import (
	"log"
	"os"
	"syscall"
)

func main() {
	file, err := os.Open("/home/mkasun/time/time1.db")
	if err != nil {
		log.Fatal(err)
	}
	if err := syscall.Flock(int(file.Fd()), syscall.LOCK_UN); err != nil {
		log.Fatal(err)
	}
	log.Println("Locked")
}
