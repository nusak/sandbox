package main

import (
	"log"
	"os"
	"runtime/pprof"
	"strconv"

	"github.com/huin/goupnp"
	"github.com/huin/goupnp/dcps/av1"
)

func main() {
	f, err := os.Create("cpu.prof")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	if err := pprof.StartCPUProfile(f); err != nil {
		log.Fatal(err)
	}
	defer pprof.StopCPUProfile()
	results, err := goupnp.DiscoverDevices(av1.URN_AVTransport_1)
	if err != nil {
		log.Fatal(err)
	}
	//pretty.Println(results)
	//devices := make(map[*goupnp.RootDevice]struct{}, len(results))
	devices := make([]goupnp.RootDevice, len(results))
	for _, result := range results {
		if result.Err != nil {
			continue
		}
		devices = append(devices, *result.Root)
		//devices[result.Root] = struct{}{}
		//log.Println(devices[result.Root].Device)
	}
	//pretty.Println(devices)
	log.Println("found", len(devices), "sonos devices")
	for i, device := range devices {
		log.Println(i, device.Device.FriendlyName)
	}
}

func quote(s string) string {
	q := strconv.Quote(s)
	if len(s) > 0 && q[0] == '"' && q[len(q)-1] == '"' {
		q = q[1 : len(q)-1]
	}
	return q
}
