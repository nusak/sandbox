package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"log"
	"os"
)

func main() {
	//mac := "4bcf481c5d85df1c61038784ca5e6af089e3561da05b9d7125d2097d8177c46d"
	bytes, err := os.ReadFile("./junk.json")
	if err != nil {
		log.Fatal(err)
	}
	hash := sha256.New()
	_, err = hash.Write(bytes)
	if err != nil {
		log.Fatal(err)
	}
	md := hash.Sum(nil)
	fmt.Println(hex.EncodeToString(md))
	//fmt.Println(int(md))

	result := hmac.New(sha256.New, []byte("990cd53beb15672bd023425a292d9124"))
	_, err = result.Write(bytes)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(hex.EncodeToString(result.Sum(nil)))
	//fmt.Println(result)
	//fmt.Println(result.Sum(nil))
	//result.Write(bytes)
	//fmt.Println(result)

	//valid := ValidMac(bytes, []byte(mac), []byte("990cd53beb15672bd023425a292d9124"))
	//fmt.Println(valid)

}

func ValidMac(message, messageMAC, key []byte) bool {
	mac := hmac.New(sha256.New, key)
	mac.Write(message)
	expectedMAC := mac.Sum(nil)
	return hmac.Equal(messageMAC, expectedMAC)
}
