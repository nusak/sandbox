package main

import (
	"fmt"
	"unsafe"

	"github.com/google/uuid"
	"github.com/kr/pretty"
)

func main() {
	id1 := uuid.New()
	ids := id1.String()
	pretty.Println(id1, ids)
	fmt.Printf("id: %T %d\n", id1, unsafe.Sizeof(id1))
	fmt.Printf("uuid: %T %d\n", ids, unsafe.Sizeof(ids))
	id, err := uuid.Parse("00000000-0000-0000-0000-000000000000")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(id)
}
