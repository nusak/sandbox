package main

import (
	"fmt"
	"net"
)

func main() {
	ip, cidr, err := net.ParseCIDR("10.10.10.32/24")
	fmt.Println(ip, cidr, err)
}
