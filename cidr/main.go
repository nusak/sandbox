package main

import (
	"errors"
	"log"
	"net"

	"github.com/c-robinson/iplib"
	"github.com/kr/pretty"
)

func main() {
	//cidr := "89cb:4206:9753:2021::1/64"
	cidr := "192.168.1.10/24"
	//cidr := ""
	normal, err := normalizeCIDR(cidr)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(normal)

	answer := net.ParseIP("::192.168.1.205").To16()
	pretty.Println("net parse ", answer)

	_, err = net.Interfaces()
	if err != nil {
		log.Fatal(err)
	}
	address, err := getPrivateAddrBackup()
	if err != nil {
		log.Fatal(err)
	}
	ip, cidr2, err := net.ParseCIDR(address.String())
	if err != nil {
		log.Fatal(err)
	}
	pretty.Println(address, ip, cidr2, err)
	log.Println(address, ip, cidr2, err)

	ip2, cidr2, err := net.ParseCIDR("192.168.0.126/24")
	log.Println(ip2, cidr2, err)
}

func normalizeCIDR(address string) (string, error) {
	ip, IPNet, err := net.ParseCIDR(address)
	if err != nil {
		return "", err
	}
	if ip.To4() == nil {
		net6 := iplib.Net6FromStr(IPNet.String())
		IPNet.IP = net6.FirstAddress()
	} else {
		net4 := iplib.Net4FromStr(IPNet.String())
		IPNet.IP = net4.FirstAddress()
	}
	return IPNet.String(), nil
}

func getPrivateAddrBackup() (net.Addr, error) {
	var address net.Addr
	ifaces, err := net.Interfaces()
	if err != nil {
		return address, err
	}
	for _, i := range ifaces {
		if i.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if i.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := i.Addrs()
		if err != nil {
			return address, err
		}
		address = addrs[0]
		break
	}
	return address, nil
}

func getPrivateAddr() (net.Addr, error) {
	var local net.Addr
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err == nil {
		defer conn.Close()
		local = conn.LocalAddr().(*net.IPNet)
	}
	if local == nil {
		local, err = getPrivateAddrBackup()
	}

	if local == nil {
		err = errors.New("could not find local ip")
	}
	return local, err
}
