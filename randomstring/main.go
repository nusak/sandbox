package main

import (
	"crypto/rand"
	"encoding/base32"
	"fmt"
	mrand "math/rand"
	"time"
)

func main() {
	start := time.Now()
	for i := 1; i < 50000; i++ {
		//fmt.Println(MakeRandomString(12))
		MakeRandomString(12)
	}
	fmt.Println(time.Now().Sub(start))
	fmt.Println()
	start = time.Now()
	for i := 1; i < 50000; i++ {
		//fmt.Println(MakeRandomString2(12))
		MakeRandomString2(12)
	}
	fmt.Println(time.Now().Sub(start))
	fmt.Println()
	start = time.Now()
	for i := 1; i < 50000; i++ {
		//fmt.Println(MakeRandomString3(12))
		MakeRandomString3(12)
	}
	fmt.Println(time.Now().Sub(start))
	fmt.Println(MakeRandomString(12), MakeRandomString2(12), MakeRandomString3(12))
}

func MakeRandomString(n int) string {
	const validChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	result := make([]byte, n)
	if _, err := rand.Reader.Read(result); err != nil {
		return ""
	}
	for i, b := range result {
		result[i] = validChars[b%byte(len(validChars))]
	}
	return string(result)
}

func MakeRandomString2(n int) string {
	const validChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	result := make([]byte, n)

	for x := 0; x < n; x++ {
		result[x] = validChars[mrand.Intn(len(validChars))]
	}
	return string(result)
}

func MakeRandomString3(length int) string {
	randomBytes := make([]byte, length)
	_, err := rand.Read(randomBytes)
	if err != nil {
		panic(err)
	}
	return base32.StdEncoding.EncodeToString(randomBytes)[:length]
}
