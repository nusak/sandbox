package main

import (
	"fmt"

	"github.com/kr/pretty"
)

func main() {
	r := [8][8]string{}
	r[0][1] = "server     "
	r[0][2] = "ingress    "
	r[0][3] = "egress     "
	r[0][4] = "relay      "
	r[0][5] = "relayed    "
	r[0][6] = "extclient  "
	r[0][7] = "egressrange"
	r[1][0] = "server     "
	r[2][0] = "ingress    "
	r[3][0] = "egress     "
	r[4][0] = "relay      "
	r[5][0] = "relayed    "
	r[6][0] = "extclient  "
	r[7][0] = "egressrange"
	r[0][0] = "N/A        "
	r[1][1] = "N/A        "
	r[2][2] = "N/A        "
	r[3][3] = "N/A        "
	r[4][4] = "N/A        "
	r[5][5] = "N/A        "
	r[6][6] = "N/A        "
	r[7][7] = "N/A        "

	pretty.Println(r)

	m := make(map[string]map[string]bool)
	m["server"] = make(map[string]bool)
	m["ingress"] = make(map[string]bool)
	m["server"] = map[string]bool{
		"ingress": true,
	}
	m["server"]["egress"] = true
	//m["ingress"]["server"] = {}

	pretty.Println(m)
	fmt.Println(m)
	fmt.Println(pretty.Formatter(m))
}
