package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/bitfield/script"
	"github.com/itchyny/gojq"
	"github.com/joho/godotenv"
	"github.com/kr/pretty"
	"github.com/pterm/pterm"
	"github.com/spf13/viper"
)

func main() {
	var myEnv map[string]string
	myEnv, err := godotenv.Read("./netmaker.default.env")

	if err != nil {
		panic(err)
	}
	pretty.Println(myEnv)
	myEnv["CLIENT_ID"] = "hello world"
	marhalled, err := godotenv.Marshal(myEnv)
	replaced, _ := script.Echo(marhalled).Replace("\"", "").String()
	os.WriteFile("./new.env", []byte(replaced), 0700)
	//if err := godotenv.Write(marhalled, "new.env"); err != nil {
	//	panic(err)
	//}
	// remove quotes from netmaker.env
	//env, err := script.File("./new.env").Replace("\"", "").String()
	//if err != nil {
	//	panic(err)
	//}
	//if _, err := script.Echo(env).WriteFile("./new.env"); err != nil {
	//	panic(err)
	//}

	return
	script.Exec("ls").Stdout()
	resp, err := script.Get("https://api.clustercat.com/api/server/status").String()
	if err != nil {
		panic(err)
	}
	pterm.Println(resp)
	request, err := http.NewRequest(http.MethodGet, "https://api.clustercat.com/api/networks", nil)
	if err != nil {
		panic(err)
	}
	request.Header.Set("Authorization", "Bearer secretkey")
	//response, err := script.Do(request).Stdout()
	response, err := script.Do(request).JQ(". | length").String()
	if err != nil {
		panic(err)
	}
	fmt.Println("response", response)
	query, err := gojq.Parse(". | length")
	if err != nil {
		panic(err)
	}
	iter := query.Run(response)
	networkCount := 0
	for {
		v, ok := iter.Next()
		if !ok {
			break
		}
		if err, ok := v.(error); ok {
			panic(err)
		}
		networkCount = v.(int)
	}
	//networkCount, err := strconv.Atoi(resp)
	//if err != nil {
	//	panic(err)
	//}
	fmt.Println("networkCount", networkCount)
	resp1, err := script.Do(request).Exec("jq '. | length'").String()
	if err != nil {
		panic(err)
	}
	fmt.Printf("resp1: %v%T", resp1, resp1)
	//if _, err := script.File("/etc/netclient/nodes.yml").JQ(".netmaker.commonnode.id").Stdout(); err != nil {
	if _, err := script.File("/etc/netclient/nodes.yml").Exec("yq .long.commonnode.id").Stdout(); err != nil {
		panic(err)
	}
	viper.SetConfigFile("/etc/netclient/nodes.yml")
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}
	fmt.Println(viper.GetString("long.commonnode.id"))

}
