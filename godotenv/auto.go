package main

import (
	"fmt"
	"os"

	_ "github.com/joho/godotenv/autoload"
)

func main() {
	name := os.Getenv("NAME")
	fmt.Println("Hello", name)
}
