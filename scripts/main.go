package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/bitfield/script"
	"github.com/spf13/viper"
	"golang.org/x/exp/slices"
)

func main() {
	print_logo()
	viper.SetConfigFile("./.env")
	viper.ReadInConfig()
	log.Println(latest())
	log.Println(viper.GetString("MASTER_KEY"))
	log.Println(viper.GetString("NM_DOMAIN"))
	viper.SetConfigFile("./docker-compose.yml")
	viper.ReadInConfig()
	log.Println(viper.GetString("SERVER_NAME"))
	log.Println(get_input("Enter a value: ", []string{"y", "n"}))

}

func get_input(prompt string, valid []string) string {
	script.Echo(prompt).Stdout()
	b := make([]byte, 1)
	for {
		_, err := script.Stdin().Read(b)
		if err != nil {
			return ""
		}
		if b[0] == '\n' {
			continue
		}
		if slices.Contains(valid, string(b)) {
			return string(b)
		}
		fmt.Println("Invalid input: valid inputs are", valid)
	}
}

func latest() string {
	latest, err := script.Get("https://api.github.com/repos/gravitl/netmaker/releases/latest").JQ(".tag_name").String()
	if err != nil {
		return ""
	}
	return strings.ReplaceAll(strings.ReplaceAll(latest, "\"", ""), "\n", "")
}

func print_logo() {
	logo := `- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
																							 
	 __   __     ______     ______   __    __     ______     __  __     ______     ______    
	/\ "-.\ \   /\  ___\   /\__  _\ /\ "-./  \   /\  __ \   /\ \/ /    /\  ___\   /\  == \   
	\ \ \-.  \  \ \  __\   \/_/\ \/ \ \ \-./\ \  \ \  __ \  \ \  _"-.  \ \  __\   \ \  __<   
	 \ \_\\"\_\  \ \_____\    \ \_\  \ \_\ \ \_\  \ \_\ \_\  \ \_\ \_\  \ \_____\  \ \_\ \_\ 
	  \/_/ \/_/   \/_____/     \/_/   \/_/  \/_/   \/_/\/_/   \/_/\/_/   \/_____/   \/_/ /_/ 
																																																	 
	
	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	`
	fmt.Print(logo)
}
