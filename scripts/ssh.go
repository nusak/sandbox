package main

import (
	"fmt"

	"github.com/bitfield/script"
)

type Pipe script.Pipe

func main() {
	response, err := script.Exec("ssh fileserver.clustercat.com 'ls -l'").String()
	if err != nil {
		panic(err)
	}
	fmt.Println(response)
	pipe := Pipe(*script.NewPipe())
	s, err := pipe.SSH("root", "fileserver.clustercat.com", "ls -l").Tee().String()
	if err != nil {
		panic(err)
	}
	fmt.Println(s)

}

func (p Pipe) SSH(user, server, cmd string) *script.Pipe {
	script := script.Pipe(p)
	return script.Exec(fmt.Sprintf("ssh %s@%s '%s'", user, server, cmd))
}
