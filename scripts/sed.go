package main

import (
	"fmt"

	"github.com/bitfield/script"
)

func main() {
	s, err := script.File("./input").Replace("foo", "bar").String()
	if err != nil {
		panic(err)
	}
	n, err := script.Echo(s).WriteFile("./input")
	if err != nil {
		panic(err)
	}
	fmt.Printf("Wrote %d bytes\n", n)
}
