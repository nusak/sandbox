package main

import (
	"fmt"
	"strings"
)

func main() {
	files := []string{"logo.png", "logo.jpg", "logo.gif", "logo.eps", "logo.svg", "logo.epttiff", "logo.ai"}
	for _, file := range files {
		parts := strings.Split(file, ".")
		ext := parts[len(parts)-1]
		if !strings.Contains("svg epttiff ai", ext) {
			fmt.Println(ext, "digitialation fee applies")
		} else {
			fmt.Println(ext, "no digitialation fee")
		}
	}
	subject := "update.ThisIsAFunction"
	fmt.Println(subject[7:])
}
