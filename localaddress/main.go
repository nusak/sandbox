package main

import (
	"errors"
	"log"
	"net"

	"github.com/gravitl/netmaker/logic"
	"github.com/gravitl/netmaker/models"
)

func main() {
	ip, err := getPrivateAddrBackup()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(ip)
	node := models.Node{
		LocalRange: "192.168.1.0/24",
	}
	ip2 := logic.GetLocalIP(node)
	log.Println("ip2", ip2)
	local, err := getPrivateAddr()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("local", local)
}

func getPrivateAddrBackup() ([]string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return []string{}, err
	}
	var local []string
	//found := false
	for _, i := range ifaces {
		//log.Println(i.Name, i.Flags)
		if i.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if i.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		if i.Flags&net.FlagPointToPoint != 0 {
			continue // tunnel
		}
		addrs, err := i.Addrs()
		if err != nil {
			return []string{}, err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				//if !found {
				ip = v.IP
				local = append(local, ip.String())
				//found = true
				//}
			case *net.IPAddr:
				//if !found {
				ip = v.IP
				local = append(local, ip.String())
				//found = true
				//}
			}
		}
	}
	//if !found {
	if len(local) < 1 {
		err := errors.New("local ip address not found")
		return []string{}, err
	}
	return local, err
}

func getPrivateAddr() (string, error) {

	var local string
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err == nil {
		defer conn.Close()

		localAddr := conn.LocalAddr().(*net.UDPAddr)
		localIP := localAddr.IP
		local = localIP.String()
	}
	//if local == "" {
	//	local, err = getPrivateAddrBackup()
	//}

	if local == "" {
		err = errors.New("could not find local ip")
	}
	//if net.ParseIP(local).To16() != nil {
	//	local = "[" + local + "]"
	//}

	return local, err
}
