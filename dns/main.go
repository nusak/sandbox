package main

import (
	"fmt"
	"log"
	"net"
	"net/mail"
	"strings"
)

func main() {
	ips, err := net.LookupIP("google.com")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("google", ips)
	addrs, err := net.LookupIP("localhost")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("localhost", addrs)
	names, err := net.LookupAddr("192.168.1.56")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("ip", names)
	hosts, err := net.LookupHost("endeavour")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("host", hosts)
	ports, err := net.LookupPort("tcp", "https")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("port", ports)
	junk, err := net.LookupIP("example.com")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("junk", junk)

	ip := net.ParseIP("example.com")
	log.Println("netparse fqdn", ip)

	for _, email := range []string{
		"mkasun@nusak.ca",
		"someone@gmail.com",
		"example.com",
		"someone@example.com",
		"joe@localhost",
		"joe@endeavour",
		"joe@192.168.0.1",
		"joe@gmail.com@aws.com",
	} {
		valid, reason := emailValid(email)
		fmt.Println(email, valid, reason)

	}
}

func emailValid(email string) (bool, string) {
	_, err := mail.ParseAddress(email)
	if err != nil {
		return false, "parse"
	}
	parts := strings.Split(email, "@")
	if len(parts) != 2 {
		return false, "split"
	}
	if ip := net.ParseIP(parts[1]); ip != nil {
		return false, "ip"
	}
	if strings.Contains(email, "example.com") {
		return false, "example.com"
	}
	return true, "valid"
}
