package main

import (
	"fmt"
	"net"

	"github.com/kr/pretty"
)

func main() {
	mac := net.HardwareAddr{}
	if mac == nil {
		fmt.Println("empty mac")
	} else {
		fmt.Println("mac not nil")
	}
	if mac.String() == "" {
		fmt.Println("empty string")
	}
	fmt.Println("macstring", mac.String())
	for i := range mac {
		fmt.Println(i, string(mac[i]))
	}
	pretty.Println(mac)
}
