package main

import (
	"fmt"
	"strconv"
)

func main() {
	amount := 1023.3456
	dollars := int(amount)
	cents := (amount - float64(dollars)) * 100

	fmt.Println(amount, dollars, strconv.FormatFloat(cents, 'f', 0, 64))
	fmt.Printf("%f $%.0f  $%d\n", amount, amount, cents*100)
	fmt.Println(strconv.FormatFloat(amount, 'f', 0, 64))

	display := fmt.Sprintf("%d,%s $", dollars, strconv.FormatFloat(cents, 'f', 0, 64))
	fmt.Println(display)
	dEN := fmt.Sprintf("$%.2f", amount)
	fmt.Println(dEN)

}
