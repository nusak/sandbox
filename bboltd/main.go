package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/google/uuid"
	"go.etcd.io/bbolt"
)

var db *bbolt.DB

type Project struct {
	ID     uuid.UUID
	Name   string
	Active bool
	Update time.Time
}

func main() {
	var err error
	db, err = bbolt.Open("/tmp/junk.db", 0666, nil)
	if err != nil {
		panic(err)
	}
	if err := db.Update(func(tx *bbolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("Projects"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	}); err != nil {
		panic(err)
	}
	if err := db.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte("MyBucket"))
		return b.Put([]byte("answer"), []byte("42"))
	}); err != nil {
		panic(err)
	}
	project := Project{
		ID:     uuid.New(),
		Name:   "netmaker",
		Active: true,
		Update: time.Now(),
	}
	if err := updateProject(project); err != nil {
		panic(err)
	}
}

func updateProject(p Project) error {
	return db.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte("Projects"))
		value, err := json.Marshal(p)
		if err != nil {
			return err
		}
		return b.Put([]byte(p.Name), value)
	})
}
