module github.com/devilcove/timetrace-gui

go 1.21.1

require (
	github.com/google/uuid v1.3.1
	go.etcd.io/bbolt v1.3.7
)

require golang.org/x/sys v0.4.0 // indirect
