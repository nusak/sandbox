package main

import (
	"encoding/hex"
	"fmt"
	"os"
	"syscall"
	"time"
)

func main() {
	start := time.Now()
	home, ok := syscall.Getenv("HOME")
	if !ok {
		home = "/usr/home"
	}
	fmt.Println(ok, home, time.Since(start))
	start = time.Now()
	home = os.Getenv("HOME")
	fmt.Println(home, time.Since(start))
	start = time.Now()
	home, ok = os.LookupEnv("HOME")
	if !ok {
		home = "/usr/home"
	}
	fmt.Println(ok, home, time.Since(start))

	src := []byte("Hello World")
	dst := make([]byte, hex.EncodedLen(len(src)))
	hex.Encode(dst, src)
	fmt.Printf("%s %s\n", src, dst)
	fmt.Printf("%s", hex.Dump(src))
	encodedStr := hex.EncodeToString(src)
	fmt.Println(encodedStr)
}
