package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type Person struct {
	Name    string
	Age     int
	Address string
}

func main() {
	txt := os.Args[1]
	pers := Person{Name: txt}
	bytes, err := json.Marshal(pers)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(bytes))
}
