package main

import (
	"bytes"
	"encoding/json"
	"log"
	"time"

	"github.com/kr/pretty"
)

type Request struct {
	Type string `json:"type,omitempty"`
	Data any    `json:"data,omitempty"`
	Num  int    `json:"num,omitempty"`
}

type ReportRequest struct {
	Start    time.Time
	End      time.Time
	Projects []string
	Users    []string
}

func main() {
	reportRequest := ReportRequest{
		Start:    time.Now(),
		End:      time.Now(),
		Projects: []string{"timetrace"},
		Users:    []string{"admin"},
	}
	request := Request{
		Type: "junk",
		Data: reportRequest,
		Num:  1,
	}
	payload, err := json.Marshal(request)
	if err != nil {
		log.Fatal(err)
	}
	pretty.Println("payload\n", request)
	response := Request{}
	if err := json.NewDecoder(bytes.NewReader(payload)).Decode(&response); err != nil {
		log.Fatal("decode", err)
	}
	pretty.Println(response)
	if response.Type == "junk" {
		jsonBody, err := json.Marshal(response.Data)
		if err != nil {
			log.Fatal("marshall", err)
		}
		if err := json.Unmarshal(jsonBody, &reportRequest); err != nil {
			log.Fatal("decode", err)
		}
	}
	pretty.Println(reportRequest)
}
