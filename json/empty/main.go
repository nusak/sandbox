package main

import (
	"encoding/json"
	"log"

	"github.com/kr/pretty"
	_ "modernc.org/sqlite"
)

type order struct {
	OrderID  int
	Customer string
	Name     string
}

func main() {
	unmarshalledOrders := []order{}
	unmarshalledOrder := order{}
	one := order{OrderID: 1, Customer: "John", Name: "John"}
	two := order{OrderID: 2, Name: "Mary"}

	order1, err := json.Marshal(one)
	if err != nil {
		log.Fatal(err)
	}
	order2, err := json.Marshal(two)
	if err != nil {
		log.Fatal(err)
	}
	pretty.Println(string(order1), string(order2))
	if err = json.Unmarshal(order1, &unmarshalledOrder); err != nil {
		log.Fatal(err)
	}
	pretty.Println(unmarshalledOrder)
	unmarshalledOrders = append(unmarshalledOrders, unmarshalledOrder)
	if err = json.Unmarshal(order2, &unmarshalledOrder); err != nil {
		log.Fatal(err)
	}
	pretty.Println(unmarshalledOrder)
	unmarshalledOrders = append(unmarshalledOrders, unmarshalledOrder)
	pretty.Println(unmarshalledOrders)
}
