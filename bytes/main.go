package main

import (
	"crypto/rand"
	"encoding/hex"
)

func main() {

	key := make([]byte, 32)
	rand.Read(key)
	println(hex.EncodeToString(key))
	println(len(key))
	key = []byte("12345678901234567890123456789012")
	println(hex.EncodeToString(key))
	println(len(key))
}
