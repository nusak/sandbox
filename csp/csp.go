package main

import (
	"log"
)

func Foo(Read chan (int), Result chan (int), Write chan (int)) {
	value := 0
	//<-Write
	//value++
	//log.Println("write")
	for {
		select {
		case <-Read:
			log.Println("read")
			Result <- value
		case <-Write:
			log.Println("write")
			value++
		}
	}
}

func main() {
	Result := make(chan int)
	Read := make(chan int)
	Write := make(chan int)
	Done := make(chan int)
	go Foo(Read, Result, Write)
	go func() {
		log.Println("send")
		Write <- 1
		//time.Sleep(time.Second)
		log.Println("recieve")
		Read <- 1
		x := <-Result
		log.Println(x)
		Write <- 1
		Done <- 1
	}()
	<-Done
}
