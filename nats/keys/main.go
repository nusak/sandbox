package main

import (
	"log"

	"github.com/nats-io/nkeys"
)

func main() {
	dummy, _ := nkeys.CreateUser()
	seed, _ := dummy.Seed()
	keypair, _ := nkeys.FromSeed(seed)
	publicKey, _ := keypair.PublicKey()
	publicKey2, _ := dummy.PublicKey()
	privateKey2, _ := dummy.PrivateKey()
	log.Println(string(seed))
	log.Println((keypair), string(privateKey2))
	log.Println(publicKey, publicKey2)
}
