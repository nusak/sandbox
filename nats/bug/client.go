package main

import (
	"log"
	"os"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/nkeys"
)

func main() {
	seed, err := os.ReadFile("/tmp/client.Seed")
	if err != nil {
		log.Fatal(err)
	}
	kp, err := nkeys.FromSeed(seed)
	if err != nil {
		log.Fatal(err)
	}
	pk, err := kp.PublicKey()
	if err != nil {
		log.Fatal(err)
	}
	sign := func(nonce []byte) ([]byte, error) {
		return kp.Sign(nonce)
	}
	opts := []nats.Option{nats.Name("client")}
	opts = append(opts, []nats.Option{
		nats.MaxReconnects(-1),
		nats.DisconnectErrHandler(func(c *nats.Conn, err error) {
			log.Println("disonnected from server", "error", err)
		}),
		nats.ClosedHandler(func(c *nats.Conn) {
			log.Println("nats connection closed")
		}),
		nats.ReconnectHandler(func(c *nats.Conn) {
			log.Println("reconnected to nats server")
		}),
		nats.ErrorHandler(func(c *nats.Conn, s *nats.Subscription, err error) {
			log.Println("nats error handler", err)
		}),
		nats.Nkey(pk, sign),
	}...)
	nc, err := nats.Connect("nats://localhost:4222", opts...)
	if err != nil {
		log.Fatal("connect err", err)
	}
	_, err = nc.Subscribe("_INBOX.>", func(msg *nats.Msg) {
		log.Println(msg.Data)
	})
	if err != nil {
		log.Fatal("subcribe ", err)
	}
	defer nc.Close()
	log.Println("connected to nats server")
	err = nc.Publish("foo", []byte("from client"))
	if err != nil {
		log.Fatal("pub to foo ", err)
	}
	log.Println("pub to foo")
	//log.Println("got response", string(msg.Data))
	_, err = nc.Request("junk", []byte("from client"), time.Second*5)
	if err != nil {
		log.Println("expected error", err)
	}
}
