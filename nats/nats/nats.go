package main

import (
	"fmt"
	"time"

	"github.com/kr/pretty"
	"github.com/nats-io/nats-server/v2/server"
	"github.com/nats-io/nats.go"
)

func main() {
	opts := &server.Options{
		//TLSCert:    "./nusak.ca.crt",
		//TLSKey:     "./nusak.ca.key",
		ConfigFile: "./nats.conf",
		//Users: []*server.User{
		//	{
		//		Username: "user",
		//		Password: "password",
		//	},
		//},
	}

	pretty.Println(opts)

	// Initialize new server with options
	ns, err := server.NewServer(opts)

	if err != nil {
		panic(err)
	}

	// Start the server via goroutine
	go ns.Start()

	// Wait for server to be ready for connections
	if !ns.ReadyForConnections(10 * time.Second) {
		panic("not ready for connection")
	}

	// Connect to server
	nc, err := nats.Connect("user:password@plexus.example.com:4222")

	if err != nil {
		panic(err)
	}

	subject := "my-subject"

	// Subscribe to the subject
	nc.Subscribe(subject, func(msg *nats.Msg) {
		// Print message data
		data := string(msg.Data)
		fmt.Println(data)
		if data == "reload" {
			user2 := server.User{
				Username: "user2",
				Password: "password2",
			}
			opts.Users = append(opts.Users, &user2)
			err := ns.ReloadOptions(opts)
			if err != nil {
				panic(err)
			}
			pretty.Println(opts.Users)
		}
		if data == "print" {
			pretty.Println(opts.Users)
		}

		// Shutdown the server (optional)
		//ns.Shutdown()
	})

	// Publish data to the subject
	nc.Publish(subject, []byte("Hello embedded NATS!"))

	// Wait for server shutdown
	ns.WaitForShutdown()
}
