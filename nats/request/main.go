package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/nats-io/nats-server/v2/server"
)

func main() {
	dir := "/root/.local/share/certmagic/certificates/acme-v02.api.letsencrypt.org-directory/plexus.testing.nusak.ca/"

	serverTLSConfig, err := server.GenTLSConfig(&server.TLSConfigOpts{
		CertFile: dir + "plexus.testing.nusak.ca.crt",
		KeyFile:  dir + "plexus.testing.nusak.ca.key",
	})
	if err != nil {
		panic(err)
	}
	opts := server.Options{
		Host:      "plexus.testing.nusak.ca",
		Port:      4222,
		TLSConfig: serverTLSConfig,
	}
	s := server.New(&opts)
	go s.Start()
	defer s.Shutdown()
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	<-quit
}
