package main

import (
	"fmt"
	"log"
	"time"

	"github.com/nats-io/nats-server/v2/server"
	natsserver "github.com/nats-io/nats-server/v2/test"
	"github.com/nats-io/nats.go"
)

const TEST_PORT = 4222

func main() {
	//t := &testing.T{}
	TestServerRestart()
}

func RunServerOnPort(port int) *server.Server {
	opts := natsserver.DefaultTestOptions
	opts.Port = port
	return RunServerWithOptions(&opts)
}

func RunServerWithOptions(opts *server.Options) *server.Server {
	return server.RunServer(opts)
}

func TestServerRestart() {
	s := RunServerOnPort(TEST_PORT)
	//s.EnableJetStream(&server.JetStreamConfig{})
	go func() {
		s.Start()
	}()
	time.Sleep(time.Second * 5)

	// use nats as normal
	sUrl := fmt.Sprintf("nats://127.0.0.1:%d", TEST_PORT)
	if nc, err := nats.Connect(sUrl); err != nil {
		panic(err)
	} else {
		js, err := nc.JetStream()
		if err != nil {
			log.Fatal("error creating jetstream context:", err)
		}
		if _, err = js.AddStream(&nats.StreamConfig{
			Name:     "natss",
			Subjects: []string{fmt.Sprintf("%s.*", "natss")},
		}); err != nil {
			log.Fatal("error adding stream:", err)
		}

	}
	defer s.Shutdown()
}
