package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/kr/pretty"
	"github.com/nats-io/nats.go"
)

func main() {
	// Connect to server
	nc, err := nats.Connect("localhost:4222", options()...)

	if err != nil {
		panic(err)
	}

	subject := "my-subject"

	// Subscribe to the subject
	nc.Subscribe(subject, func(msg *nats.Msg) {
		// Print message data
		data := string(msg.Data)
		fmt.Println(data)

		// Shutdown the server (optional)
		//ns.Shutdown()
	})
	nc.Subscribe("hello", func(msg *nats.Msg) {
		fmt.Println("Received message on 'hello':", string(msg.Data))
	})
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	ctx, cancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go publish(ctx, wg, nc)

	<-quit
	cancel()
	wg.Wait()
	pretty.Println("Done")
}

func options() []nats.Option {
	opts := []nats.Option{nats.Name("NATS Sample Publisher")}
	opts = append(opts, []nats.Option{
		nats.MaxReconnects(-1),
		nats.DisconnectErrHandler(func(nc *nats.Conn, err error) {
			fmt.Println("Got disconnected!", err)
		}),
		nats.ClosedHandler(func(nc *nats.Conn) {
			fmt.Println("Connection to NATS server closed")
		}),
		nats.ReconnectHandler(func(nc *nats.Conn) {
			fmt.Println("Reconnected to NATS server")
		}),
		nats.ErrorHandler(func(nc *nats.Conn, sub *nats.Subscription, err error) {
			fmt.Println("Error:", err)
		}),
	}...)
	return opts
}

func publish(ctx context.Context, wg *sync.WaitGroup, nc *nats.Conn) {
	defer wg.Done()
	ticker := time.NewTicker(5 * time.Second)
	defer ticker.Stop()
	i := 0
	for {
		select {
		case <-ctx.Done():
			if err := nc.Publish("my-subject", []byte("Goodbye embedded NATS!")); err != nil {
				fmt.Println("Error publishing message:", err)
			}
			return
		case <-ticker.C:
			i++
			fmt.Println("Publishing message", i)
			msg := fmt.Sprintf("Hello embedded NATS! %d", i)
			if err := nc.Publish("my-subject", []byte(msg)); err != nil {
				fmt.Println("Error publishing message:", err)
			}
		}
	}
}
