package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"

	"github.com/nats-io/nkeys"
	"github.com/sagikazarmark/slog-shim"
)

type KeyValue struct {
	URL     string
	Seed    string
	KeyName string
}

func main() {
	token := "eyJVUkwiOiJuYXRzOi8vbG9jYWxob3N0OjQyMjIiLCJTZWVkIjoiU1VBTDJOWVZQUjREWkI0VzdEVFhHT1I1TUFFWkNSWVhBQ0pZMkdYQ1ZFRk5FMjU2NFBVVkFZVU1PWSIsIktleU5hbWUiOiJoZWxsbyJ9"
	keyvalue, _ := decodeToken(token)
	kp, _ := nkeys.FromSeed([]byte(keyvalue.Seed))
	pk, _ := kp.PublicKey()
	priv, _ := kp.PrivateKey()
	fmt.Println(string(priv), pk)
	kp, _ = nkeys.FromSeed([]byte(keyvalue.Seed))
	pk, _ = kp.PublicKey()
	priv, _ = kp.PrivateKey()
	fmt.Println(string(priv), pk)
}

func decodeToken(token string) (KeyValue, error) {
	kv := KeyValue{}
	data, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		slog.Error("base64 decode", "error", err)
		return kv, err
	}
	slog.Info(string(data))
	if err := json.Unmarshal(data, &kv); err != nil {
		slog.Error("token unmarshal", "error", err)
		return kv, err
	}
	return kv, nil
}
