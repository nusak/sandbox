package main

import (
	"log"
	"time"

	"github.com/kr/pretty"
	"github.com/nats-io/nats-server/v2/server"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/nkeys"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	opts, err := server.ProcessConfigFile("./nats.conf")
	if err != nil {
		log.Fatal(err)
	}
	user, err := nkeys.CreateUser()
	if err != nil {
		log.Fatal("create user ", err)
	}
	publicKey, err := user.PublicKey()
	if err != nil {
		log.Fatal("get public key ", err)
	}
	sign := func(nonce []byte) ([]byte, error) {
		return user.Sign(nonce)
	}
	opts.Nkeys = append(opts.Nkeys, &server.NkeyUser{
		Nkey: publicKey,
		Permissions: &server.Permissions{
			Publish: &server.SubjectPermission{
				Allow: []string{">"},
				Deny:  nil,
			},
			Subscribe: &server.SubjectPermission{
				Allow: []string{">"},
				Deny:  nil,
			},
		},
	})
	pretty.Println(opts)
	s, err := server.NewServer(opts)
	if err != nil {
		log.Fatal(err)
	}
	go s.Start()
	if s.ReadyForConnections(1 * time.Second) {
		log.Println("Ready for connections")
	} else {
		log.Fatal("Not ready for connections")
	}
	nc, err := nats.Connect("nats://localhost:4222", nats.Name("nats-test"), nats.UserInfo("admin", "admin"))
	if err != nil {
		log.Fatal("connect ", err)
	}
	nc.Subscribe("checkin.*", func(m *nats.Msg) {
		log.Println("Received checkin")
		m.Respond([]byte("OK"))
	})
	nc.Subscribe("login.*", func(m *nats.Msg) {
		log.Println("Received login request")
		m.Respond([]byte("OK"))
	})
	opt2 := nats.Options{
		Url:         "nats://localhost:4222",
		Nkey:        publicKey,
		Name:        "nats-test-nkey",
		SignatureCB: sign,
	}
	nc2, err := opt2.Connect()
	if err != nil {
		log.Fatal("nkey connect ", err)
	}
	nc2.Subscribe("checkin.*", func(m *nats.Msg) {
		log.Println("Received checkin on nkey channel")
		m.Respond([]byte("OK nkey"))
	})
	s.WaitForShutdown()
}
