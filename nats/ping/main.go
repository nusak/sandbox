package main

import (
	"errors"
	"fmt"
	"time"

	"github.com/devilcove/plexus"
	"github.com/nats-io/nats.go"
)

func main() {
	nc, _ := nats.Connect("nats://localhost:4223")
	ec, _ := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	defer ec.Close()
	resp := plexus.PingResponse{}
	if err := ec.Request("2vOXL4fM1NmkSqcou8Ly+tJbAJ4N2nkTecaYSn8SIms=",
		plexus.UpdateRequest{Action: plexus.Ping}, &resp, time.Second*2); err != nil {
		if errors.Is(err, nats.ErrTimeout) {
			fmt.Println("Request timed out")
		} else {
			fmt.Println("Error publishing:", err)
		}
	}
	fmt.Println("resp", resp)

	fmt.Println(plexus.NetworkUpdate{Type: plexus.Ping})

}
