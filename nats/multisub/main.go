package main

import (
	"fmt"
	"time"

	"github.com/nats-io/nats-server/v2/server"
	"github.com/nats-io/nats.go"
)

type general interface{}

type hello struct {
	Message string
}

type bar struct {
	Message string
	Number  int
}

func main() {
	ns, _ := server.NewServer(&server.Options{})
	go ns.Start()
	if !ns.ReadyForConnections(3 * time.Second) {
		panic("Server is not ready for connections")
		return
	}
	nc, _ := nats.Connect(nats.DefaultURL)
	ec, _ := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	ec.Subscribe("foo.>", func(subj, reply string, msg *general) {
		fmt.Printf("foo.> Received a message: %s\n", subj[4:])
		fmt.Println(*msg)
		data := make(map[string]interface{})
		tmp := *msg
		data = tmp.(map[string]interface{})
		fmt.Println("message:", data["Message"])
		fmt.Println("number:", data["Number"])
	})
	ec.Subscribe("foo.hello", func(subj, reply string, msg *hello) {
		fmt.Printf("hello Received a message: %s len: %d\n", msg.Message, len(msg.Message))
	})
	ec.Subscribe("foo.bar", func(subj, reply string, msg *bar) {
		fmt.Printf("bar Received a message: %s %d\n", msg.Message, msg.Number)
	})
	ec.Subscribe("*.device", func(subj, reply string, msg *hello) {
		fmt.Printf("Received a message: topic %s, message %s", subj[45:], msg.Message)
	})
	for {
	}

}
