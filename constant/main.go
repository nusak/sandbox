package main

import (
	"errors"
	"fmt"
	"time"
)

var (
	time1        = time.Second * 10
	time2        = time.Minute * 10
	ErrServerURL = errors.New("invalid server url")
)

const (
	time3 = time.Second * 10
	time4 = time.Minute * 10
)

func main() {
	fmt.Println(time1, time2, time3, time4)
	fmt.Println(ErrServerURL)
	err := ErrServerURL
	fmt.Println(err)
	junk := true
	fmt.Println(junk)
}
