package main

import (
	"encoding/json"
	"errors"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"time"

	"github.com/juju/mutex"
)

type netclientClock struct {
	delay time.Duration
}

func (n *netclientClock) After(time.Duration) <-chan time.Time {
	return time.After(n.delay)
}
func (n *netclientClock) Now() time.Time {
	return time.Now()
}

const TIMEOUT = time.Second * 5

var someChan = make(chan struct{})

var spec = mutex.Spec{
	Name:  "netclient",
	Clock: &netclientClock{},
	Delay: time.Millisecond,
	//Timeout: time.Second * 10,
	Cancel: someChan,
}

func main() {
	wg := sync.WaitGroup{}
	if err := spec.Validate(); err != nil {
		log.Fatal(err)
	}
	for i := 1; i < 10; i++ {
		wg.Add(1)
		go Write(&wg, i)
	}
	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		for {
			r, err := mutex.Acquire(spec)
			if err != nil {
				panic(err)
			}
			bytes, _ := os.ReadFile("/tmp/junk")
			log.Println(string(bytes))
			r.Release()
		}
		wg.Done()
	}(&wg)
	if err := Write(&wg, 10); err != nil {
		log.Fatal(err)
	}
	wg.Wait()
	log.Println("write successful")
}

func Write(wg *sync.WaitGroup, i int) error {
	defer wg.Done()
	r, err := mutex.Acquire(spec)
	if err != nil {
		return err
	}
	defer r.Release()
	//simulate long write time
	os.WriteFile("/tmp/junk", []byte(strconv.Itoa(i)), os.ModePerm)
	//time.Sleep(time.Second * 1)
	log.Println("wrote ", i)
	return nil
}

func Lock() error {
	pid := os.Getpid()
	start := time.Now()
	lockfile := filepath.Join(os.TempDir(), "lockfile")
	log.Println("lock try")
	for {
		if _, err := os.Stat(lockfile); !errors.Is(err, os.ErrNotExist) {
			log.Println("file exists")
		} else {
			bytes, _ := json.Marshal(pid)
			if err := os.WriteFile(lockfile, bytes, os.ModePerm); err == nil {
				log.Println("file locked")
				return nil
			} else {
				log.Println("unable to write")
			}
		}
		log.Println("unable to get lock")
		if time.Since(start) > TIMEOUT {
			return errors.New("TIMEOUT")
		}
		time.Sleep(time.Millisecond * 100)
	}
}

func UnLock() error {
	var pid int
	start := time.Now()
	lockfile := filepath.Join(os.TempDir(), "lockfile")
	log.Println("unlock try")
	for {
		bytes, err := os.ReadFile(lockfile)
		if err != nil {
			if errors.Is(err, os.ErrNotExist) {
				return nil
			}
			log.Println("error reading file")
			return err
		}
		log.Println("lockfile exists")
		if err := json.Unmarshal(bytes, &pid); err == nil {
			if pid == os.Getpid() {
				if err := os.Remove(lockfile); err == nil {
					log.Println("removed lockfile")
					return nil
				} else {
					log.Println("error removing file", err)
				}
			} else {
				log.Println("wrong pid")
			}
		} else {
			log.Println("unmarshal err ", err)

		}
		log.Println("unable to unlock")
		if time.Since(start) > TIMEOUT {
			return errors.New("TIMEOUT")
		}
		time.Sleep(time.Millisecond * 100)
	}
}
