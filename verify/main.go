package main

import "log"

func main() {
	log.Println(verify("en"))
	log.Println(verify("fr"))
	log.Println(verify("gr"))
	log.Println(verify(""))

}

func verify(lang string) bool {
	if lang != "en" && lang != "fr" {
		return false
	}
	return true
}
