package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gravitl/netmaker/models"
	"github.com/kr/pretty"
)

var SigningKey []byte

func SetupRouter() *gin.Engine {
	ctx.Endpoint = "https://api.clustercat.com/"
	ctx.Masterkey = "secretkey"

	router := gin.Default()
	router.POST("/api/v1/enrollment-keys", createKey)
	router.GET("/api/v1/enrollment-keys", getKey)
	router.POST("/api/v1/host/register/:token", register)
	router.POST("/api/nodes/:network", join)
	return router
}

func join(c *gin.Context) {
	log.Println("join called")
	network := c.Param("network")
	payload := models.JoinData{}
	if err := c.BindJSON(&payload); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "invalid data " + err.Error()})
		log.Println("bind error ", err)
		return
	}
	ctx.Masterkey = "Bearer " + payload.Key
	ctx.Headers = []Header{
		{
			Name:  "requestfrom",
			Value: "node",
		},
	}
	response, err := callapi[models.NodeJoinResponse](http.MethodPost, "api/nodes/"+network, payload)
	if err != nil {
		log.Println("err from server ", err)
		c.JSON(http.StatusBadRequest, gin.H{"message": "invalid data " + err.Error()})
		return
	}
	pretty.Println(response)
	c.JSON(http.StatusOK, response)

}

func delete(c *gin.Context) {
	network := c.Param("network")
	id := c.Param("id")
	response, err := callapi[models.EnrollmentKey](http.MethodDelete, "api/nodes/"+network+"/"+id, nil)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "invalid data " + err.Error()})
		return
	}
	c.JSON(http.StatusOK, response)
}

func createKey(c *gin.Context) {
	var request models.APIEnrollmentKey
	if err := c.BindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "invalid data " + err.Error()})
		return
	}
	response, err := callapi[models.EnrollmentKey](http.MethodGet, "api/v1/enrollement-keys", request)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "invalid data " + err.Error()})
		return
	}
	c.JSON(http.StatusOK, response)
}

func getKey(c *gin.Context) {
	response, err := callapi[models.EnrollmentKey](http.MethodGet, "api/v1/enrollement-keys", nil)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "invalid data " + err.Error()})
		return
	}
	c.JSON(http.StatusOK, response)
}

func register(c *gin.Context) {
	token := c.Param("token")
	response, err := callapi[models.EnrollmentKey](http.MethodGet, "api/v1/host/register/"+token, nil)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "invalid data " + err.Error()})
		return
	}
	c.JSON(http.StatusOK, response)
}

func callapi[T any](method, route string, payload any) (*T, error) {
	var (
		req *http.Request
		err error
	)
	if payload == nil {
		req, err = http.NewRequest(method, ctx.Endpoint+route, nil)
		if err != nil {
			return nil, err
		}
	} else {
		payloadBytes, jsonErr := json.Marshal(payload)
		if jsonErr != nil {
			return nil, err
		}
		req, err = http.NewRequest(method, ctx.Endpoint+route, bytes.NewReader(payloadBytes))
		if err != nil {
			return nil, err
		}
		req.Header.Set("Content-Type", "application/json")
	}
	req.Header.Set("Authorization", "Bearer "+ctx.Masterkey)
	for _, header := range ctx.Headers {
		req.Header.Set(header.Name, header.Value)
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	resBodyBytes, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("http status %d %s", res.StatusCode, res.Status)
	}
	body := new(T)
	if len(resBodyBytes) > 0 {
		if err := json.Unmarshal(resBodyBytes, body); err != nil {
			log.Println("json err unmarshalling server response", string(resBodyBytes), err.Error())
			return nil, err
		}
	}
	return body, nil
}
