package main

import (
	"log"
)

func main() {
	//config, err := Get()
	//if err != nil {
	//log.Fatal(err)
	//}
	//log.Println("using config")
	//pretty.Println(config)

	router := SetupRouter()
	router.Run("127.0.0.1:8090")

	log.Println("shutting down daemon")
	log.Println("shutdown complete")
}
