package main

import (
	"net"

	"github.com/gravitl/netmaker/models"
	"github.com/spf13/viper"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

type Config struct {
	models.Host
	PrivateKey        wgtypes.Key
	TrafficKeyPrivate []byte
	InterneGateway    net.UDPAddr
	HostPeers         map[string][]wgtypes.PeerConfig
}

type Endpoint struct {
	Endpoint  string
	Masterkey string
	Headers   []Header
}

type Header struct {
	Name  string
	Value string
}

var cached *Config
var ctx Endpoint

// FromFile reads a configuration file called conf.yml and returns it as a
// Config instance. If no configuration file is found, nil and no error will be
// returned. The configuration must live in one of the following directories:
//
//   - /etc/golfballs
//   - $HOME/.golfballs
//   - .
//
// In case multiple configuration files are found, the one in the most specific
// or "closest" directory will be preferred.
func FromFile() (*Config, error) {
	viper.SetConfigName("netclient")
	viper.SetConfigType("yml")
	viper.AddConfigPath("/etc/netclient/")
	viper.AddConfigPath("$HOME/.config/netclient")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			return nil, err
		}
	}

	var config Config

	if err := viper.Unmarshal(&config); err != nil {
		return nil, err
	}

	cached = &config

	return cached, nil
}

// Get returns the parsed configuration. The fields of this configuration either
// contain values specified by the user or the zero value of the respective data
// type, e.g. "" for an un-configured string.
//
// Using Get over FromFile avoids the config file from being parsed each time
// the config is needed.
func Get() (*Config, error) {
	if cached != nil {
		return cached, nil
	}

	config, err := FromFile()
	if err != nil {
		return &Config{}, err
	}

	return config, nil
}
