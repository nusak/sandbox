package main

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

func main() {
	//hash := "$2a$05$zdPxGjlGw/AHzah.LCzQxe7WwJjEmeDYo.VUWmdhPjQgtMO8UDQBa"
	//pass := "5yia2n6BFAWmzp3KdLNTY5IcNtWz3TZDpNgAXKYThxUmiTWWhvX4KBlmSJiYq0l1"
	//if err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(pass)); err != nil {
	//	log.Fatal(err)
	//}
	log.Println("success")

	blank := ""
	newhash, err := bcrypt.GenerateFromPassword([]byte(blank), 5)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(newhash))
}
