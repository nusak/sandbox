package main

import (
	"fmt"
	"log"
	"os"

	"github.com/kr/pretty"
)

func main() {
	file, err := os.CreateTemp("", "xxx")
	if err != nil {
		log.Fatal(err)
	}
	pretty.Println(file)
	defer os.Remove(file.Name())
	defer file.Close()

	fmt.Println("\t", "hello")
	fmt.Println("\t", "world")
	fmt.Print("\t", "world", "\n")
	fmt.Println("")
	fmt.Print("        ", "world")
}
