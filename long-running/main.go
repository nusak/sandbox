package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-contrib/timeout"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/ping", timeout.New(
		timeout.WithTimeout(4*time.Second),
		timeout.WithResponse(errorResponse),
		timeout.WithHandler(func(c *gin.Context) {
			signal := make(chan struct{}, 1)
			abort := make(chan string, 1)
			ctx, cancel := context.WithTimeout(c.Request.Context(), time.Second*5)

			go longRunningOperation(c, ctx, signal, abort)

			select {
			case <-signal:
				close(signal)
				close(abort) // remember to clean up after yourself
				// move on, will print "Processing"

			case err := <-abort:
				c.JSON(500, gin.H{
					"message": err,
				})
				close(abort)
				close(signal)
				return
			case <-c.Request.Context().Done():
				// abort
				log.Println("abort")
				cancel()
				return
			}

			log.Print("Processing")
			c.JSON(200, gin.H{
				"message": "pong",
			})
		}),
	))
	r.Run()
}

func longRunningOperation(c *gin.Context, ctx context.Context, signal chan<- struct{}, abort chan<- string) {
	//ticker := time.NewTicker(10 * time.Second)

	select {
	case <-c.Done():
		fmt.Println("cancel received", c.Err())
		return
		//case <-ticker.C:
	default:
		for i := 0; i < 10; i++ {
			//	select {
			//	case <-ctx.Done():
			//		fmt.Println("cancel in loop", ctx.Err())
			//		abort <- ctx.Err().Error()
			//		return
			//	default:
			time.Sleep(time.Second)
			fmt.Println("tick", i)
			//	}
		}
		signal <- struct{}{} // signal that this operation has finished
		return
	}
}

func errorResponse(c *gin.Context) {
	c.String(http.StatusRequestTimeout, "timeout middleware")
}
