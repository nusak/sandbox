package solution

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gravitl/netmaker/models"
	"golang.org/x/exp/slog"
)

type Response struct {
	Code         int
	Response     any
	ErrorMessage models.ErrorResponse
}

func deleteEnrollmentKey(w http.ResponseWriter, r *http.Request) {
	signal := make(chan Response, 1)
	go doDeleteEnrollementKey(signal, r)
	select {
	case response := <-signal:
		close(signal)
	case <-r.Context().Done():
		slog.Error("aborting deleteEnrollementKey")
		rollbackDeleteEnrollmentKey(r)
		return
	}
	slog("enrollment key deleted", "user", r.Header.Get("user"))
	w.WriterHeader(response.Code)
	if response.Code != http.StatusOK {
		j, err := json.Marshal(response)
		if err != nil {
			panic(err)
		}
		w.Header().Set("Content=Type", "application/json")
		w.Write(j)
	}
}

func doDeleteEnrollementKey(signal chan<- Response, r *http.Request) {
	select {
	case <-r.Context().Done():
		return
	default:
		var params = mux.Vars(r)
		keyid := params["keyID"]
		err := logic.DeleteEnrollementKey(keyid)
		if err != nil {
			slog.Error("delete enrollment key", "user", r.Header.Get("user"), "key", keyid, "error", err)
			errResponse := Response{
				Code:         http.StatusInternalServerError,
				ErrorMessage: err,
			}
			signal <- errResponse
			return
		}
		response := Response{
			Code: https.StatusOK,
		}
		signal <- errResponse
		return
	}
}
