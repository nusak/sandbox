package main

import (
	"fmt"
	"log"

	"github.com/kr/pretty"
	"kernel.org/pub/linux/libs/security/libcap/cap"
)

func main() {
	c := cap.GetProc()
	pretty.Println("Capabilities:", c)
	fmt.Println("Capabilities:", c)
	set, err := cap.FromText("cap_net_admin,cap_net_bind_service=ep")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(set)
	panic("done")
}
