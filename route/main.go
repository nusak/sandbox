package main

import (
	"log"
	"net"

	"github.com/kr/pretty"
	"github.com/vishvananda/netlink"
)

func main() {
	google := net.IPv4(8, 8, 8, 8)
	routes, err := netlink.RouteGet(google)
	if err != nil {
		log.Fatal("routeget", err)
	}

	pretty.Println(routes)
	links, err := netlink.LinkList()
	if err != nil {
		log.Fatal("linklist", err)
	}
	index := 0
	for i, link := range links {
		if link.Attrs().Index == routes[0].LinkIndex {
			pretty.Println(link.Attrs().Name)
			index = i
		}
	}
	route := routes[0].String()
	pretty.Println(route)
	pretty.Println()
	routes, err = netlink.RouteList(links[index], 1)
	if err != nil {
		log.Fatal("routelist", err)
	}
	for _, route := range routes {
		//log.Println(route.String())
		if route.Dst == nil {
			//if route.Src
			log.Println("\ndefault route", route.String(), "\n")
		}
	}
	link, err := netlink.LinkByName("netmaker")
	dst := &net.IPNet{
		IP:   net.IPv4(192, 168, 100, 0),
		Mask: net.CIDRMask(24, 32),
	}
	ip := net.IPv4(10, 41, 238, 2)
	route2 := netlink.Route{
		LinkIndex: link.Attrs().Index,
		Dst:       dst,
		Src:       ip,
	}
	if err := netlink.RouteAdd(&route2); err != nil {
		log.Fatal("error adding route ", err)
	}

}
