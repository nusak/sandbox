package main

import (
	"fmt"
	"log"
	"net"

	"github.com/kr/pretty"
	"github.com/vishvananda/netlink"
)

func main() {
	//ip := net.ParseIP("192.168.1.1")
	ip := net.ParseIP("8.8.8.8")
	routes, err := netlink.RouteGet(ip)
	if err != nil {
		log.Fatal(err)
	}
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Fatal(err)
	}
	for _, route := range routes {
		fmt.Println(route.String(), route.LinkIndex, route.ILinkIndex)
		pretty.Println(route)
		fmt.Println(ifaces[route.LinkIndex-1])
		for _, iface := range ifaces {
			if iface.Index == route.LinkIndex {
				pretty.Println(iface)
			}
		}
	}
	//pretty.Println(ifaces)

	//links, err := netlink.LinkList()
	//if err != nil {
	//	log.Fatal("linklist", err)
	//}
	//handle, err := netlink.NewHandle()
	//if err != nil {
	//	log.Fatal("newhandle", err)
	//}
	//pretty.Println(handle)
}
