package main

import (
	"bytes"
	"encoding/json"
	"fmt"
)

func main() {
	var metrics struct{}
	w := new(bytes.Buffer)
	json.NewEncoder(w).Encode(metrics)

	fmt.Println(w)
}
