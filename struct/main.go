package main

import (
	"fmt"
)

type API struct {
	Name   string
	Number int
}

func main() {
	api := []API{{Name: "junk"}}
	if api == nil {
		fmt.Println("api is nil")
	}
	if len(api) > 0 {
		fmt.Println("data avail")
	}
	fmt.Println(api)
}
