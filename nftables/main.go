package main

import (
	"fmt"
	"log"

	"github.com/google/nftables"
	"github.com/kr/pretty"
)

func main() {
	c := &nftables.Conn{}
	tables, err := c.ListTables()
	if err != nil {
		log.Fatalf("failed to list tables: %v", err)
	}
	for _, table := range tables {
		if table.Name != "plexus" || table.Family != nftables.TableFamilyIPv4 {
			continue
		}
		//if table.Family != nftables.TableFamilyIPv4 {
		//	continue
		//}
		log.Printf("table: %s:%d", table.Name, table.Family)
		//sets, err := c.GetSets(table)
		//if err != nil {
		//	log.Fatalf("failed to list sets: %v", err)
		//}
		//if len(sets) > 0 {
		//	log.Printf("sets:")
		//}
		//for _, set := range sets {
		//	pretty.Println(set)
		//}

		chains, err := c.ListChains()
		if err != nil {
			log.Fatalf("failed to list chains: %v", err)
		}
		for _, chain := range chains {
			if chain.Name != "plexus" && chain.Name != "plexus-nat" {
				continue
			}
			log.Printf("  chain: %s", chain.Name)
			pretty.Println("chain: ", chain)
			rules, err := c.GetRules(table, chain)
			if err != nil {
				log.Fatalf("failed to list rules: %v", err)
			}
			for _, rule := range rules {
				pretty.Println("rule: ", rule)
				fmt.Println("rule: ", rule)
				for _, expr := range rule.Exprs {
					fmt.Println("expr", expr)
				}
				//log.Printf("    rule: %d", rule.Handle)
				//for _, expr := range rule.Exprs {
				//	log.Printf("      expr: %s", expr)
				//}
			}
		}
	}
}
