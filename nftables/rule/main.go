package main

import (
	"net"
	"runtime/debug"

	"github.com/c-robinson/iplib"
	"github.com/google/nftables"
	"github.com/google/nftables/expr"
	"github.com/kr/pretty"
)

func main() {
	debug.SetTraceback("single")
	//c, err := nftables.New()
	//if err != nil {
	//	panic(err)
	//}
	c := &nftables.Conn{}
	plexusTable := c.AddTable(&nftables.Table{
		Name:   "plexus",
		Family: nftables.TableFamilyIPv4,
	})
	//if err := c.Flush(); err != nil {
	//	panic(err)
	//}
	plexusChain := c.AddChain(&nftables.Chain{
		Name:     "plexus",
		Table:    plexusTable,
		Type:     nftables.ChainTypeNAT,
		Hooknum:  nftables.ChainHookPrerouting,
		Priority: nftables.ChainPriorityFilter,
	})
	//if err := c.Flush(); err != nil {
	//	panic(err)
	//}
	subnet := iplib.NewNet4(net.ParseIP("10.10.10.0"), 24)
	virtnet := iplib.NewNet4(net.ParseIP("10.100.0.0"), 24)
	sub := subnet.FirstAddress()
	virt := virtnet.FirstAddress()
	rule := &nftables.Rule{
		Table: plexusTable,
		Chain: plexusChain,
		//Position: 1,
		Exprs: []expr.Any{
			&expr.Payload{
				OperationType:  expr.PayloadLoad,
				SourceRegister: 0,
				DestRegister:   1,
				Base:           expr.PayloadBaseNetworkHeader,
				Offset:         0x10,
				Len:            4,
			},
			&expr.Cmp{
				Op:       expr.CmpOpEq,
				Register: 1,
				Data:     virt,
			},
			&expr.Immediate{
				Register: 1,
				Data:     sub,
			},
			&expr.NAT{
				Type:        expr.NATTypeDestNAT,
				Family:      uint32(nftables.TableFamilyIPv4),
				RegAddrMin:  1,
				RegAddrMax:  1,
				RegProtoMin: 0,
				RegProtoMax: 0,
			},
		},
		UserData: []byte("plexus"),
	}
	pretty.Println(rule)
	c.AddRule(rule)
	if err := c.Flush(); err != nil {
		panic(err)
	}
	for {
		var err error
		sub, err = subnet.NextIP(sub)
		if err != nil {
			break
		}
		virt, err = virtnet.NextIP(virt)
		if err != nil {
			break
		}
		rule.Exprs[1].(*expr.Cmp).Data = virt
		rule.Exprs[2].(*expr.Immediate).Data = sub
		c.AddRule(rule)
		if err := c.Flush(); err != nil {
			panic(err)
		}
	}
	//cmd := exec.Command("nft", "add", "rule", "ip", "nat", "plexus", "ip", "daddr", "10.200.0.7", "dnat", "to", "10.225.211.7")
	//out, err := cmd.CombinedOutput()
	//if err != nil {
	//	log.Println(err)
	//}
	//log.Println(string(out))
}
