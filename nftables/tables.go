package main

import (
	"log"

	"github.com/google/nftables"
	"github.com/google/nftables/expr"
	"github.com/kr/pretty"
)

func main() {
	forwardChain := nftables.Chain{}
	c := &nftables.Conn{}
	//tables, err := c.ListTables()
	//if err != nil {
	//	log.Fatal(err)
	//}
	//for _, table := range tables {
	//	pretty.Println(table)
	//}
	chains, err := c.ListChains()
	if err != nil {
		log.Fatal(err)
	}
	for _, chain := range chains {
		if chain.Name == "FORWARD" && chain.Table.Family == nftables.TableFamilyIPv4 {
			forwardChain = *chain
			pretty.Println(chain)
		}
	}
	//rule := c.AddRule(&nftables.Rule{
	rule := c.AddRule(&nftables.Rule{
		Table: forwardChain.Table,
		Chain: &forwardChain,
		Exprs: []expr.Any{
			&expr.Meta{Key: expr.MetaKeyIIFNAME, Register: 1},
			&expr.Cmp{
				Op:       expr.CmpOpEq,
				Register: 1,
				Data:     []byte("netmaker" + "\x00"),
			},
			&expr.Verdict{Kind: expr.VerdictJump, Chain: "netmakerfilter"},
		},
		//UserData: []byte("-i:netmaker:-j:netmakerfilter"),
	})
	pretty.Println(rule)
	if err := c.Flush(); err != nil {
		log.Fatal("c.Flush: ", err)
	}
	log.Println("done")
	rules, err := c.GetRules(forwardChain.Table, &forwardChain)
	if err != nil {
		log.Fatal(err)
	}
	for _, rule := range rules {
		pretty.Println(rule)
	}
}
