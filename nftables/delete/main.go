package main

import (
	"fmt"
	"log"

	"github.com/google/nftables"
)

func main() {
	c := nftables.Conn{}
	tables, err := c.ListTables()
	if err != nil {
		log.Fatal(err)
	}
	for _, table := range tables {
		fmt.Println(table.Name)
	}
	c.FlushRuleset()
	if err := c.Flush(); err != nil {
		log.Fatal(err)
	}
	tables, err = c.ListTables()
	if err != nil {
		log.Fatal(err)
	}
	for _, table := range tables {
		fmt.Println(table.Name)
		c.FlushTable(table)
	}
	if err := c.Flush(); err != nil {
		log.Fatal(err)
	}

}
