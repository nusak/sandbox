package main

import (
	"runtime/debug"

	"github.com/google/nftables"
	"github.com/google/nftables/expr"
	"github.com/kr/pretty"
)

func main() {
	debug.SetTraceback("single")
	//c, err := nftables.New()
	//if err != nil {
	//	panic(err)
	//}
	c := &nftables.Conn{}
	plexusTable := c.AddTable(&nftables.Table{
		Name:   "plexus",
		Family: nftables.TableFamilyIPv4,
	})
	//if err := c.Flush(); err != nil {
	//	panic(err)
	//}
	plexusChain := c.AddChain(&nftables.Chain{
		Name:     "plexus-nat",
		Table:    plexusTable,
		Type:     nftables.ChainTypeNAT,
		Hooknum:  nftables.ChainHookPostrouting,
		Priority: nftables.ChainPriorityNATSource,
	})
	//if err := c.Flush(); err != nil {
	//	panic(err)
	//}
	rule := &nftables.Rule{
		Table: plexusTable,
		Chain: plexusChain,
		//Position: 1,
		Exprs: []expr.Any{
			&expr.Masq{},
		},
		UserData: []byte("comment: plexus"),
	}

	pretty.Println(rule)
	ruleX := c.AddRule(rule)
	pretty.Println(ruleX)
	if err := c.Flush(); err != nil {
		panic(err)
	}
	//cmd := exec.Command("nft", "add", "rule", "ip", "nat", "plexus", "ip", "daddr", "10.200.0.7", "dnat", "to", "10.225.211.7")
	//out, err := cmd.CombinedOutput()
	//if err != nil {
	//	log.Println(err)
	//}
	//log.Println(string(out))
}
