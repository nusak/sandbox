package main

import (
	"fmt"
	"log"
	"net"

	"github.com/google/nftables"
	"github.com/google/nftables/expr"
	"github.com/kr/pretty"
)

func main() {
	c := &nftables.Conn{}

	// Lets create our table first.
	myTable := c.AddTable(&nftables.Table{
		Family: nftables.TableFamilyIPv4,
		Name:   "myFilter",
	})

	// Now, we create a chain which we will add our filter
	// rules to.
	myChain := c.AddChain(&nftables.Chain{
		Name:     "myChain",
		Table:    myTable,
		Type:     nftables.ChainTypeFilter,
		Hooknum:  nftables.ChainHookInput,
		Priority: nftables.ChainPriorityFilter,
	})

	// Lets add a rule to the chain that loads the source
	// address, and compares it to a hardcoded IP.
	rule := &nftables.Rule{
		Table: myTable,
		Chain: myChain,
		Exprs: []expr.Any{
			// payload load 4b @ network header + 12 => reg 1
			&expr.Payload{
				DestRegister: 1,
				Base:         expr.PayloadBaseNetworkHeader,
				Offset:       12,
				Len:          4,
			},
			// cmp eq reg 1 0x0245a8c0
			&expr.Cmp{
				Op:       expr.CmpOpEq,
				Register: 1,
				Data:     net.ParseIP("192.168.1.6").To4(),
			},
			// [ immediate reg 0 drop ]
			&expr.Verdict{
				Kind: expr.VerdictDrop,
			},
		},
	}
	pretty.Println(rule)
	fmt.Println(rule.Table.Name)
	fmt.Println(rule.Chain.Table.Name)
	c.AddRule(rule)

	// Apply the above (commands are queued till a call to Flush())
	if err := c.Flush(); err != nil {
		// handle error
		log.Fatalf("Failed to flush rules: %v", err)
	}
}
