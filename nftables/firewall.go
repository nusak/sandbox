package functions

import (
	"github.com/google/nftables"
	"github.com/google/nftables/expr"
	"github.com/gravitl/netclient/config"
	"github.com/gravitl/netmaker/logger"
	"github.com/gravitl/netmaker/models"
)

func setupIngress() {
	if config.Netclient().FirewallInUse != models.FIREWALL_IPTABLES || config.Netclient().FirewallInUse != models.FIREWALL_NFTABLES {
		logger.Log(1, "unsupported filewall", config.Netclient().FirewallInUse, "...skipping")
		return
	}
	nodes := config.GetNodes()
	for _, node := range nodes {
		if !node.IsIngressGateway {
			continue
		}
		if config.Netclient().FirewallInUse == models.FIREWALL_NFTABLES {
			setupNFTIngress(&node)
		} else {
			setupIPTIngress(&node)
		}
	}
}

func setupNFTIngress(node *config.Node) {
	c := &nftables.Conn{}
	netmakerTable := c.AddTable(&nftables.Table{
		Family: nftables.TableFamilyIPv4,
		Name: "netmaker",
	})
	netmakerChain := c.AddChain(&nftables.Chain{
		Name: "netmaker",
		Table: netmakerTable,
		Type: nftables.ChainTypeFilter,
		Hooknum: nftables.ChainHookInput,
		Priority: nftables.ChainPriorityFilter,
	})
	c.AddRule(&nftables.Rule{
		Table: netmakerTable,
		Chain: netmakerChain,
		Exprs: []expr.Any{
			DestRegister; 1,
			Base: expr.PayloadBaseNetworkHeader,
			Offset: 12,
			Len: 4,
		},
		&expr.Cmp{
			Op: expr.CompOpEq,
			Register: 1,
			Data

		}
	})

}
