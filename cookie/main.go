package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
)

func main() {
	client := &http.Client{}
	postData := []byte(`{"username":"admin","password":"password"}`)
	req, err := http.NewRequest("POST", "http://localhost:8080/login", bytes.NewBuffer(postData))
	if err != nil {
		panic(err)
	}
	response, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	for _, c := range response.Cookies() {
		if c.Name == "time" {
			cookie, err := json.Marshal(*c)
			if err != nil {
				panic(err)
			}
			if err := os.WriteFile("/tmp/cookie.timetrace", cookie, 0644); err != nil {
				panic(err)
			}
		}
	}
	cookie := getCookie()
	fmt.Println("Cookie: ", cookie)
	req, err = http.NewRequest("GET", "http://localhost:8080/projects/status", nil)
	req.AddCookie(&cookie)
	response, err = client.Do(req)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	fmt.Println("Response: ", response)

}

func getCookie() http.Cookie {
	cookie := http.Cookie{}
	file, err := os.ReadFile("/tmp/cookie.timetrace")
	if err != nil {
		panic(err)
		return cookie
	}
	if err := json.Unmarshal(file, &cookie); err != nil {
		panic(err)
		return cookie
	}
	return cookie
}
