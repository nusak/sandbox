// Package config provides functions for reading the config.
package config

import (
	"github.com/spf13/viper"
)

type Config struct {
	DB               string           `yaml:"db"`
	DBFile           string           `yaml:"dbfile"`
	DBPath           string           `yaml:"dbpath"`
	CertIssuer       string           `yaml:"certissuer"`
	ImagePath        string           `yaml:"imagepath"`
	Bambora          BamboraSecrets   `yaml:"bambora"`
	Connect          ConnectSecrets   `yaml:"connect"`
	Riskified        RiskifiedSecrets `yam:"riskified"`
	Debug            bool             `yaml:"debug"`
	Production       bool             `yaml:"production"`
	Email            EmailConfig      `yaml:"email"`
	ProductionEmails []string         `yaml:"productionemails"`
	OrderEmails      []string         `yaml:"orderemails"`
}

type EmailConfig struct {
	Token      string `yaml:"token"`
	Account    string
	SMTPServer string
	Port       int
	From       string
}

type RiskifiedSecrets struct {
	ShopDomain string `yaml:"shopdomain"`
	Token      string `yaml:"token"`
	URL        string `yaml:"url"`
	Production bool   `yaml:"production"`
}
type ConnectSecrets struct {
	Username string `yaml:"username"`
	Password string `yaml:"password"`
}

type BamboraSecrets struct {
	MerchantID string `yaml:"merchantid"`
	Apikey     string `yaml:"apikey"`
}

// SQLConfig - Generic SQL Config
type SQLConfig struct {
	Host     string `yaml:"host"`
	Port     int32  `yaml:"port"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	DB       string `yaml:"db"`
	SSLMode  string `yaml:"sslmode"`
}

var cached *Config

// FromFile reads a configuration file called conf.yml and returns it as a
// Config instance. If no configuration file is found, nil and no error will be
// returned. The configuration must live in one of the following directories:
//
//   - /etc/golfballs
//   - $HOME/.golfballs
//   - .
//
// In case multiple configuration files are found, the one in the most specific
// or "closest" directory will be preferred.
func FromFile() (*Config, error) {
	viper.SetConfigName("conf")
	viper.SetConfigType("yml")
	viper.AddConfigPath("/mnt/golf/")
	viper.AddConfigPath("$HOME/.golfballs")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			return nil, err
		}
	}

	var config Config

	if err := viper.Unmarshal(&config); err != nil {
		return nil, err
	}

	cached = &config

	return cached, nil
}

// Get returns the parsed configuration. The fields of this configuration either
// contain values specified by the user or the zero value of the respective data
// type, e.g. "" for an un-configured string.
//
// Using Get over FromFile avoids the config file from being parsed each time
// the config is needed.
func Get() (*Config, error) {
	if cached != nil {
		return cached, nil
	}

	config, err := FromFile()
	if err != nil {
		return &Config{}, err
	}

	return config, nil
}
