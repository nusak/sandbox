package main

import (
	"github.com/kr/pretty"
	"gitlab.com/nusak/sandbox/email/gomail/config"
	"gopkg.in/gomail.v2"
)

func main() {
	config, err := config.Get()
	if err != nil {
		panic(err)
	}
	msg := gomail.NewMessage()
	msg.SetHeader("From", msg.FormatAddress(config.Email.From, "Golf Town Ball Customizer"))

	msg.SetHeader("To", config.ProductionEmails...)
	msg.SetHeader("Subject", "New Order Added To Your Production Queue")
	msg.SetBody("text/html", `<p>Hello</p>
	<p>A new PZ order has been moved to your Production Queue.<br>
	Login here: https://customgolfballs.golftown.com/console</p>
	<p>Thank you</p>`)
	pretty.Println(msg)

	n := gomail.NewDialer(config.Email.SMTPServer, config.Email.Port, config.Email.Account, config.Email.Token)
	//Send the email
	if err := n.DialAndSend(msg); err != nil {
		panic(err)
	}
}
