package main

import (
	"log"
	"os"

	"net/smtp"
)

func main() {

	// Choose auth method and set it up

	auth := smtp.PlainAuth("", "mkasun@nusak.ca", os.Getenv("EMAIL_PASS"), "smtp.zoho.com")

	// Here we do it all: connect to our server, set up a message and send it

	to := []string{"mkasun@gmail.com"}

	msg := []byte("smtp test email\r\n" +

		"Subject: smtp test email\r\n" +

		"\r\n" +

		"This is a test message\r\n")

	err := smtp.SendMail("smtp.zoho.com:587", auth, "mkasun@nusak.ca", to, msg)

	if err != nil {

		log.Fatal(err)

	}

}
