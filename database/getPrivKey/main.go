package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	"github.com/kr/pretty"
	_ "github.com/mattn/go-sqlite3" // need to blank import this package
)

func main() {
	type Key struct {
		PrivateKey string `json:"privatekey"`
	}
	var key Key
	var value string
	id := "4150324f-bb5b-456f-b644-4a82e8d96024"
	db, err := sql.Open("sqlite3", "/tmp/netmaker.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	//row, err := db.Query("SELECT VALUE from serverconf where key='" + id + "'")
	row, err := db.Query("SELECT VALUE from serverconf where key='" + id + "'")
	if err != nil {
		log.Fatal(err)
	}
	row.Next()
	row.Scan(&value)
	log.Println("key-value", id, value)
	//if err := json.NewDecoder([]byte(value).Decode(&key)); err != nil {
	if err := json.Unmarshal([]byte(value), &key); err != nil {
		log.Fatal(err)
	}
	log.Println(key)
	pretty.Println(key)
	fmt.Println(key.PrivateKey)

}
