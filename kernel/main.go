package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/bitfield/script"
)

func main() {
	kernel, err := script.Exec("uname -r").String()
	if err != nil {
		panic(err)
	}
	parts := strings.Split(kernel, ".")
	major, err := strconv.Atoi(parts[0])
	if err != nil {
		panic(err)
	}
	minor, err := strconv.Atoi(parts[1])
	if err != nil {
		panic(err)
	}
	fmt.Println(kernel, major, minor)
}
