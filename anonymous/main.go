package main

import "fmt"

type Room struct {
	Doors   int
	Windows int
}

type House struct {
	Doors int
	Room
}

func main() {
	room := Room{Doors: 1, Windows: 3}
	house := House{Room: room, Doors: 2}

	fmt.Println(house)
	fmt.Println(house.Doors)
	fmt.Println(house.Room.Doors)
	fmt.Println(house.Windows)
	fmt.Println(house.Room.Windows)
}
