package main

import (
	"bytes"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"log/slog"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"golang.org/x/crypto/ssh"
)

type Login struct {
	Message string        `json:"message"`
	Sig     ssh.Signature `json:"sig"`
	User    string        `json:"user"`
}

func main() {
	logLevel := &slog.LevelVar{}
	logger := slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		AddSource: true,
		Level:     logLevel,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.SourceKey {
				a.Value = slog.StringValue(filepath.Base(a.Value.String()))
			}
			return a
		},
	}))
	slog.SetDefault(logger)
	//logLevel.Set(slog.LevelInfo)
	log.SetFlags(log.Lshortfile)
	client := &http.Client{Timeout: 1 * time.Second}
	defer client.CloseIdleConnections()
	if err := register(client); err != nil {
		log.Fatal(err)
	}
	s, err := hello(client)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := login(client, s)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(resp)
	slog.Info("slog", "response", resp)
	slog.Debug(resp, "try", 1)
	logLevel.Set(slog.LevelDebug)
	slog.Debug(resp, "try", 2)
}

func register(c *http.Client) error {
	pub, err := os.ReadFile(os.Getenv("HOME") + "/.ssh/id_ed25519.pub")
	if err != nil {
		return fmt.Errorf("read pub key %w", err)
	}
	register := struct {
		User string
		Key  string
	}{
		User: "theUser",
		Key:  string(pub),
	}
	payload, err := json.Marshal(register)
	if err != nil {
		return fmt.Errorf("marshal %v", err)
	}
	request, err := http.NewRequest(http.MethodPost, "http://localhost:8090/register", bytes.NewBuffer(payload))
	if err != nil {
		return fmt.Errorf("new http request %w", err)
	}
	resp, err := c.Do(request)
	if err != nil {
		return fmt.Errorf("post %w", err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("read body %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("status error %s %x ", resp.Status, body)
	}
	return nil
}

func hello(c *http.Client) ([]byte, error) {
	empty := []byte{}
	request, err := http.NewRequest("GET", "http://localhost:8090/hello", nil)
	if err != nil {
		return empty, fmt.Errorf("http request %w", err)
	}
	resp, err := c.Do(request)
	if err != nil {
		return empty, fmt.Errorf("get %w", err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return empty, fmt.Errorf("read body %w", err)
	}
	log.Println("response from hello", string(body))
	return body, nil
}

func login(c *http.Client, body []byte) (string, error) {
	blank := ""
	key, err := os.ReadFile(os.Getenv("HOME") + "/.ssh/id_ed25519")
	if err != nil {
		return blank, fmt.Errorf("read key %w", err)
	}
	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		return blank, fmt.Errorf("parse private key %w", err)
	}
	sig, err := signer.Sign(rand.Reader, body)
	if err != nil {
		return blank, fmt.Errorf("sign %w", err)
	}
	login := Login{
		Message: string(body),
		Sig:     *sig,
		User:    "theUser",
	}
	payload, err := json.Marshal(login)
	if err != nil {
		return blank, fmt.Errorf("marshal %w", err)
	}
	request, err := http.NewRequest("POST", "http://localhost:8090/login", bytes.NewBuffer(payload))
	if err != nil {
		return blank, fmt.Errorf("http request %w", err)
	}
	resp, err := c.Do(request)
	if err != nil {
		return blank, fmt.Errorf("post %w", err)
	}
	defer resp.Body.Close()
	body, err = io.ReadAll(resp.Body)
	if err != nil {
		return blank, fmt.Errorf("read body %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return blank, fmt.Errorf("status error %s %s", resp.Status, string(body))
	}
	return string(body), nil
}
