package main

import (
	"log"
	"log/slog"
)

func main() {
	slog.Info("hello world")
	slog.Info("hello world", "name", "slog")
	log.Println("hello world")
}
