package main

import (
	"bytes"
	"crypto/rand"
	"log"
	"os"

	"github.com/hiddeco/sshsig"
	"golang.org/x/crypto/ssh"
)

func main() {
	key, err := os.ReadFile(os.Getenv("HOME") + "/.ssh/id_ed25519")
	if err != nil {
		log.Fatal(err)
	}
	//ssh.User("mkasun")
	//out, err := ssh.Run([]byte(key), "server.clustercat.com", "ping -c 3 10.121.248.1")
	//if err != nil {
	//	log.Fatal(err)
	//}
	//log.Println(out)
	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		log.Fatal(err)
	}
	message := []byte("hello world\n")
	sig, err := signer.Sign(rand.Reader, message)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Sig: %s\n", sig.Format)
	log.Printf("Signature: %x\n", sig.Blob)
	log.Printf("Rest: %x\n", sig.Rest)

	sig2, err := sshsig.Sign(bytes.NewReader(message), signer, sshsig.HashSHA512, "file")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Sig: ", sig2)

	armoredSig := sshsig.Armor(sig2)
	log.Printf("Armored Sig: %s\n", armoredSig)

	authKeys, err := os.ReadFile(os.Getenv("HOME") + "/.ssh/id_ed25519.pub")
	if err != nil {
		log.Fatal(err)
	}
	pubKey, _, _, _, err := ssh.ParseAuthorizedKey(authKeys)
	if err != nil {
		log.Fatal(err)
	}

	if err := sshsig.Verify(bytes.NewReader(message), sig2, pubKey, sig2.HashAlgorithm, "file"); err != nil {
		log.Fatal(err)
	}

	log.Println("Verified!")
	if err := pubKey.Verify(message, sig); err != nil {
		log.Fatal(err)
	}
	log.Println("Verified! with ssh")

}
