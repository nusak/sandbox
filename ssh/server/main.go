package main

import (
	"crypto/rand"
	"encoding/base32"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/ssh"
)

type Login struct {
	Message string        `json:"message"`
	Sig     ssh.Signature `json:"sig"`
	User    string        `json:"user"`
}

type Registation struct {
	User string `json:"user"`
	Key  string `json:"key"`
}

var users map[string]string

func main() {
	users = make(map[string]string)
	router := setupRouter()
	router.Run(":8080")
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/hello", func(c *gin.Context) {
		c.String(200, randomString(14))
	})
	r.POST("/login", func(c *gin.Context) {
		var login Login
		if err := c.ShouldBindJSON(&login); err != nil {
			log.Println("login ", err)
			c.JSON(400, gin.H{"error": err.Error()})
			return
		}
		fmt.Println(users)
		pub, ok := users[login.User]
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "user not found"})
			return
		}
		pubKey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(pub))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		if err := pubKey.Verify([]byte(login.Message), &login.Sig); err != nil {
			log.Println("login verify ", err)
			c.JSON(401, gin.H{"error": err.Error()})
			return
		}
		c.JSON(200, gin.H{"message": "Hello World"})
	})
	r.POST("/register", func(c *gin.Context) {
		var reg Registation
		if err := c.ShouldBindJSON(&reg); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		users[reg.User] = reg.Key
		c.String(http.StatusOK, "registration successfull")
	})
	return r
}

func randomString(n int) string {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		log.Fatal("randomString", err)
	}
	return base32.StdEncoding.EncodeToString(b)[:n]
}
