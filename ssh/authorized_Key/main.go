package main

import (
	"fmt"
	"log"

	"golang.org/x/crypto/ssh"
)

func main() {
	public, comment, options, rest, err := ssh.ParseAuthorizedKey([]byte("file.nusak.ca ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAXMGK3UQdkkaWIFpqkzMBitQFs3y3ZIm+tasXeI3W+NPe4Y725cqKvhkcOuncn6qEV7hMFZQL4oY8wr1XLjfrw="))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(public)
	fmt.Println(comment)
	fmt.Println(options)
	fmt.Println(string(rest))
}
