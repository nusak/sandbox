package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"syscall"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/terminal"
)

func main() {
	var passphraseError *ssh.PassphraseMissingError
	private, err := os.ReadFile(os.Getenv("HOME") + "/.ssh/id_ed25519.withpassphrase")
	keyString := string(private)
	fmt.Println(keyString)
	if err != nil {
		log.Fatal(err)
	}
	signer, err := ssh.ParsePrivateKey([]byte(keyString))
	if errors.As(err, &passphraseError) {
		fmt.Println("Enter passphrase for key")
		passphrase, err := terminal.ReadPassword(int(syscall.Stdin))
		if err != nil {
			log.Fatal(err)
		}
		signer, err = ssh.ParsePrivateKeyWithPassphrase(private, passphrase)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Private key parsed successfully")
		return
	}
	if err != nil {
		log.Fatal(err)
	}
	_ = signer
	fmt.Println("Private key parsed successfully")

}
