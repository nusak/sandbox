package main

import (
	"bytes"
	"fmt"
	"log"
	"os"

	"golang.org/x/crypto/ssh"
)

func main() {
	public, _, _, _, err := ssh.ParseAuthorizedKey([]byte("file.nusak.ca ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAXMGK3UQdkkaWIFpqkzMBitQFs3y3ZIm+tasXeI3W+NPe4Y725cqKvhkcOuncn6qEV7hMFZQL4oY8wr1XLjfrw="))
	//var hostKey ssh.PublicKey
	//hostkey, _, _, _, err := ssh.ParseAuthorizedKey([]byte("ssh-id_ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMxRV791bjjyqjvVMRg367xkqnvDWW9XdX2CNqzdzqap"))
	//if err != nil {
	//	log.Fatal("public key", err)
	//}
	//pretty.Println(hostkey)
	// A public key may be used to authenticate against the remote
	// server by using an unencrypted PEM-encoded private key file.
	//
	// If you have an encrypted private key, the crypto/x509 package
	// can be used to decrypt it.
	//hostKeyCallback, err := knownhosts.New("/home/mkasun/.ssh/known_hosts")
	//if err != nil {
	//	log.Fatal("known hosts", err)
	//}
	//pretty.Println(hostKeyCallback)
	key, err := os.ReadFile("/home/mkasun/.ssh/id_ed25519")
	if err != nil {
		log.Fatalf("unable to read private key: %v", err)
	}

	// Create the Signer for this private key.
	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		log.Fatalf("unable to parse private key: %v", err)
	}

	config := &ssh.ClientConfig{
		User: "mkasun",
		Auth: []ssh.AuthMethod{
			// Use the PublicKeys method for remote authentication.
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: ssh.FixedHostKey(public),
	}

	// Connect to the remote server and perform the SSH handshake.
	client, err := ssh.Dial("tcp", "file.nusak.ca:22", config)
	if err != nil {
		log.Fatalf("unable to connect: %v", err)
	}
	defer client.Close()
	session, err := client.NewSession()
	if err != nil {
		log.Fatal("session", err)
	}
	var stdoutBuf bytes.Buffer
	session.Stdout = &stdoutBuf
	session.Run("ls")
	fmt.Println(stdoutBuf.String())
}
