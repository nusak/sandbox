package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gravitl/netmaker/netclient/ncutils"
	"github.com/kr/pretty"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	"gopkg.in/ini.v1"
)

func main() {
	peers, err := ReadPeers("nm-netmaker")
	if err != nil {
		log.Fatal(err)
	}
	pretty.Println(peers)
}

func ReadPeers(iface string) ([]wgtypes.Peer, error) {
	var peers []wgtypes.Peer
	var wgpeer wgtypes.Peer
	out, err := ncutils.RunCmd("/usr/bin/wg showconf "+iface, true)
	if err != nil {
		return peers, fmt.Errorf("runcmd error: %v", err)
	}
	os.WriteFile("/tmp/netmaker.conf", []byte(out), 0600)
	options := ini.LoadOptions{
		AllowNonUniqueSections: true,
		AllowShadows:           true,
	}
	file, err := ini.LoadSources(options, "/tmp/netmaker.conf")
	if err != nil {
		return peers, fmt.Errorf("loading file %v", err)
	}
	peersections, err := file.SectionsByName("Peer")
	if err != nil {
		return peers, fmt.Errorf("peer section %v", err)
	}
	for _, peersection := range peersections {
		key := peersection.Key("PublicKey").String()
		wgpeer.PublicKey, err = wgtypes.ParseKey(key)
		if err != nil {
			return peers, fmt.Errorf("parseKey %v", err)
		}
		peers = append(peers, wgpeer)
	}
	return peers, nil
}
