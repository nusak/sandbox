package main

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()
	masterkey, ok := os.LookupEnv("MASTERKEY")
	if !ok {
		panic("MASTERKEY is not set")
	}
	fmt.Println("MASTERKEY is set to", masterkey)
	masterkey = os.Getenv("MASTERKEY")
	fmt.Println("MASTERKEY is set to", masterkey)
}
