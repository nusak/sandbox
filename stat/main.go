package main

import (
	"log"
	"os"

	"github.com/kr/pretty"
)

func main() {
	info, err := os.Stat("/sbin/init")
	if err != nil {
		log.Fatal(err)
	}
	pretty.Println(info)
}
