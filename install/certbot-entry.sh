#!/bin/sh
# deps
apk update
apk add bash curl
# zerossl
wget -qO zerossl-bot.sh "https://github.com/zerossl/zerossl-bot/raw/master/zerossl-bot.sh"
chmod +x zerossl-bot.sh
# request the certs
./zerossl-bot.sh "	certonly --standalone 		--non-interactive --agree-tos 		-m matt@netmaker.io 		-d api.nm.138-197-143-220.nip.io 		-d broker.nm.138-197-143-220.nip.io 		-d dashboard.nm.138-197-143-220.nip.io 		-d turn.nm.138-197-143-220.nip.io 		-d turnapi.nm.138-197-143-220.nip.io"
