package main

import (
	"fmt"
	"log"
	"net/http"
	"os/exec"
	"regexp"
	"strings"
)

func main() {
	matched, err := regexp.MatchString(`SVG|AI|EPS`, "SVG")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(matched)

	vectors := []string{"file.pdf", "file.eps", "file.svg"}
	for _, vector := range vectors {
		log.Println("working on ", vector)
		cmd := exec.Command("identify", "-quiet", "-format", "'%m'", vector)
		out, err := cmd.CombinedOutput()
		if err != nil {
			log.Fatal("message", "failed to idenfity file type: ", err.Error())
			return
		}
		filetype := string(out)
		log.Println("filetype of supplied logo", filetype)
		expression := regexp.MustCompile(`SVG|AI|EPT`)
		if !expression.MatchString(filetype) {
			//if (filetype != "PNG" || filetype != "EPT\nTIFF" || filetype != "SVG") {
			log.Println(http.StatusBadRequest, "regexp: invalid file type: "+filetype)
		}
		var extension string
		if strings.Contains(filetype, "EPT") {
			extension = "eps"
		} else if filetype == "'SVG'" {
			extension = "svg"
		} else if filetype == "'AI'" {
			extension = "pdf"
		} else {
			log.Println(http.StatusBadRequest, "mesage: invalid file type: ", filetype)
		}
		log.Println(extension)
	}

}
