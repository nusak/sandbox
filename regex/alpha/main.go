// Go program to check Alphanumeric Regex
package main

import (
	"fmt"
	"regexp"
)

func main() {
	for _, word := range []string{"abc", "abc123", "abc123!", "abc123#", "two words"} {
		is_alphanumeric := regexp.MustCompile(`^[a-zA-Z0-9]*$`).MatchString(word)
		fmt.Print(is_alphanumeric, " ")
		if is_alphanumeric {
			fmt.Printf("%s is an Alphanumeric string\n", word)
		} else {
			fmt.Printf("%s is not an Alphanumeric string\n", word)
		}
		whitespace := regexp.MustCompile(`\s+`).MatchString(word)
		if whitespace {
			fmt.Printf("%s has whitespace\n", word)
		} else {
			fmt.Printf("%s has no whitespace\n", word)
		}

	}
}
