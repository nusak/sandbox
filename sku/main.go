package main

import (
	"log"

	"github.com/kr/pretty"
)

type Color struct {
	Name        string
	ColorDetail ColorDetail
}

type ColorDetail struct {
	HexValue string
	NameEN   string
	NameFR   string
}

func main() {
	red := Color{
		Name: "Red",
		ColorDetail: ColorDetail{
			HexValue: "#c8102e",
			NameEN:   "Red",
			NameFR:   "Rouge",
		},
	}
	pretty.Println(red)
	log.Println(red)

	file.Open
}
