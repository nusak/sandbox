package main

import (
	"fmt"
	"net"

	"github.com/kr/pretty"
)

func main() {
	ips := []net.IPNet{}
	ip := net.IPNet{
		IP:   net.ParseIP("192.168.0.1"),
		Mask: net.CIDRMask(32, 32),
	}
	ip6 := net.IPNet{
		IP:   net.ParseIP("::c389:1"),
		Mask: net.CIDRMask(128, 32),
	}
	ipempty := net.IPNet{}
	ip6empty := net.IPNet{}
	ips = append(ips, ip)
	ips = append(ips, ipempty)
	ips = append(ips, ip6)
	ips = append(ips, ip6empty)
	pretty.Println(ips)
	fmt.Println(ips)
}
