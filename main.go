package main

import "fmt"

func main() {
	for i := range 4 {
		for j := range 4 {
			for k := range 4 {
				fmt.Println(i, j, k, clamp(i, j, k))
			}
		}
	}
}

func clamp(value, left, right int) int {
	return max(left, min(value, right))
}
