package main

import (
	"fmt"
	"net"
)

func main() {
	netIP := net.IPNet{
		IP:   net.ParseIP("10.192.168.0"),
		Mask: net.CIDRMask(24, 32),
	}
	one, bits := netIP.Mask.Size()
	fmt.Println(one, bits)
	//network := iplib.Net4FromStr("192.168.0.0/24")
	//first := network.FirstAddress()
	//broadcast := network.BroadcastAddress()
	//fmt.Println("First: ", first, first.String())
	//fmt.Println("Broadcast: ", broadcast, broadcast.String())

}
