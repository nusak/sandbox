package main

import (
	"bufio"
	"log"
	"net"
)

func main() {
	gateway := "192.168.1.6:51821"
	c, err := net.Dial("tcp", gateway)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()
	log.Println("Connected to", c.RemoteAddr())
	p := make([]byte, 1024)
	c.Write([]byte("Hello, world!\n"))
	_, err = bufio.NewReader(c).Read(p)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(p))
}
