package main

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/gravitl/netmaker/database"
	"github.com/kr/pretty"
)

const (
	Undefined KeyType = iota
	TimeExpiration
	Uses
	Unlimited
)

type KeyType int

func (k KeyType) String() string {
	return [...]string{"Undefined", "TimeExpiration", "Uses", "Unlimited"}[k]
}

type Old struct {
	Expiration    time.Time `json:"expiration"`
	UsesRemaining int       `json:"uses_remaining"`
	Value         string    `json:"value"`
	Networks      []string  `json:"networks"`
	Unlimited     bool      `json:"unlimited"`
	Tags          []string  `json:"tags"`
	Token         string    `json:"token,omitempty"` // B64 value of EnrollmentToken
}

type New struct {
	Expiration    time.Time `json:"expiration"`
	UsesRemaining int       `json:"uses_remaining"`
	Value         string    `json:"value"`
	Networks      []string  `json:"networks"`
	Unlimited     bool      `json:"unlimited"`
	Tags          []string  `json:"tags"`
	Token         string    `json:"token,omitempty"` // B64 value of EnrollmentToken
	Type          KeyType   `json:"type"`
}

func main() {
	database.InitializeDatabase()
	expire := Old{
		Expiration: time.Now().Add(time.Hour * 24),
	}
	unlimited := Old{
		Unlimited: true,
	}
	uses := Old{
		UsesRemaining: 5,
	}
	data, err := json.Marshal(&expire)
	if err != nil {
		log.Fatal(err)
	}
	if err := database.Insert("one", string(data), database.ENROLLMENT_KEYS_TABLE_NAME); err != nil {
		log.Fatal(err)
	}
	data, err = json.Marshal(&unlimited)
	if err != nil {
		log.Fatal(err)
	}
	if err := database.Insert("two", string(data), database.ENROLLMENT_KEYS_TABLE_NAME); err != nil {
		log.Fatal(err)
	}
	data, err = json.Marshal(&uses)
	if err != nil {
		log.Fatal(err)
	}
	if err := database.Insert("three", string(data), database.ENROLLMENT_KEYS_TABLE_NAME); err != nil {
		log.Fatal(err)
	}
	rows, err := database.FetchRecords(database.ENROLLMENT_KEYS_TABLE_NAME)
	if err != nil {
		log.Fatal(err)
	}
	for k := range rows {
		var new New
		if err := json.Unmarshal([]byte(rows[k]), &new); err != nil {
			log.Fatal(err)
		}
		pretty.Println("new", new)
		if new.Type == Undefined {
			if new.Unlimited {
				new.Type = Unlimited
			}
			if new.UsesRemaining > 0 {
				new.Type = Uses
			}
			if !new.Expiration.IsZero() {
				new.Type = TimeExpiration

			}

		}
		fmt.Println("new type", new.Type)
		pretty.Println("new", new)
	}
}
