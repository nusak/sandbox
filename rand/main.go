package main

import (
	"fmt"
	"math/rand"
	"time"
)

func random() bool {
	rand.Seed(time.Now().UnixNano())
	num := rand.Intn(10)
	fmt.Println(num)
	return num > 1
}

func main() {
	for i := 0; i < 10; i++ {
		fmt.Println(random())
	}
}
