package main

import (
	"bufio"
	"strings"

	"github.com/kr/pretty"
)

type IPs struct {
	Name string
	IP   string
}

func main() {
	ips := []IPs{
		IPs{
			Name: "ello",
			IP:   "10.0.0.1",
		},
		IPs{
			Name: "Two",
			IP:   "10.0.0.2",
		},
	}
	dnstring := "hello 10.0.0.1\ntwo 10.0.0.2\nthree 10.0.0.3"
	dns := []byte(dnstring)

	new := updateDNS(dns, ips)
	pretty.Println(string(new))
}

func updateDNS(file []byte, peers []IPs) []byte {
	dns := ""
	fulldata := strings.NewReader(string(file))
	scanner := bufio.NewScanner(fulldata)
	for scanner.Scan() {
		line := scanner.Text()
		for _, peer := range peers {
			ip := peer.IP
			if strings.Contains(line, ip) {
				dns = dns + line + "\n"
			}
		}
	}
	return []byte(dns)
}
