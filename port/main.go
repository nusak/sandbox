package main

import (
	"fmt"
	"log"
	"net"

	"github.com/kr/pretty"
)

func main() {
	addr := net.TCPAddr{
		IP: net.ParseIP("127.0.0.1"),
	}
	conn, err := net.ListenTCP("tcp", &addr)
	if err != nil {
		log.Println("Error listening: ", err)

	}
	fmt.Println(conn.Addr())
	//fmt.Println(conn.Addr().IP)
	//fmt.Println(conn.Addr().Port)
	pretty.Println(conn.Addr())
	fmt.Println(net.TCPAddr(conn.Addr()))
	pretty.Println(addr)
	fmt.Println(addr.IP.String())
	//pretty.Println(*addr.(net.TCPAddr).IP)
	conn.Close()
}
