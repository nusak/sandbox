package main

import (
	"fmt"
	"time"
)

func main() {
	old := int32(20)
	//new := int64(old)
	keep := time.Duration(int64(time.Second) * int64(old))
	fmt.Println(keep)
}
