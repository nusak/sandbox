package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/mochi-co/mqtt/v2"
	"github.com/mochi-co/mqtt/v2/hooks/auth"
	"github.com/mochi-co/mqtt/v2/listeners"
	"github.com/rs/zerolog"
)

func main() {
	quit := make(chan os.Signal, 1)
	//quit := make(chan bool, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	//go func() {
	//	<-sigs
	//	done <- true
	//}()

	// An example of configuring various server options...
	options := &mqtt.Options{
		// InflightTTL: 60 * 15, // Set an example custom 15-min TTL for inflight messages
	}
	log.Println("setting up server")
	server := mqtt.New(options)

	// For security reasons, the default implementation disallows all connections.
	// If you want to allow all connections, you must specifically allow it.
	err := server.AddHook(new(auth.AllowHook), &auth.Options{
		Ledger: &auth.Ledger{
			Auth: auth.AuthRules{ // Auth disallows all by default
				{Username: "peach", Password: "password1", Allow: true},
				{Username: "melon", Password: "password2", Allow: true},
				//{Remote: "127.0.0.1:*", Allow: true},
				//{Remote: "localhost:*", Allow: true},
			},
			ACL: auth.ACLRules{ // ACL allows all by default
				//{Remote: "127.0.0.1:*"}, // local superuser allow all
				{
					// user melon can read and write to their own topic
					Username: "melon", Filters: auth.Filters{
						"melon/#":   auth.ReadWrite,
						"updates/#": auth.WriteOnly, // can write to updates, but can't read updates from others
					},
				},
				{
					// Otherwise, no clients have publishing permissions
					Filters: auth.Filters{
						"#":         auth.ReadOnly,
						"updates/#": auth.Deny,
					},
				},
			},
		},
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Println("adding listener")
	tcp := listeners.NewTCP("t1", ":1883", nil)
	err = server.AddListener(tcp)
	if err != nil {
		log.Fatal(err)
	}

	l := server.Log.Level(zerolog.DebugLevel)
	server.Log = &l
	log.Println("start server")
	go func() {
		err := server.Serve()
		if err != nil {
			log.Fatal(err)
		}
	}()

	log.Println("waiting for quit")
	//<-done
	<-quit
	server.Log.Warn().Msg("caught signal, stopping...")
	server.Close()
	server.Log.Info().Msg("main.go finished")
}
