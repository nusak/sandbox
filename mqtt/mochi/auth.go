package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/kr/pretty"
	"github.com/mochi-co/mqtt/v2"
	"github.com/mochi-co/mqtt/v2/hooks/auth"
	"github.com/mochi-co/mqtt/v2/listeners"
)

func main() {
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)
	reset := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	signal.Notify(reset, syscall.SIGHUP)
	server := mqtt.New(nil)

	go func() {
		<-sigs
		done <- true
	}()

	//authRules := &auth.Ledger{
	//	Auth: auth.AuthRules{ // Auth disallows all by default
	//		{Username: "peach", Password: "password1", Allow: true},
	//		{Username: "melon", Password: "password2", Allow: true},
	//		{Username: "junk", Allow: true},
	//		{Username: "god", Allow: true},
	//		{Remote: "127.0.0.1:", Allow: true},
	//		{Remote: "localhost:", Allow: true},
	//	},
	//	ACL: auth.ACLRules{ // ACL allows all by default
	//		{Remote: "127.0.0.1:*"}, // local superuser allow all
	//		{Username: "junk:*"},
	//		{
	//			// user melon can read and write to their own topic
	//			Username: "melon", Filters: auth.Filters{
	//				"melon/#":   auth.ReadWrite,
	//				"updates/#": auth.WriteOnly, // can write to updates, but can't read updates from others
	//			},
	//		},
	//		{Username: "god", Filters: auth.Filters{
	//			"#": auth.ReadWrite,
	//		},
	//		},
	//		{
	//			// Otherwise, no clients have publishing permissions
	//			Filters: auth.Filters{
	//				"#":          auth.ReadOnly,
	//				"updates/#":  auth.Deny,
	//				"melon/test": auth.WriteOnly,
	//			},
	//		},
	//	},
	//}

	// you may also find this useful...
	// d, _ := authRules.ToYAML()
	// d, _ := authRules.ToJSON()
	// fmt.Println(string(d))
	authRules := &auth.Ledger{
		Users: auth.Users{
			"melon": auth.UserRule{Username: "melon", Password: "pass", ACL: auth.Filters{
				"#": auth.ReadWrite},
			},
			"apple": auth.UserRule{Username: "apple", Password: "pass", ACL: auth.Filters{
				"#": auth.ReadOnly},
			},
		},
	}

	err := server.AddHook(new(auth.Hook), &auth.Options{
		Ledger: authRules,
	})
	if err != nil {
		log.Fatal(err)
	}

	tcp := listeners.NewTCP("t1", ":1883", nil)
	err = server.AddListener(tcp)
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		err := server.Serve()
		if err != nil {
			log.Fatal(err)
		}
	}()

	go func() {
		for {
			<-reset
			authRules.Update(&auth.Ledger{
				Users: auth.Users{
					"melon": auth.UserRule{Username: "melon", Password: "pass", ACL: auth.Filters{
						"#": auth.ReadWrite},
					},
				},
			})
			clients := mqtt.NewClients()
			pretty.Println(clients)
		}
	}()

	<-done
	server.Log.Warn().Msg("caught signal, stopping...")
	server.Close()
	server.Log.Info().Msg("main.go finished")
}
