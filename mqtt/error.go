package main

import (
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"golang.org/x/exp/slog"
)

func main() {
	opts := mqtt.NewClientOptions()
	opts.AddBroker("ssl://broker.clustercat.com:1883")
	c := mqtt.NewClient(opts)
	if token := c.Connect(); !token.WaitTimeout(30*time.Second) || token.Error() != nil {
		slog.Error("test:", "error", token.Error, "string", token.Error())
	}
}
