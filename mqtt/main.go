package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var mqclient mqtt.Client

func main() {
	mqtt.ERROR = log.New(os.Stdout, "[ERROR] ", 0)
	mqtt.CRITICAL = log.New(os.Stdout, "[CRIT] ", 0)
	mqtt.WARN = log.New(os.Stdout, "[WARN]  ", 0)
	mqtt.DEBUG = log.New(os.Stdout, "[DEBUG] ", 0)

	host := os.Getenv("HOSTNAME")
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal("hostname err", err)
	}
	if host == "" {
		log.Println("hostname env not set")
	}
	log.Println("host:", host, []byte(host))
	log.Println("hostname:", hostname, []byte(hostname))
	brokers := []string{"ubuntu.clustercat.com", "lx.clustercat.com"}
	setUpMQTT(brokers)
	defer mqclient.Disconnect(250)
	wg := sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	wg.Add(1)
	go Checkin(ctx, &wg)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, os.Interrupt)
	<-quit
	log.Println("received signal")
	cancel()
	wg.Wait()
	log.Println("exiting")
}

func Checkin(ctx context.Context, wg *sync.WaitGroup) {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, os.Interrupt)
	for {
		select {
		case <-quit:
			log.Println("go routine received signal")
			wg.Done()
			return
		case <-ctx.Done():
			log.Println("context done received")
			wg.Done()
			return
		case <-time.After(time.Second << 2):
			//	brokers := []string{"lxc.clustercat.com"}
			//	client := setUpMQTT(brokers)
			//	defer client.Disconnect(250)
			mqclient.Publish("hello", 0, false, "world")
			log.Println("sent message")
		}
	}
}

func setUpMQTT(brokers []string) {
	opts := mqtt.NewClientOptions()
	for _, broker := range brokers {
		opts.AddBroker(broker + ":1883")
	}
	//opts.AddBroker("ssl://" + broker + ":8883")
	//tlsConfig := NewTLSConfig()
	//opts.SetTLSConfig(tlsConfig)
	opts.SetOnConnectHandler(func(client mqtt.Client) {
		if token := client.Subscribe("hello", 0, mqtt.MessageHandler(All)); token.Wait() && token.Error() != nil {
			log.Fatal("subscription error", token.Error())
		}
	})
	mqclient = mqtt.NewClient(opts)
	if token := mqclient.Connect(); token.Wait() && token.Error() != nil {
		log.Fatal("Connection Error", token.Error().Error())
	}
}

//func NewTLSConfig() *tls.Config {
//	certpool := x509.NewCertPool()
//	ca, err := os.ReadFile("./root.pem")
//	if err != nil {
//		log.Fatal("could not read root ca", err)
//	}
//	ok := certpool.AppendCertsFromPEM(ca)
//	if !ok {
//		log.Fatal("could not append root ca", err)
//	}
//	clientKeyPair, err := tls.LoadX509KeyPair("./client.pem", "client.key")
//	if err != nil {
//		log.Fatal("could not load client cert/key", err)
//	}
//	return &tls.Config{
//		RootCAs:            certpool,
//		InsecureSkipVerify: false,
//		VerifyConnection: func(cs tls.ConnectionState) error {
//			if cs.ServerName != "broker.ping.clustercat.com" {
//				logger.Log(0, "VerifyConnection - certifiate mismatch")
//				return errors.New("certificate doesn't match server")
//			}
//			//ca, err := ssl.ReadCert("./root.pem")
//			//if err != nil {
//			//	logger.Log(0, "VerifyConnection - unable to read ca", err.Error())
//			//	return errors.New("unable to read ca")
//			//}
//			for _, cert := range cs.PeerCertificates {
//				if cert.IsCA {
//					if string(cert.PublicKey.(ed25519.PublicKey)) != string(ca.PublicKey.(ed25519.PublicKey)) {
//						logger.Log(0, "VerifyConnection - public key mismatch")
//						return errors.New("cert public key does not match ca public key")
//					}
//				}
//			}
//			return nil
//		},
//		Certificates: []tls.Certificate{clientKeyPair},
//	}
//}

func All(client mqtt.Client, msg mqtt.Message) {
	log.Println("received message")
	log.Println("Topic: ", string(msg.Topic()))
	log.Println("Message:", string(msg.Payload()))
}
