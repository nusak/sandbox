package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/maragudk/gomponents"
	"github.com/maragudk/gomponents/components"
	"github.com/maragudk/gomponents/html"
)

func main() {
	router := gin.Default()
	router.GET("/new", gin.WrapF(new))
	router.GET("/home", gin.WrapF(handler))
	router.GET("/about", gin.WrapF(handler))

	//_ = http.ListenAndServe("localhost:8080", http.HandlerFunc(handler))
	router.Run("localhost:8080")
}

func new(w http.ResponseWriter, r *http.Request) {
	gomponents.Node(
		html.Div(html.H1(gomponents.Text("Hello World!")))
	).Render(w)
}

func handler(w http.ResponseWriter, r *http.Request) {
	_ = Page("Hi!", r.URL.Path).Render(w)
}

func Page(title, currentPath string) gomponents.Node {
	return components.HTML5(components.HTML5Props{
		Title:    title,
		Language: "en",
		Head: []gomponents.Node{
			html.StyleEl(html.Type("text/css"), gomponents.Raw(".is-active{ font-weight: bold }")),
		},
		Body: []gomponents.Node{
			Navbar(currentPath),
			html.H1(gomponents.Text(title)),
			html.P(gomponents.Textf("Welcome to the page at %v.", currentPath)),
		},
	})
}

func Navbar(currentPath string) gomponents.Node {
	return html.Nav(
		NavbarLink("/", "Home", currentPath),
		NavbarLink("/about", "About", currentPath),
		NavbarLink("/new", "New", currentPath),
	)
}

func NavbarLink(href, name, currentPath string) gomponents.Node {
	return html.A(html.Href(href), components.Classes{"is-active": currentPath == href}, gomponents.Text(name))
}
