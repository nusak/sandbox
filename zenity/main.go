package main

import (
	"fmt"
	"time"

	"github.com/ncruces/zenity"
)

func main() {
	timeSelected, err := zenity.Calendar("", zenity.DefaultDate(2023, time.November, 1))
	if err != nil {
		fmt.Println("Error:", err)
	} else {
		fmt.Println("Selected:", timeSelected)
	}
	selected, err := zenity.List("testing", []string{"foo", "bar", "baz"}, zenity.Title("Select one"),
		zenity.OKLabel("Fruit"), zenity.ExtraButton("Vegetables"), zenity.DefaultItems("bar", "baz"),
		zenity.DisallowEmpty())
	if err != nil {
		fmt.Println("Error:", err)
	} else {
		fmt.Println("Selected:", selected)
	}
	if err := zenity.Notify("testing", zenity.Title("Hello"), zenity.InfoIcon,
		zenity.CancelLabel("junk")); err != nil {
		fmt.Println("Notify Error:", err)
	}

}
