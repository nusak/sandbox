package main

import (
	"errors"
	"log"

	"github.com/kr/pretty"
	"github.com/sagikazarmark/slog-shim"
)

type MyError struct {
	Part1 string
	Part2 string
}

func (m MyError) Error() string {
	return m.Part1
}

var myError *MyError

func main() {
	err := Hello()
	log.Println(err)
	//log.Println((err.(MyError).Part2), err.(MyError).Part1)
	if errors.Is(err, myError) {
		log.Println("myerror")
	}
	if errors.As(err, &myError) {
		log.Println("errorsAs", myError.Part1, myError.Part2, myError.Error())
	}

	errs := errors.Join(errors.New("error1"), errors.New("error2"))
	pretty.Println(errs)
	errs2 := errors.Join(nil, nil)
	pretty.Println(errs2)
	if errs2 == nil {
		log.Println("nil")
	}
	errs = errors.Join(errs, errs2)
	pretty.Println(errs)
	if errs != nil {
		slog.Info("errs", "ERROR", errs.Error())
	}

}

func Hello() error {
	return &MyError{
		Part1: "Hello",
		Part2: "World",
	}
	//return error(err)
}
