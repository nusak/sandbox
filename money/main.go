package main

import "fmt"

type Currency int64

const (
	Cent   Currency = 1
	Dollar Currency = 100
)

func (c Currency) String() string {
	dollars := c / Dollar
	cents := c % Dollar
	return fmt.Sprintf("$%d.%d", dollars, cents)
}

func main() {
	x := 125
	y := 1.13
	total := Currency(int64(float64(x) * y))
	fmt.Println(total)

	amount := 1*Dollar + 25*Cent
	fmt.Println(amount)
	hst := amount * 13 / 100
	fmt.Println("hst", hst)
	total2 := amount + hst
	//total2 := Currency(float64(amount) * 1.13)
	fmt.Println(total2)
	priceWithTax := amount * 113 / 100
	fmt.Println(priceWithTax)
}
