package main

import (
	"github.com/google/netstack/tcpip/iptables"
	"github.com/kr/pretty"
)

func main() {
	tables := iptables.DefaultTables()
	pretty.Println(tables)
}
