package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	if err := os.MkdirAll("/etc/mkasun/", os.ModePerm); err != nil {
		log.Fatal("mkdir", err)
	}
	fmt.Println("good")
}
