package main

import (
	"log"
	"os"
)

func main() {
	if err := os.Chmod("/etc/mkasun/", 0x775); err != nil {
		log.Fatal("chmod: ", err)
	}
	log.Println("good")
}
