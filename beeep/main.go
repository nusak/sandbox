package main

import (
	"fmt"
	"log"
	"path/filepath"
	"time"

	"github.com/gen2brain/beeep"
	"github.com/godbus/dbus/v5"
	"github.com/kr/pretty"
)

func main() {
	if notify("Title2", "message body2\n<b>Bold</b>", "/tmp/information.png") != nil {
		panic(notify)
	}
	err := beeep.Notify("Title", "message body", "assets/warning.png")
	if err != nil {
		log.Fatal(err)
	}
	time.Sleep(time.Second)
	fmt.Println("done")

}

func notify(title, message, appIcon string) error {
	conn, err := dbus.SessionBus()
	if err != nil {
		return nil
	}
	icon, err := filepath.Abs(appIcon)
	if err != nil {
		icon = appIcon
	}
	defer conn.Close()
	obj := conn.Object("org.freedesktop.Notifications", dbus.ObjectPath("/org/freedesktop/Notifications"))
	call := obj.Call("org.freedesktop.Notifications.Notify", 0, "", uint32(0),
		icon, title, message, []string{},
		map[string]dbus.Variant{}, int32(10000))
	pretty.Println(call)
	return call.Err
}
