package main

import (
	"fmt"
	"log"

	"github.com/kr/pretty"
	"github.com/spf13/viper"
)

type Config struct {
	User string `mapstructure:"USER"`
	Pass string `mapstructure:"PASS"`
	Junk string `mapstructure:"JUNK"`
}

func main() {
	config := Config{}
	//viper.SetConfigType("env")
	//viper.AddConfigPath(".")
	//viper.SetConfigName(".env")
	viper.SetConfigFile(".env")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}
	fmt.Println(viper.Get("USER"))
	if err := viper.Unmarshal(&config); err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
	pretty.Println(config)
	someFunc()
}

func someFunc() {
	fmt.Println("in some func", viper.Get("USER"))
}
