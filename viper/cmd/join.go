/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"
	"log"
	"net"
	"os"

	"github.com/kr/pretty"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type Node struct {
	Name     string `yaml:"name"`
	Network  string `yam:"network"`
	Hostname string
	Address  net.IPNet
	DNSon    bool
}

// joinCmd represents the join command
var joinCmd = &cobra.Command{
	Use:   "join",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("join called")
		flags := viper.New()
		flags.BindPFlags(cmd.Flags())
		JoinNet(flags)
	},
}

func init() {
	//var node Node
	hostname, _ := os.Hostname()
	rootCmd.AddCommand(joinCmd)
	joinCmd.Flags().StringP("token", "t", "", "access token for joining network")
	joinCmd.Flags().StringP("key", "k", "", "access key for joining network")
	joinCmd.Flags().StringP("server", "s", "", "api endpoint of netmaker server (api.example.com)")
	joinCmd.Flags().StringP("user", "u", "", "username of netmaker user")
	joinCmd.Flags().StringP("network", "n", "", "network to perform spedified action against")
	joinCmd.Flags().StringP("password", "p", "", "password for authentication with netmaker")
	joinCmd.Flags().String("name", hostname, "indentifiable name for machine in netmaker network")
	//joinCmd.Flags().IPNetVar(&node.Address, "address", node.Address, "cidr for network")
	joinCmd.Flags().Bool("dnson", false, "use private dns")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// joinCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// joinCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func JoinNet(flags *viper.Viper) {
	flags.SetConfigName("node.yml")
	flags.SetConfigType("yml")
	flags.AddConfigPath("/etc/netclient/nodes")
	log.Println("checking for file")
	if _, err := os.Stat("/etc/netclient/nodes/node.yml"); err != nil {
		os.MkdirAll("/etc/netclient/nodes", os.ModePerm)
		f, err := os.Create("/etc/netclient/nodes/node.yml")
		if err != nil {
			log.Println(err)
		}
		f.Close()
	}
	log.Println("setting address")
	_, cidr, err := net.ParseCIDR("192.168.0.0/24")
	if err != nil {
		log.Fatal(err)
	}
	flags.Set("address", cidr)
	log.Println("writing config")
	if err := flags.WriteConfig(); err != nil {
		if err := flags.WriteConfigAs("/etc/netclient/node/node.yml"); err != nil {
			log.Fatal(err)
		}
	}
	log.Println("success")
	var node Node
	if err := flags.Unmarshal(&node); err != nil {
		pretty.Println(node)
		log.Fatal(err)
	}
	pretty.Println(node)

	newNode := Node{}
	viperNode := viper.New()
	f, err := os.Open("/etc/netclient/nodes/node.yml")
	if err != nil {
		log.Fatal(err)
	}
	if err := viperNode.ReadConfig(f); err != nil {
		log.Fatal(err)
	}
	if err := flags.Unmarshal(&newNode); err != nil {
		pretty.Println(newNode)
		log.Fatal(err)
	}
	pretty.Println(newNode)

}
