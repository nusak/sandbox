/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import (
	"log"

	"github.com/kr/pretty"
	"github.com/spf13/viper"
)

type Config struct {
	SQLConfig SQLConfig      `yaml:"sqlconf"`
	DB        string         `yaml:"db"`
	DBFile    string         `yaml:"dbfile"`
	DBPath    string         `yaml:"dbpath"`
	Taxes     []Tax          `yaml:"taxes"`
	Bambora   BamboraSecrets `yaml:"bambora"`
	Connect   ConnectSecrets `yaml:"connect"`
}

type Tax struct {
	Province string
	Rate     float64
}

type ConnectSecrets struct {
	Username string
	Password string
}

type BamboraSecrets struct {
	MerchantID string `yaml:"merchantid"`
	Apikey     string `yaml:"apikey"`
}

// SQLConfig - Generic SQL Config
type SQLConfig struct {
	Host     string `yaml:"host"`
	Port     int32  `yaml:"port"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	DB       string `yaml:"db"`
	SSLMode  string `yaml:"sslmode"`
}

var cached *Config

func FromFile() (*Config, error) {
	viper.SetConfigName("conf")
	viper.SetConfigType("yml")
	viper.AddConfigPath("/etc/golfballs/")
	viper.AddConfigPath("$HOME/.golfballs")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			return nil, err
		}
	}

	var config Config

	if err := viper.Unmarshal(&config); err != nil {
		return nil, err
	}

	cached = &config

	return cached, nil
}

func Get() (*Config, error) {
	if cached != nil {
		return cached, nil
	}

	config, err := FromFile()
	if err != nil {
		return &Config{}, err
	}

	return config, nil
}

func main() {
	config, err := Get()
	if err != nil {
		log.Fatal(err)
	}
	pretty.Println(config)
}
