package main

import (
	"fmt"

	"github.com/spf13/viper"
)

func main() {
	nodes := viper.New()
	nodes.SetConfigFile("/etc/netclient/nodes.yml")
	if err := nodes.ReadInConfig(); err != nil {
		panic(err)
	}
	nodeID := nodes.GetString("long.commonnode.id")
	hostID := nodes.GetString("long.commonnode.id")
	fmt.Println(nodeID, hostID)

	cfg := viper.New()
	cfg.SetConfigFile("./config.yml")
	if err := cfg.ReadInConfig(); err != nil {
		panic(err)
	}
	checkDefault(cfg.GetString("user"), cfg.GetString("pass"))
}

func checkDefault(user, pass string) {
	if user == "" || pass == "" {
		fmt.Println("vars not set")
	} else {
		fmt.Println("vars set")
	}
}
