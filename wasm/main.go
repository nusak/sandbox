package main

import (
	"errors"
	"fmt"
	"syscall/js"

	"golang.org/x/crypto/ssh"
)

func main() {
	quit := make(chan struct{}, 0)
	var passphraseError *ssh.PassphraseMissingError
	//bytes, err := os.ReadFile("file.txt")
	//if err != nil {
	//	fmt.Println(err)
	//	return
	//}
	//fmt.Println(string(bytes))
	document := js.Global().Get("document")
	document.Get("body").Call("insertAdjacentHTML", "beforeend", `
		<p>Choose private ssh key for signing (key does not leave browser):</p>
		<input type="file" id="file-selector">
		<output id="private-key"></output>
		<input type="text" id="file-name" value="This is a test">
	`)
	fileInput := document.Call("getElementById", "file-selector")
	fileOutput := document.Call("getElementById", "private-key")
	junk := document.Call("getElementById", "file-name")
	fileInput.Set("oninput", js.FuncOf(func(this js.Value, args []js.Value) any {
		fmt.Println("oninput called")
		fileInput.Get("files").Call("item", 0).Call("arrayBuffer").Call("then", js.FuncOf(func(this js.Value, args []js.Value) any {
			fmt.Println("arrayBuffer called")
			data := js.Global().Get("Uint8Array").New(args[0])
			dst := make([]byte, data.Get("length").Int())
			js.CopyBytesToGo(dst, data)
			if _, err := ssh.ParsePrivateKey(dst); err != nil {
				if errors.As(err, &passphraseError) {
					fmt.Println("Passphrase required")
					js.Global().Get("prompt").Invoke("Passphrase required")
					return nil
				}
				fmt.Println("Error parsing key:", err)
				js.Global().Get("alert").Invoke("Error parsing key: " + err.Error())

				//document.Call("alert", "Error parsing key: "+err.Error())
			}
			out := string(dst)
			if len(out) > 100 {
				out = out[:100] + "..."
			}
			fileOutput.Set("innerText", out)
			junk.Set("innerText", "Hello World")
			return nil
		}))
		return nil
	}))
	//fmt.Println("Hello, World!")
	//fmt.Println("This is a test")
	//x := js.Global().Get("document").Call("getElementById", "file-selector")
	//fmt.Println(x, x.Get("files").Index(0), x.Get("value"))
	//key := js.Global().Get("document").Call("getElementById", "private-key")
	//reader := js.Global().New("FileReader")
	//reader.Call("readAsText", x.Get("files").Index(0))
	//fmt.Println("reader", reader.Get("result"))

	//key.Set("value", reader.Get("result"))
	//y := js.Global().Get("document").Call("getElementById", "file-name")
	//fmt.Println(y, y.Get("value"))
	//y.Set("value", "Hello World")
	//x.Call("addEventListener", "change", js.FuncOf(func(this js.Value, args []js.Value) interface{} {
	//	fmt.Println("Changed")
	//	return nil
	//}))
	select {}
	<-quit
}
