package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/mail"
	"syscall/js"

	"golang.org/x/crypto/ssh"
)

var (
	fileInput  js.Value
	fileOutput js.Value
	fileName   js.Value
	userName   js.Value
	passphrase js.Value
	key        []byte
)

func main() {
	document := js.Global().Get("document")
	//	document.Get("body").Call("insertAdjacentHTML", "beforeend", `
	//<h1>Hello, WebAssembly!</h1>
	//<p>Choose private ssh key for signing (key does not leave browser)</p>
	//<input type="file" id="file"/>
	//<input type="text" id="name" value="this is a test"/>
	//<p><label for="username">Username</label>
	//<input type="text" id="username" name="username" placeholder="username"
	//	onchange=validateEmail() required/></p>
	//<p><output type="text" id="output"></output></p>
	//<p><input type="text" id="passphrase" hidden/></p>
	//<p><button type="button" id="button">Login</button><p>
	//`)
	fmt.Println("Hello World")
	fileInput = document.Call("getElementById", "file")
	fileOutput = document.Call("getElementById", "output")
	fileName = document.Call("getElementById", "name")
	userName = document.Call("getElementById", "username")
	passphrase = document.Call("getElementById", "passphrase")
	fileInput.Set("oninput", js.FuncOf(fileSelect))
	login := document.Call("getElementById", "button")
	login.Set("onclick", js.FuncOf(processLogin))
	select {}
}

func processLogin(this js.Value, p []js.Value) interface{} {
	username := userName.Get("value").String()
	passphrase := js.Global().Get("document").Call("getElementById", "passphrase").Get("value").String()
	message, err := revalidate(username, passphrase)
	if err != nil {
		js.Global().Get("alert").Invoke(message + err.Error())
		return nil
	}
	resp, err := http.Get("http://localhost:8080/hello")
	if err != nil {
		js.Global().Get("alert").Invoke("Error: " + err.Error())
		return nil
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		js.Global().Get("alert").Invoke("Error: " + err.Error())
		return nil
	}
	js.Global().Get("alert").Invoke(string(body))

	return nil
}

func fileSelect(this js.Value, p []js.Value) interface{} {
	fileInput.Get("files").Index(0).Call("arrayBuffer").Call("then", js.FuncOf(validate))
	return nil
}

func revalidate(username, passphrase string) (string, error) {
	fmt.Println("key", key)
	if _, err := mail.ParseAddress(username); err != nil {
		return "Invalid email", err
	}
	if passphrase == "" {
		if _, err := ssh.ParsePrivateKey(key); err != nil {
			fmt.Println("invalid key", err, key)
			return "Invalid key", err
		}
	} else {
		if _, err := ssh.ParsePrivateKeyWithPassphrase(key, []byte(passphrase)); err != nil {
			fmt.Println("invalid key or passphrase", err, key, passphrase)
			return "Invalid key or passphrase", err
		}
	}
	return "Valid", nil
}

func validate(this js.Value, p []js.Value) interface{} {
	var passphraseErr *ssh.PassphraseMissingError
	phrase := ""
	data := js.Global().Get("Uint8Array").New(p[0])
	dst := make([]byte, data.Get("byteLength").Int())
	cont := js.CopyBytesToGo(dst, data)
	fmt.Println("cont", cont)
	key = dst
	fmt.Println(key)
	if _, err := ssh.ParsePrivateKey(dst); err != nil {
		fileName.Set("value", "Invalid key")
		if errors.As(err, &passphraseErr) {
			fileName.Set("value", "Passphrase required")
			phrase = js.Global().Get("prompt").Invoke("Enter passphrase").String()
			fmt.Println("passphrase", phrase)
			js.Global().Get("document").Call("getElementById", "passphrase").Set("value", phrase)
			if _, err := ssh.ParsePrivateKeyWithPassphrase(dst, []byte(phrase)); err != nil {
				js.Global().Get("alert").Invoke("Invalid passphrase")
			}
		} else {
			js.Global().Get("alert").Invoke("Invalid key")
		}
	} else {
		fileName.Set("value", "Valid key")
		passphrase.Set("value", "")
	}
	out := string(dst)
	if len(out) > 25 {
		out = out[:25] + "..."
	}
	fileOutput.Set("value", out)
	return nil
}
