package main

import (
	"fmt"
	"log"

	"golang.org/x/exp/slices"
)

type Junk struct {
	Name string
}

func main() {
	junks := []string{"ubuntu-20.site", "test.network", "test.site"}
	log.Println(slices.Contains(junks, "test.site"))
	fmt.Println(slices.Replace(junks, 1, 2, "my.replacement"))

}
