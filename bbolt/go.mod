module github.com/mattkasun/bbolt

go 1.21.5

toolchain go1.21.6

require (
	github.com/boltdb/bolt v1.3.1
	github.com/boltdb/boltd v0.0.0-20150220181201-1f04e2021e45
	github.com/devilcove/boltdb v0.1.1
)

require (
	go.etcd.io/bbolt v1.3.8 // indirect
	golang.org/x/sys v0.13.0 // indirect
)
