package main

import (
	"errors"
	"fmt"
	"net"
	"strconv"
	"strings"
)

func main() {
	addr := net.UDPAddr{
		Port: 60024,
		IP:   net.ParseIP("127.0.0.1"),
	}
	ln, err := net.ListenUDP("udp", &addr)
	if err != nil {
		fmt.Println("port not avail", err)
	}
	fmt.Println("conn", ln)
	port, err := GetFreePort(60024)
	if err != nil {
		fmt.Println("err from getfreeport ", err)
	}
	fmt.Println("free port:", port)
	for i := 0; i < 10; i++ {
		port, _ = GetFreePort(60024)
		fmt.Println("free port:", port)
	}
	for i := 0; i < 10; i++ {
		port, _ = GetFreePort2(50820 + int32(i))
		fmt.Println("free port:", port)
	}
}

// GetFreePort - gets free port of machine
func GetFreePort(myport int32) (int32, error) {
	addr := net.UDPAddr{
		Port: int(myport),
	}
	conn, err := net.ListenUDP("udp", &addr)
	if err != nil {
		return 0, err
	}
	defer conn.Close()
	address := conn.LocalAddr().String()
	parts := strings.Split(address, ":")
	port, err := strconv.Atoi(parts[len(parts)-1])
	if err != nil {
		return int32(addr.Port), err
	}
	return int32(port), nil
}

func GetFreePort2(rangestart int32) (int32, error) {
	addr := net.UDPAddr{}
	if rangestart == 0 {
		//rangestart = NETCLIENT_DEFAULT_PORT
		rangestart = 51820
	}
	for x := rangestart; x <= 65535; x++ {
		addr.Port = int(x)
		ln, err := net.ListenUDP("udp", &addr)
		if err != nil {
			continue
		}
		defer ln.Close()
		return x, nil
	}
	return rangestart, errors.New("no free ports")
}
