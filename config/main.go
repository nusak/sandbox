package main

import "github.com/kr/pretty"

func main() {
	if err := configureServer(); err != nil {
		panic(err)
	}
	pretty.Println(config)
}
