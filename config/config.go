package main

import (
	"errors"
	"os"

	"github.com/spf13/viper"
)

type Configuration struct {
	Admin     string
	Pass      string
	Verbosity string
	FQDN      string
	Secure    bool
	Port      string
	Email     string
	DB        string
	Tables    []string
}

var (
	config       Configuration
	ErrServerURL = errors.New("invalid server URL")
)

func configureServer() error {
	viper.SetDefault("admin", "admin")
	viper.SetDefault("pass", "password")
	viper.SetDefault("verbosity", "INFO")
	viper.SetDefault("fqdn", "localhost")
	viper.SetDefault("secure", "false")
	viper.SetDefault("port", "8080")
	viper.SetDefault("db", "plexus-server.db")
	viper.SetDefault("tables", "[users,keys,networks,peers,settings]")
	viper.SetConfigFile(os.Getenv("HOME") + "/.config/plexus-server/config")
	viper.SetConfigType("yaml")
	if err := viper.ReadInConfig(); err != nil && !errors.Is(err, os.ErrNotExist) {
		return err
	}
	viper.SetEnvPrefix("PLEXUS")
	viper.AutomaticEnv()
	viper.Unmarshal(&config)

	//config = &Configuration{}
	//ok := false
	//if err := godotenv.Load(); err != nil {
	//	slog.Warn("Error loading .env file")
	//}
	//// get verbosity and setup logging
	//config.Verbosity, ok = os.LookupEnv("VERBOSITY")
	//if !ok {
	//	config.Verbosity = "INFO"
	//}
	//logger := plexus.SetLogging(config.Verbosity)
	// set server URL
	//config.ServerFQDN, ok = os.LookupEnv("PLEXUS_URL")
	//if !ok {
	//	config.ServerFQDN = "localhost"
	//}
	//config.SecureServer, ok = os.LookupEnv("PLEXUS_SECURE")
	//if !ok {
	//	config.SecureServer = "false"
	//}
	if config.Secure && config.FQDN == "localhost" {
		return errors.New("secure server requires FQDN")
	}
	if config.Secure && config.Email == "" {
		return errors.New("email address required")
	}
	// initalize database
	//home := os.Getenv("HOME")
	//config.DatabaseFile = os.Getenv("PLEXUS_DB")
	//if config.DatabaseFile == "" {
	//	config.DatabaseFile = home + "/.local/share/plexus/plexus-server.db"
	//	if err := os.MkdirAll(home+"/.local/share/plexus", os.ModePerm); err != nil {
	//		return logger, err
	//	}
	//}
	//config.Tables = []string{"users", "keys", "networks", "peers", "settings"}
	//dbFile := os.Getenv("HOME") + "/.local/share/plexus/" + config.DatabaseFile
	//if err := boltdb.Initialize(dbFile, config.Tables); err != nil {
	//	return logger, fmt.Errorf("init database %w", err)
	//}
	// check default user exists
	//config.Admin, ok = os.LookupEnv("PLEXUS_USER")
	//if !ok {
	//	config.Admin = "admin"
	//}
	//config.AdminPass, ok = os.LookupEnv("PLEXUS_PASS")
	//if !ok {
	//	config.AdminPass = "password"
	//}
	//if err := checkDefaultUser(config.Admin, config.AdminPass); err != nil {
	//	return logger, err
	//}
	return nil
}

func get() (*Configuration, error) {
	if config.Admin != "" {
		return &config, nil
	}
	err := configureServer()
	return &config, err
}
