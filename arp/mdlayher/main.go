package main

import (
	"fmt"
	"log"
	"net"
	"net/netip"
	"time"

	"github.com/mdlayher/arp"
)

func main() {
	iface, err := net.InterfaceByName("wlan0")
	if err != nil {
		log.Fatal("iface", err)
	}
	c, err := arp.Dial(iface)
	if err != nil {
		log.Fatal("dial", err)
	}
	_, cidr, err := net.ParseCIDR("192.168.1.1/24")
	if err != nil {
		log.Fatal("parseaddr", err)
	}
	ip, err := netip.ParseAddr("192.168.1.1")
	if err != nil {
		log.Fatal(err)
	}
	//first := nextIP(ip.Mask(cidr.Mask))
	for host := ip; cidr.Contains(host.AsSlice()); host = host.Next() {
		c.SetDeadline(time.Now().Add(time.Millisecond * 500))
		mac, err := c.Resolve(host)
		if err != nil {
			fmt.Println(host, err)
			continue
		}
		fmt.Println(host, mac)
	}
}

func nextIP(ip net.IP) net.IP {
	addr, _ := netip.AddrFromSlice(ip)
	return addr.Next().AsSlice()
}
