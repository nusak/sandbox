package main

import (
	"log"
	"net"
	"time"
)

func main() {
	_, err := net.DialTimeout("tcp", "192.168.1.1:1", time.Second*2)
	if err != nil {
		log.Println(err)
	}
	if _, err := net.Dial("tcp", "192.168.1.2:1"); err != nil {
		log.Println(err)
	}
	//conn.Close()
	_, err = net.DialTimeout("tcp", "192.168.1.2:1", time.Second*5)
	if err != nil {
		log.Println(err)
	}
	//conn1.Close()

}
