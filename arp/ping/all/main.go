package main

import (
	"fmt"
	"net"
	"sync"
	"time"

	pinger "github.com/go-ping/ping"
	"github.com/kr/pretty"
)

func Hosts(cidr string) ([]string, error) {
	ip, ipnet, err := net.ParseCIDR(cidr)
	if err != nil {
		return nil, err
	}

	var ips []string
	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); inc(ip) {
		ips = append(ips, ip.String())
	}
	// remove network address and broadcast address
	return ips[1 : len(ips)-1], nil
}

// http://play.golang.org/p/m8TNTtygK0
func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

//type Pong struct {
//	Ip    string
//	Alive bool
//}

// func ping(pingChan <-chan string, pongChan chan<- Pong, nodes map[string]string) {
func ping(wg *sync.WaitGroup, ip string, nodes map[string]string) {
	defer wg.Done()
	//for ip := range pingChan {
	//_, err := exec.Command("ping", "-c1", "-t1", ip).Output()
	p, err := pinger.NewPinger(ip)
	//		var alive bool
	if err != nil {
		//			pongChan <- Pong{}
		return
	}
	p.Count = 1
	p.Timeout = time.Second
	p.OnRecv = func(pkt *pinger.Packet) {
		//			alive = true
		nodes[pkt.Addr] = "alive"
	}
	err = p.Run()
	if err != nil {
		//	alive = false
	}
	//pongChan <- Pong{Ip: ip, Alive: alive}
}

//func receivePong(pongNum int, pongChan <-chan Pong, doneChan chan<- []Pong) {
//	var alives []Pong
//	for i := 0; i < pongNum; i++ {
//		pong := <-pongChan
//		//fmt.Println("received:", pong)
//		if pong.Alive {
//			alives = append(alives, pong)
//		}
//	}
//	doneChan <- alives
//}

func main() {
	hosts, _ := Hosts("192.168.1.0/24")
	nodes := make(map[string]string)
	//concurrentMax := len(hosts)
	//pingChan := make(chan string, concurrentMax)
	//pongChan := make(chan Pong, len(hosts))
	//doneChan := make(chan []Pong)
	wg := &sync.WaitGroup{}
	//for i := 0; i < concurrentMax; i++ {
	for _, ip := range hosts {
		wg.Add(1)
		go ping(wg, ip, nodes)
	}

	//go receivePong(len(hosts), pongChan, doneChan)

	//for _, ip := range hosts {
	//	pingChan <- ip
	//fmt.Println("sent: " + ip)
	//}

	//alives := <-doneChan
	wg.Wait()
	//pretty.Println(alives)
	for k, v := range nodes {
		fmt.Print(k, ":")
		pretty.Println(v)
	}
	//pretty.Println(nodes)
}
