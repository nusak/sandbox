package main

import (
	"fmt"

	"github.com/go-ping/ping"
	"github.com/kr/pretty"
)

func main() {
	pinger, err := ping.NewPinger("192.168.1.178")
	if err != nil {
		panic(err)
	}
	pinger.Count = 1
	pinger.SetPrivileged(false)
	pinger.OnRecv = func(pkt *ping.Packet) {
		fmt.Println("response from ", pkt.IPAddr)
	}
	pinger.OnDuplicateRecv = func(pkt *ping.Packet) {
		fmt.Println("duplicate from ", pkt.IPAddr)
	}
	err = pinger.Run() // Blocks until finished.
	if err != nil {
		panic(err)
	}
	stats := pinger.Statistics() // get send/receive/duplicate/rtt stats
	pretty.Println(stats)
}
