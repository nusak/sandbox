package main

import (
	"fmt"
	"net"
	"strconv"
	"sync"
	"time"

	"github.com/j-keck/arping"
)

func main() {
	//arping.EnableVerboseLog()
	arping.SetTimeout(time.Second * 2)
	wg := &sync.WaitGroup{}
	//ips := []int{1, 27, 28, 29}
	//for _, i := range ips {
	for i := 1; i < 255; i++ {
		x := i
		wg.Add(1)
		go func() {
			defer wg.Done()
			dstIP := net.ParseIP("192.168.1." + strconv.Itoa(x))
		label:
			if hwAddr, duration, err := arping.Ping(dstIP); err != nil {
				//fmt.Println(dstIP, err)
				if err.Error() == "interrupted system call" {
					fmt.Println(dstIP, err)
					goto label
				}
			} else {
				fmt.Printf("%s (%s) %d usec\n", dstIP, hwAddr, duration/1000)
			}
		}()
	}
	wg.Wait()
}
