package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/kr/pretty"
	"github.com/vishvananda/netlink"
)

func main() {
	hosts := []netlink.NeighUpdate{}
	update := make(chan netlink.NeighUpdate, 0)
	done := make(chan struct{})
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, os.Interrupt)

	if err := netlink.NeighSubscribe(update, done); err != nil {
		log.Fatal(err)
	}

	for {
		select {
		case <-quit:
			log.Println("quit")
			pretty.Println(hosts)
			close(done)
			return
		case x := <-update:
			log.Println(x.IP, x.HardwareAddr)
			hosts = append(hosts, x)
		}
	}
}
