package main

import (
	"fmt"
	"log"
	"net"
)

func main() {
	wlan, err := net.InterfaceByName("wlan0")
	if err != nil {
		log.Fatal(err)
	}
	addrs, err := wlan.Addrs()
	if err != nil {
		log.Fatal(err)
	}
	for _, val := range addrs {
		fmt.Println(val)
	}
	multi, err := wlan.MulticastAddrs()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("multi")
	for _, val := range multi {
		fmt.Println(val)
	}
}
