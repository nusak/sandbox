package main

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"math"
	"net"
	"net/netip"
	"os"
	"runtime"
	"sort"
	"strings"
	"sync"
	"text/tabwriter"
	"time"

	"github.com/c-robinson/iplib"
	"github.com/endobit/oui"
	"github.com/godbus/dbus/v5"
	"github.com/holoplot/go-avahi"
	"github.com/mostlygeek/arp"
	"github.com/pterm/pterm"
	"golang.org/x/net/icmp"
	"golang.org/x/net/ipv4"
)

type unreachable struct {
	IP  net.IP
	ERR error
}

type Node struct {
	IP     net.IP
	Name   string
	MAC    string
	Vendor string
	Alive  bool
	Err    error
}

func main() {
	if len(os.Args) != 2 {
		os.Args = append(os.Args, "192.168.1.0/24")
	}
	ip, cidr, err := net.ParseCIDR(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	//dbus setup
	conn, err := dbus.SystemBus()
	if err != nil {
		log.Fatal(err)
	}
	server, err := avahi.ServerNew(conn)
	if err != nil {
		log.Fatal(err)
	}
	first := nextIP(ip.Mask(cidr.Mask))
	broadcast := broadcast(cidr)
	nodes := make(map[string]Node)
	reachable := make(chan net.IP, 0)
	errors := make(chan unreachable, 0)
	done := make(chan bool)
	exit := make(chan bool)
	wg := &sync.WaitGroup{}
	spinner, _ := pterm.DefaultSpinner.Start("checking for hosts")
	for host := first; cidr.Contains(host); host = nextIP(host) {
		if host.Equal(broadcast) {
			continue
		}
		wg.Add(1)
		go ping(wg, host, reachable, errors)
	}
	go func() {
		//i := 1
		for {
			select {
			case <-done:
				spinner.Success("check complete")
				yes := 0
				no := 0
				ips := make([]net.IP, 0, yes)
				for k, v := range nodes {
					if v.Alive {
						yes++
						ips = append(ips, net.ParseIP(k))
					} else {
						no++
					}

				}
				fmt.Printf("\ntotal hosts: %v, reachable %v, unreachable %v\n\n", len(nodes), yes, no)
				// print out in order
				sort.Slice(ips, func(i, j int) bool {
					return bytes.Compare(ips[i], ips[j]) < 0
				})
				w := new(tabwriter.Writer)
				w.Init(os.Stdout, 15, 25, 2, ' ', 0)
				fmt.Fprintf(w, "IP\tHostname\tMacAddress\tVendor\n")
				for _, ip := range ips {
					//if ip.String() == "192.168.1.27" {
					//	continue
					//}
					host := nodes[ip.String()]
					fmt.Fprintf(w, "%s\t%s\t%s\t%s\n", host.IP.String(), host.Name, host.MAC, host.Vendor)
					//fmt.Println(nodes[ip.String()])
				}
				w.Flush()
				exit <- true
				return
			case ip := <-reachable:
				node := Node{IP: ip, Alive: true}
				mac := arp.Search(ip.String())
				node.Vendor = oui.Vendor(mac)
				node.MAC = mac
				node.Name = getName(server, ip.String())
				nodes[ip.String()] = node
				spinner.UpdateText("found " + ip.String())
			case no := <-errors:
				if no.IP.String() == "192.168.1.255" {
					continue
				}

				nodes[no.IP.String()] = Node{IP: no.IP, Alive: false, Err: no.ERR}
			}
		}
	}()
	wg.Wait()
	done <- true
	<-exit
}

func ping(wg *sync.WaitGroup, ip net.IP, reachable chan net.IP, errors chan unreachable) {
	defer wg.Done()
	switch runtime.GOOS {
	case "darwin", "ios":
	case "linux":
		//log.Println("you may need to adjust the net.ipv4.ping_group_range kernel state")
	default:
		log.Println("not supported on", runtime.GOOS)
		return
	}

	c, err := icmp.ListenPacket("udp4", "0.0.0.0")
	if err != nil {
		errors <- unreachable{IP: ip, ERR: err}
		return
	}
	defer c.Close()
	if err := c.SetReadDeadline(time.Now().Add(time.Millisecond * 500)); err != nil {
		errors <- unreachable{IP: ip, ERR: err}
		return
	}
	wm := icmp.Message{
		Type: ipv4.ICMPTypeEcho, Code: 0,
		Body: &icmp.Echo{
			ID: os.Getpid() & 0xffff, Seq: 1,
			Data: []byte("HELLO-R-U-THERE"),
		},
	}
	wb, err := wm.Marshal(nil)
	if err != nil {
		errors <- unreachable{IP: ip, ERR: err}
		return
	}
	if _, err := c.WriteTo(wb, &net.UDPAddr{IP: ip}); err != nil {
		errors <- unreachable{IP: ip, ERR: err}
		return
	}
	rb := make([]byte, 1500)
	_, _, err = c.ReadFrom(rb)
	if err != nil {
		errors <- unreachable{IP: ip, ERR: err}
		return
	}
	reachable <- ip
}

func numIPs(cidr *net.IPNet) int {
	maskSize, _ := cidr.Mask.Size()
	total := int(math.Pow(2, float64(32-maskSize))) - 2
	if total < 0 {
		total = 0
	}
	return total
}

func nextIP(ip net.IP) net.IP {
	addr, _ := netip.AddrFromSlice(ip)
	return addr.Next().AsSlice()
}

func broadcast(cidr *net.IPNet) net.IP {
	net4 := iplib.Net4FromStr(cidr.String())
	return net4.BroadcastAddress()
}

func getName(server *avahi.Server, ip string) string {
	ctx, _ := context.WithTimeout(context.Background(), time.Millisecond*500)
	ad, err := server.AddressResolverNew(avahi.InterfaceUnspec, avahi.ProtoUnspec, ip, 0)
	if err != nil {
		return "unavailable"
	}
	select {
	case <-ctx.Done():
		server.AddressResolverFree(ad)
		return "unavailable"
	case address := <-ad.FoundChannel:
		//server.AddressResolverFree(ad)
		server.AddressResolverFree(ad)
		return strings.Split(address.Name, ".")[0]
	}
}
