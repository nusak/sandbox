package main

import (
	"log"

	"github.com/bitfield/script"
)

func main() {
	id, err := script.File("/etc/os-release").Match("ID").String()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(id)
	ans, err := script.Exec("yum list docker").Match("docker").String()
	log.Println("answer", ans, "error", err)

}
