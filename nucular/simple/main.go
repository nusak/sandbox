package main

import (
	"fmt"

	"github.com/aarzilli/nucular"
	"github.com/aarzilli/nucular/style"
)

var count int

func main() {
	window := nucular.NewMasterWindow(0, "Counter", updatefn)
	window.SetStyle(style.FromTheme(style.DarkTheme, 2.0))
	window.Main()
}

func updatefn(w *nucular.Window) {
	w.Row(50).Dynamic(1)
	if w.ButtonText(fmt.Sprintf("increment: %d", count)) {
		count++
	}
}
