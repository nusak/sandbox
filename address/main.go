package main

import (
	"fmt"
	"net"

	"github.com/vishvananda/netlink"
)

func main() {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		panic(err)
	}
	for _, addr := range addrs {
		fmt.Println(addr)
	}
	fmt.Println()
	adds, err := netlink.AddrList(nil, netlink.FAMILY_V4)
	if err != nil {
		panic(err)
	}
	for _, addr := range adds {
		fmt.Println(addr.IP)
	}

}
