package main

import (
	"fmt"
)

var tracked *string

type Junk struct {
	Name string
}

func (j *Junk) Start() {
	if j == nil {
		j = &Junk{}
	}
	j.Name = "world"
}

func (j Junk) Update() {
	j.Name = "hello"
}

func main() {
	fmt.Println(*tracked)
}
