package main

import (
	"fmt"
	"log"
	"net"

	"github.com/kr/pretty"
)

func main() {
	var netIPNetCount, netIPAddrCount, otherCount int
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Fatal(err)
	}
	for _, i := range ifaces {
		pretty.Println(i)
		log.Println(i.Flags.String())

		if i.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if i.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := i.Addrs()
		if err != nil {
			log.Fatal(err)
		}
		for _, addr := range addrs {
			//log.Println(addr.String())
			switch v := addr.(type) {
			case *net.IPNet:
				_ = v
				netIPNetCount = netIPNetCount + 1
			case *net.IPAddr:
				netIPAddrCount = netIPAddrCount + 1
			default:
				otherCount = otherCount + 1
			}

		}
	}
	log.Println(netIPNetCount, netIPAddrCount, otherCount)

	ip := "161.216.164.97"
	port := "51821"
	endpoint, err := net.ResolveUDPAddr("udp", ip+":"+port)
	if err != nil {
		log.Fatal(err)
	}
	pretty.Println(endpoint)
	fmt.Println(endpoint.IP.String())

}
