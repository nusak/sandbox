package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/netip"

	"github.com/c-robinson/iplib"
	"github.com/devilcove/boltdb"
	"github.com/kr/pretty"
)

type Network struct {
	Name          string `form:"name"`
	ServerURL     string
	Net           iplib.Net4
	Address       netip.Addr
	AddressString string `form:"addressstring"`
	ListenPort    int
	Interface     string
}

func main() {
	addressString := "10.100.100.0/24"
	net := iplib.Net4FromStr(addressString)
	fmt.Println(net)
	pretty.Println(net)
	request := struct {
		Name          string `form:"name"`
		AddressString string `form:"addressstring"`
	}{
		AddressString: addressString,
		Name:          "test",
	}
	bytes, err := json.Marshal(request)
	if err != nil {
		panic(err)
	}
	network := Network{}
	err = json.Unmarshal(bytes, &network)
	if err != nil {
		panic(err)
	}
	network.ServerURL = "plexus.testing.nusak.ca"
	network.Net = iplib.Net4FromStr(network.AddressString)
	var ok bool
	network.Address, ok = netip.AddrFromSlice(network.Net.IP())
	if !ok {
		log.Println("net.ParseCIDR", network.AddressString)
		return
	}

	fmt.Println(network)
	pretty.Println(network)
	boltdb.Initialize("./test.db", []string{"networks"})
	if err := boltdb.Save(network, network.Name, "networks"); err != nil {
		panic(err)
	}

	networks, err := boltdb.GetAll[Network]("networks")
	if err != nil {
		panic(err)
	}
	pretty.Println(networks)
	netx, err := boltdb.Get[Network]("test", "networks")
	if err != nil {
		panic(err)
	}
	pretty.Println(netx)

	fmt.Println("marshal/unmarshal")
	bytes, err = json.Marshal(network)
	if err != nil {
		panic(err)
	}
	net2 := Network{}
	err = json.Unmarshal(bytes, &net2)
	if err != nil {
		panic(err)
	}
	pretty.Println(net2)
	iplibNet := iplib.Net4FromStr(net2.AddressString)
	broadcast := iplibNet.BroadcastAddress()
	fmt.Println(broadcast)

}
