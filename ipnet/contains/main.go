package main

import (
	"fmt"
	"net"
)

func main() {
	one := net.IPNet{
		IP:   net.IP{10, 10, 10, 0},
		Mask: net.CIDRMask(20, 32),
	}
	two := net.IPNet{
		IP:   net.IP{10, 10, 11, 0},
		Mask: net.CIDRMask(24, 32),
	}
	if one.Contains(two.IP) || two.Contains(one.IP) {
		fmt.Println("overlap")
	} else {
		fmt.Println("no overlap")
	}
}
