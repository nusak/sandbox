package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"unsafe"

	"github.com/gravitl/netmaker/models"
	"github.com/kr/pretty"
)

func main() {
	var junk models.AccessToken

	tokenbytes, err := base64.StdEncoding.DecodeString(os.Args[1])
	if err != nil {
		log.Fatal("error decoding token")
	}
	if err := json.Unmarshal(tokenbytes, &junk); err != nil {
		log.Fatal("err unmarshalliing")
	}

	fmt.Println(unsafe.Sizeof(junk))
	pretty.Println(junk)
}
