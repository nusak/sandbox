package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/kr/pretty"
)

type Release struct {
	Version     string `json:"name"`
	ReleaseDate string `json:"published_at"`
}

func main() {
	request, err := http.NewRequest(http.MethodGet, "https://api.github.com/repos/gravitl/netmaker/releases/latest", nil)
	client := http.Client{
		Timeout: time.Second * 30,
	}
	resp, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
	}
	var release Release
	bytes, _ := ioutil.ReadAll(resp.Body)
	//log.Println(string(bytes))
	if err := json.Unmarshal(bytes, &release); err != nil {
		//if err := json.NewDecoder(resp.Body).Decode(&release); err != nil {
		log.Fatal(err)
	}

	releaseDate, err := time.Parse(time.RFC3339, release.ReleaseDate)
	if err != nil {
		log.Println(releaseDate, err)
	}

	hours := time.Now().Sub(releaseDate).Hours()
	days := int(hours) / 24

	pretty.Println(release, hours, days)
}
