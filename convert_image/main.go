package main

import (
	"log"
	"os/exec"
	"strings"

	"github.com/kr/pretty"
)

func main() {
	cmd := exec.Command("magick", "vector", "-transparent", "white", "logo.png")
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(out))
	if strings.Contains(string(out), "Invalid TIFF directory") {
		cmd := exec.Command("magick", "logo-0.png", "-transparent", "white", "logo.png")
		out, err := cmd.CombinedOutput()
		if err != nil {
			log.Fatal(err)
		}
		pretty.Println(out)
		if string(out) != "" {
			log.Fatal("output from second conversion", string(out), out)
		}

	}

	log.Println("success")

}
