package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gravitl/netmaker/netclient/ncutils"
)

func main() {
	broker := "broker.clustercat.com"
	dir := ncutils.GetNetclientPathSpecific()
	log.Println(dir + broker)
	dir = ncutils.GetNetclientServerPath("broker.clustercat.com")
	log.Println(dir)
	if err := os.RemoveAll(dir + broker + "/"); err != nil {
		log.Println(err)
	}

	if err := os.RemoveAll("./dir"); err != nil {
		log.Println(err)
	}
	fmt.Println("good")

	files, err := os.ReadDir(dir + broker)
	if err != nil {
		log.Println("could not read dir ", err)
		return
	}
	for _, file := range files {
		if err := os.Remove(dir + broker + "/" + file.Name()); err != nil {
			log.Println("could not remove file ", file, err)
		}
	}

}
