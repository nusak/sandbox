package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gravitl/netmaker/netclient/ncutils"
	"github.com/kr/pretty"
	"github.com/mdlayher/genetlink"
	"github.com/mdlayher/netlink"
	"github.com/mdlayher/netlink/nlenc"
	"golang.org/x/sys/unix"
	"golang.zx2c4.com/wireguard/wgctrl"
)

func main() {
	out, err := ncutils.RunCmd("sudo wg", true)
	if err != nil {
		log.Fatal("wg ", err)
	}
	log.Println(out)
	wgclient, err := wgctrl.New()
	if err != nil {
		log.Fatal("wgctl ", err)
	}
	defer wgclient.Close()

	b, err := netlink.MarshalAttributes([]netlink.Attribute{{
		Type: unix.WGDEVICE_A_IFNAME,
		Data: nlenc.Bytes("nm-mesh"),
	}})
	if err != nil {
		log.Fatal("netlink.MarshalAttributes ", err)
	}
	pretty.Println("b", b)
	msg, err := Execute(wgclient, unix.WG_CMD_GET_DEVICE, netlink.Request|netlink.Dump, b)
	if err != nil {
		log.Fatal("execute ", err)
	}
	pretty.Println("msg ", msg)
	client, err := wgclient.Device("nm-mesh")
	if err != nil {
		log.Fatal("wgclient.Device ", err)
	}
	pretty.Println(client)

}

func Execute(c *wgctrl.Client, command uint8, flags netlink.HeaderFlags, attrb []byte) ([]genetlink.Message, error) {
	msg := genetlink.Message{
		Header: genetlink.Header{
			Command: command,
			Version: unix.WG_GENL_VERSION,
		},
		Data: attrb,
	}

	msgs, err := c.c.Execute(msg, c.family.ID, flags)
	if err == nil {
		return msgs, nil
	}

	// We don't want to expose netlink errors directly to callers so unpack to
	// something more generic.
	oerr, ok := err.(*netlink.OpError)
	if !ok {
		// Expect all errors to conform to netlink.OpError.
		return nil, fmt.Errorf("wglinux: netlink operation returned non-netlink error (please file a bug: https://golang.zx2c4.com/wireguard/wgctrl): %v", err)
	}

	switch oerr.Err {
	// Convert "no such device" and "not a wireguard device" to an error
	// compatible with os.ErrNotExist for easy checking.
	case unix.ENODEV, unix.ENOTSUP:
		return nil, os.ErrNotExist
	default:
		// Expose the inner error directly (such as EPERM).
		return nil, oerr.Err
	}
}
