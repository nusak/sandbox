package main

import (
	"log"
	"net"

	"github.com/c-robinson/iplib"
	"github.com/kr/pretty"
)

func main() {
	var address net.IP
	net6 := iplib.Net6FromStr("2605:b100:d3d:d35:5f0b:1943:d7e1:4036/64")
	log.Println(net6.FirstAddress(), net6.IP())
	if address == nil {
		log.Println("empty address")
	}
	pretty.Println(address)
}
