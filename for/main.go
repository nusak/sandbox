package main

import "fmt"

type Junk struct {
	Name string
	ID   string
}

func main() {
	one := Junk{Name: "hello", ID: "world"}
	two := Junk{Name: "xxxx", ID: "yyyy"}
	empty := []Junk{}
	full := []Junk{}

	full = append(full, one)
	full = append(full, two)

	fmt.Println("checking full")
	for i := range full {
		fmt.Println(full[i].Name)
	}
	fmt.Println("checking empty")
	for i := range empty {
		fmt.Println(empty[i].Name)
	}

}
