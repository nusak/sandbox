package main

import (
	"fmt"

	"github.com/kr/pretty"
)

type Peer struct {
	ID       int
	Endpoint string
}

var existing = []Peer{
	{
		ID:       1,
		Endpoint: "one",
	},
	{
		ID:       4,
		Endpoint: "one",
	},
	{
		ID:       8,
		Endpoint: "two",
	},
	{
		ID:       9,
		Endpoint: "two",
	},
}

func main() {
	var new = []Peer{
		{
			ID:       1,
			Endpoint: "one",
		},
		{
			ID:       3,
			Endpoint: "two",
		},
	}

	toKeep, toDelete, toAdd := findDiff(new, existing)
	pretty.Println("keep\n", toKeep, "delete\n", toDelete, "add\n", toAdd)

}

func findDiff(newPeers, oldPeers []Peer) ([]Peer, []Peer, []Peer) {
	var delete []Peer
	var keep []Peer
	var add []Peer
	for i, old := range oldPeers {
		foundOld := false
		foundNew := false
		for j, new := range newPeers {
			if foundNew {
				continue
			}
			//in both new an old so keep
			if old.ID == new.ID && old.Endpoint == new.Endpoint {
				fmt.Println("adding ", old.ID, " to keep", i, j)
				keep = append(keep, old)
				foundOld = true
				foundNew = true
				continue
				//same id but other attributed differ so modify
			} else if old.ID == new.ID {
				fmt.Println("adding ", old.ID, " to delete modifiy", i, j)
				delete = append(delete, old)
				fmt.Println("adding ", new.ID, " to add modify", i, j)
				add = append(add, new)
				foundOld = true
				foundNew = true
				continue
			}
			if !foundNew {
				fmt.Println("adding ", new.ID, " to add missing", i, j)
				add = append(add, new)
				continue
			}
		}
		if !foundOld {
			fmt.Println("adding ", old.ID, " to delete not found", i)
			delete = append(delete, old)
		}
	}
	return keep, delete, add
}
