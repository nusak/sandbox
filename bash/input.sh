#!/bin/bash
#
get_passwd() {
    echo "Enter password for plexus user"
    read PASS1
    echo "Retype plexus user password"
    read PASS2
}


echo "Enter Fully Qualified Domain Name of plexus server (eg. plexus.example.com)"
read FQDN
echo "Enter email to use with letsencrypt"
read EMAIL
echo "Enter name for plexus user"
read USER
PASS1="empty"
unset PASS2
while [ "$PASS1" != "$PASS2" ]
do
    get_passwd
done

echo password is: $PASS1
cat << EOF > ./config
fqdn: $FQDN
email: $EMAIL
adminname: $USER
adminpass: $PASS1
EOF

