package main

import (
	"errors"
	"log"
	"net/http"

	"github.com/devilcove/httpclient"
	"github.com/kr/pretty"
)

func main() {
	type Data struct {
		User string
		Pass string
	}
	data := Data{
		User: "demo",
		Pass: "pass1",
	}

	type Answer struct {
		Request Data
		Error   string
	}
	type JWT struct {
		JWT string
	}
	var response JWT
	var answer Answer

	endpoint := httpclient.JSONEndpoint[JWT, Answer]{
		URL:           "https://firefly.nusak.ca",
		Route:         "/login",
		Method:        http.MethodPost,
		Data:          data,
		Response:      JWT{},
		ErrorResponse: Answer{},
	}

	response, errResponse, err := endpoint.GetJSON(response, answer)
	if err != nil {
		var clientError *httpclient.ErrHttpClient
		if errors.Is(err, clientError) {
			pretty.Println("httpclient error: ", err)
			log.Fatal(errResponse)
		}
		if errors.As(err, &clientError) {
			pretty.Println("ErrStatus", clientError.Status)
			log.Fatal("")
		}

		//pretty.Println(err)
		if data, ok := err.(httpclient.ErrHttpClient); ok {
			pretty.Println(data.Status, data.Message, data.Error())
			//pretty.Println(data.Body)
		}

		log.Fatal("some error:", err)
	}
	log.Println("response", response)
}
