package main

import (
	"encoding/json"
	"log"
	"strings"

	"github.com/kr/pretty"
	"gopkg.in/ini.v1"
)

const (
	section_interface = "Interface"
	section_peers     = "Peer"
)

type PostUP struct {
	ID      string
	Command string
}

func main() {
	var post PostUP
	options := ini.LoadOptions{
		AllowNonUniqueSections: true,
		AllowShadows:           true,
	}
	postUpCmd := "kldload ipfw ipfw_nat ; "
	postUpCmd += "ipfw disable one_pass ; "
	postUpCmd += "ipfw nat 1 config if wg0 same_ports unreg_only reset ; "
	postUpCmd += "ipfw add 64000 reass all from any to any in ; "
	postUpCmd += "ipfw add 64000 nat 1 ip from any to any in via wg0 ; "
	postUpCmd += "ipfw add 64000 check-state ; "
	postUpCmd += "ipfw add 64000 nat 1 ip from any to any out via wg0 ; "
	postUpCmd += "ipfw add 65534 allow ip from any to any ; "
	postUpCmd += "nft add 'chain ip nat prerouting { type nat hook prerouting priority 0 ;}' ; "
	postUpCmd += "nft flush table filter ; nft flush table nat ; "
	pretty.Println("postup:", postUpCmd)
	post.ID = "one"
	post.Command = postUpCmd
	postData, _ := json.Marshal(&post)
	pretty.Println(string(postData))

	wireguard := ini.Empty(options)
	wireguard.Section(section_interface).Key("PrivateKey").SetValue("xhsshwprivatekey")
	wireguard.Section(section_interface).Key("ListenPort").SetValue("51820")
	wireguard.Section(section_interface).Key("Address").SetValue("10.10.10.1")
	wireguard.Section(section_interface).Key("Address").AddShadow("10.10.20.1")
	wireguard.Section(section_interface).Key("Address").AddShadow("hello world")
	wireguard.SectionWithIndex(section_peers, 0).Key("PublicKey").SetValue("kskfjakjlgjapublickey")
	wireguard.SectionWithIndex(section_peers, 0).Key("AllowedIps").SetValue("10.10.10.3/24")
	wireguard.SectionWithIndex(section_peers, 0).Key("AllowedIps").AddShadow("10.10.10.5/24")
	wireguard.SectionWithIndex(section_peers, 1).Key("PublicKey").SetValue("kskfjakjlgjapublickey")
	wireguard.SectionWithIndex(section_peers, 1).Key("AllowedIps").SetValue("10.10.10.2/24")
	wireguard.SectionWithIndex(section_peers, 1).Key("AllowedIps").AddShadow("10.10.10.6/24")
	wireguard.SectionWithIndex(section_peers, 1).Key("PostUp").SetValue("iptables -A FORWARD -i wg0 -j ACCEPT")
	wireguard.SectionWithIndex(section_peers, 1).Key("PostUp").AddShadow("iptables -A FORWARD -o wg0 -j ACCEPT")
	parts := strings.Split(postUpCmd, " ; ")
	for _, part := range parts {
		wireguard.SectionWithIndex(section_peers, 1).Key("PostUp").AddShadow(part)
	}

	if err := wireguard.SaveTo("/tmp/wg0.conf"); err != nil {
		log.Fatal("failed to save", err)
	}
	log.Println("done saving")

	wg, err := ini.LoadSources(options, "/tmp/wg0.conf")
	if err != nil {
		log.Fatal("failed to load file", err)
	}
	index, _ := wg.GetSection(section_interface)
	pkey := index.Key("PrivateKey").String()
	address := index.Key("Address").String()
	addressString := index.Key("Address").Strings(" ")
	addresses := index.Key("Address").StringsWithShadows(",")
	log.Println(pkey)
	log.Printf("address is %v %T", address, address)
	log.Printf("address is %v %T", addressString, addressString)
	log.Printf("address is %v %T", addresses, addresses)
	for _, address := range addresses {
		log.Printf("%v %T", address, address)
	}
	port := index.Key("ListenPort").StringsWithShadows(" ")
	log.Printf("%v %T", port, port)
	for _, ports := range port {
		pretty.Println(ports)
	}
}
