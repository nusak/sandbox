package main

import (
	"fmt"
	"strings"
)

func main() {
	filename := "vector.ept''tiff"
	parts := strings.Split(filename, ".")
	ext := parts[len(parts)-1]
	fmt.Println(ext)
	if !strings.Contains("svg epttiff ai", ext) {
		fmt.Println("not supported")
	} else {
		fmt.Println("supported")
	}
	fmt.Println(strings.Replace(strings.ToLower(ext), "'", "", -1))
}
