package main

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	router := setupRouter()
	//router.Use(sloggin.New(logger))
	if err := router.Run(":8080"); err != nil {
		log.Fatal(err)
	}
}

func setupRouter() *gin.Engine {
	router := gin.Default()
	router.LoadHTMLGlob("html/*.html")
	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "main", "")
	})
	router.POST("/example", func(c *gin.Context) {
		c.String(http.StatusOK, "Hello World this is a test")
	})
	return router
}
