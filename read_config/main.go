package main

import (
	"log"

	"github.com/gravitl/netclient/config"
	"github.com/kr/pretty"
)

func main() {
	cfg, err := config.ReadConfig("migrate")
	if err != nil {
		log.Fatal(err)
	}
	pretty.Println(cfg)
}
