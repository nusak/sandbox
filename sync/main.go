package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/devilcove/boltdb"
	"github.com/devilcove/plexus"
	"github.com/nats-io/nats-server/v2/server"
	"github.com/nats-io/nats.go"
)

func main() {
	wg := sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)

	wg.Add(1)
	go func() {
		defer wg.Done()
		defer log.Println("go routine done")
		ticker := time.NewTicker(2 * time.Second)
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				fmt.Println("tick")
			case <-ctx.Done():
				log.Println("ctx cancel")
				return
			}
		}
	}()
	//start interface
	if err := boltdb.Initialize("/home/mkasun/.local/share/plexus/plexus-agent.db", []string{"networks", "devices"}); err != nil {
		log.Fatal(err)
	}
	self, err := boltdb.Get[plexus.Device]("self", "devices")
	if err != nil {
		log.Fatal(err)
	}
	networks, err := boltdb.GetAll[plexus.Network]("networks")
	if err != nil {
		log.Fatal(err)
	}
	for _, network := range networks {
		if err := startInterface(self, network); err != nil {
			log.Fatal(err)
		}
	}
	//nats
	ns, err := server.NewServer(&server.Options{Host: "localhost", Port: 4225, NoSigs: true})
	if err != nil {
		log.Fatal(err)
	}
	ns.Start()
	if !ns.ReadyForConnections(10 * time.Second) {
		log.Fatal("nats server not ready")
	}
	nc, err := nats.Connect("nats://localhost:4225")
	if err != nil {
		log.Fatal(err)
	}
	ec, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		log.Fatal(err)
	}
	ec.Subscribe(">", func(subj, reply string, msg any) {
		fmt.Printf("Received a message on [%s]: %s\n", subj, msg)
	})

	ticker := time.NewTicker(2 * time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			fmt.Println("tick from main")
		case <-quit:
			log.Println("quit")
			cancel()
			///go deleteAllInterface(&wg)
			deleteAllInterface()
			log.Println("stop nats")
			ec.Drain()
			ns.Shutdown()
			ns.WaitForShutdown()
			log.Println("nats has been shutdown")
			wg.Wait()
			log.Println("main done")
			return
		}
	}
}
