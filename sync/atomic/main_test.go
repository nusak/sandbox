package main_test

import (
	"sync"
	"sync/atomic"
	"testing"
)

type atom struct {
	value int
	name  string
	junk  int
}

type mute struct {
	value int
	name  string
	junk  int
	lock  sync.RWMutex
}

var (
	a       = atomic.Pointer[atom]{}
	v       = atomic.Value{}
	m       = &mute{}
	aResult *atom
	mResult *mute
)

func BenchmarkAtomicValueWrite(b *testing.B) {
	for i := range b.N {
		v.Store(&atom{
			value: i,
			name:  "test",
			junk:  i + 1,
		})
	}
}

func BenchmarkAtomicWrite(b *testing.B) {
	for i := range b.N {
		a.Store(&atom{
			value: i,
			name:  "test",
			junk:  i + 1,
		})
	}
}

func BenchmarkMutexWrite(b *testing.B) {
	for i := range b.N {
		m.lock.Lock()
		m.value = i
		m.name = "test"
		m.junk = i + 1
		m.lock.Unlock()
	}
}

func BenchmarkAtomicValueRead(b *testing.B) {
	var x *atom
	for _ = range b.N {
		x = v.Load().(*atom)
	}
	aResult = x
}

func BenchmarkAtomicRead(b *testing.B) {
	var x *atom
	for _ = range b.N {
		x = a.Load()
	}
	aResult = x
}

func BenchmarkMutexRead(b *testing.B) {
	var x *mute
	for _ = range b.N {
		m.lock.RLock()
		x = m
		m.lock.RUnlock()
	}
	mResult = x
}
