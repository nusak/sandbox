package main

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/kr/pretty"
)

type Node struct {
	mu      sync.Mutex
	Value   int
	Pointer *int
}

var (
	node Node
	at   atomic.Pointer[Node]
)

func main() {
	dummy := 256
	node := Node{Value: 123, mu: sync.Mutex{}, Pointer: &dummy}
	at.Store(&node)

	updateTicker := time.NewTicker(10 * time.Second)
	atomicTicker := time.NewTicker(1 * time.Second)
	mutexTicker := time.NewTicker(1 * time.Second)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		atomicCount := 0
		mutexCount := 0
		for {
			select {
			case <-updateTicker.C:
				node.mu.Lock()
				mutexCount++
				node.Value = mutexCount
				fmt.Println("mutex:", node.Value)
				node.mu.Unlock()
				atomicCount++
				at.Store(&Node{Value: atomicCount, Pointer: &atomicCount})
				fmt.Println("atomic:", at.Load().Value)
			case <-atomicTicker.C:
				for _ = range 1000 {
					at.Load()
				}
				data := at.Load()
				fmt.Println("Atomic Count: ", data.Value, *data.Pointer)
			case <-mutexTicker.C:
				data := Node{}
				for _ = range 1000 {
					node.mu.Lock()
					data = node
					node.mu.Unlock()
				}
				fmt.Println("Mutex Count: ", data.Value, *data.Pointer)
			case <-quit:
				fmt.Println("Shutting down")
				updateTicker.Stop()
				atomicTicker.Stop()
				mutexTicker.Stop()
				wg.Done()
				return
			}
		}
	}()
	wg.Wait()
	pretty.Println(node)
	pretty.Println(at.Load())
}
