package main

import (
	"errors"
	"fmt"
	"log"
	"log/slog"
	"net"
	"slices"
	"time"

	"github.com/devilcove/boltdb"
	"github.com/devilcove/plexus"
	"github.com/vishvananda/netlink"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

func deleteInterface(name string) error {
	slog.Debug("deleting interface", "interface", name)
	defer log.Println("delete inteface done")
	link, err := netlink.LinkByName(name)
	if err != nil {
		return fmt.Errorf("interface does not exist %w", err)
	}
	log.Println(link.Attrs().Name, link.Attrs().Index)
	return netlink.LinkDel(link)
}

// func deleteAllInterface(wg *sync.WaitGroup) {
func deleteAllInterface() {
	//defer wg.Done()
	defer log.Println("all interfaces deleted")
	slog.Debug("deleting all interfaces")
	networks, err := boltdb.GetAll[plexus.Network]("networks")
	if err != nil {
		slog.Error("retrieve networks", "error", err)
		return
	}
	log.Printf("%d interfaces to delete", len(networks))
	for _, network := range networks {
		log.Println("calling deleteInterface", network.Interface)
		if err := deleteInterface(network.Interface); err != nil {
			slog.Error("delete interface", "error", err)
			return
		}
	}
}

func startAllInterfaces(self plexus.Device) {
	networks, err := boltdb.GetAll[plexus.Network]("networks")
	if err != nil {
		slog.Error("get networks", "error", err)
		return
	}
	for _, network := range networks {
		slog.Debug("starting interface", "interface", network.Interface, "network", network.Name, "server", network.ServerURL)
		if err := startInterface(self, network); err != nil {
			slog.Error("start interface", "network", network.Name, "interface", network.Interface, "error", err)
		}
	}
}

// func startInterfaces(ctx context.Context, wg *sync.WaitGroup) {
func startInterface(self plexus.Device, network plexus.Network) error {
	address := netlink.Addr{}
	for _, peer := range network.Peers {
		if peer.WGPublicKey == self.WGPublicKey {
			add := net.IPNet{
				IP:   peer.Address.IP,
				Mask: network.Net.Mask,
			}
			address.IPNet = &add
			break
		}
	}
	if address.IPNet == nil {
		return errors.New("no address for network" + network.Name)
	}
	privKey, err := wgtypes.ParseKey(self.WGPrivateKey)
	if err != nil {
		slog.Error("unable to parse private key", "error", err)
		return err
	}
	if _, err := netlink.LinkByName(network.Interface); err == nil {
		slog.Info("interface exists", "interface", network.Interface)
		return err
	}
	mtu := 1420
	peers := getWGPeers(self, network)
	port, err := getFreePort(network.ListenPort)
	if err != nil {
		return err
	}
	config := wgtypes.Config{
		PrivateKey:   &privKey,
		ListenPort:   &port,
		ReplacePeers: true,
		Peers:        peers,
	}
	link := plexus.New(network.Interface, mtu, address, config)
	if err := link.Up(); err != nil {
		slog.Error("failed initializition interface", "interface", network.Interface, "error", err)
		return err
	}
	return nil
}

func addPeertoInterface(name string, peer plexus.NetworkPeer) error {
	iface, err := plexus.Get(name)
	if err != nil {
		return err
	}
	key, err := wgtypes.ParseKey(peer.WGPublicKey)
	if err != nil {
		return err
	}
	keepalive := time.Second * 20
	iface.Config.Peers = append(iface.Config.Peers, wgtypes.PeerConfig{
		PublicKey: key,
		Endpoint: &net.UDPAddr{
			IP:   net.ParseIP(peer.Endpoint),
			Port: peer.PublicListenPort,
		},
		PersistentKeepaliveInterval: &keepalive,
		ReplaceAllowedIPs:           true,
		AllowedIPs: []net.IPNet{
			{
				IP:   peer.Address.IP,
				Mask: net.CIDRMask(32, 32),
			},
		},
	})
	return iface.Apply()
}

func deletePeerFromInterface(name string, peerToDelete plexus.NetworkPeer) error {
	iface, err := plexus.Get(name)
	if err != nil {
		return err
	}
	key, err := wgtypes.ParseKey(peerToDelete.WGPublicKey)
	if err != nil {
		return err
	}
	for i, peer := range iface.Config.Peers {
		if peer.PublicKey == key {
			iface.Config.Peers[i].Remove = true
		}
	}
	return iface.Apply()
}

func getFreePort(start int) (int, error) {
	addr := net.UDPAddr{}
	if start == 0 {
		start = 51820
	}
	for x := start; x <= 65535; x++ {
		addr.Port = x
		conn, err := net.ListenUDP("udp", &addr)
		if err != nil {
			continue
		}
		conn.Close()
		return x, nil
	}
	return 0, errors.New("no free ports")
}

func getAllowedIPs(relay plexus.NetworkPeer, peers []plexus.NetworkPeer) []net.IPNet {
	allowed := []net.IPNet{}
	for _, peer := range peers {
		if peer.IsRelayed {
			if slices.Contains(relay.RelayedPeers, peer.WGPublicKey) {
				allowed = append(allowed, peer.Address)
			}
		}
	}
	return allowed
}

func getWGPeers(self plexus.Device, network plexus.Network) []wgtypes.PeerConfig {
	keepalive := time.Second * 20
	peers := []wgtypes.PeerConfig{}
	for _, peer := range network.Peers {
		slog.Info("checking peer", "peer", peer.WGPublicKey, "address", peer.Address, "mask", network.Net.Mask)
		if peer.WGPublicKey == self.WGPublicKey {
			continue
		}
		if peer.IsRelayed {
			continue
		}
		pubKey, err := wgtypes.ParseKey(peer.WGPublicKey)
		if err != nil {
			slog.Error("unable to parse public key", "key", peer.WGPublicKey, "error", err)
			continue
		}
		allowed := []net.IPNet{}
		if peer.IsRelay {
			allowed = getAllowedIPs(peer, network.Peers)
		}
		allowed = append(allowed, net.IPNet{
			IP:   peer.Address.IP,
			Mask: net.CIDRMask(32, 32),
		})
		wgPeer := wgtypes.PeerConfig{
			PublicKey:         pubKey,
			ReplaceAllowedIPs: true,
			AllowedIPs:        allowed,
			Endpoint: &net.UDPAddr{
				IP:   net.ParseIP(peer.Endpoint),
				Port: peer.PublicListenPort,
			},
			PersistentKeepaliveInterval: &keepalive,
		}
		peers = append(peers, wgPeer)
	}
	return peers
}
