package main

import (
	"errors"
	"fmt"
	"time"

	"github.com/kr/pretty"
)

type Node struct {
	ID            string
	PendingDelete bool
	Action        string
	LastModified  time.Time
}

var nodes = make(map[string]Node)

func main() {
	node := Node{
		ID: "1",
	}
	node2 := Node{
		ID: "2",
	}
	node3 := Node{
		ID: "3",
	}
	nodes[node.ID] = node
	nodes[node2.ID] = node2
	nodes[node3.ID] = node3

	go DeleteNodeCleanUp()
	pretty.Println(nodes)
	// simulate call from UI
	if err := DeleteNode(&node, false); err != nil {
		fmt.Println("error from delete node ", err)
	}
	pretty.Println(nodes)
	// simulate call from UI
	if err := DeleteNode(&node2, false); err != nil {
		fmt.Println("error from delete node ", err)
	}
	pretty.Println(nodes)
	// simulate call from node withh no prior call from UI
	if err := DeleteNode(&node3, true); err != nil {
		fmt.Println("error from delete node ", err)
	}
	pretty.Println(nodes)
	time.Sleep(time.Second * 1)
	// simulate call from node
	if err := DeleteNode(&node, true); err != nil {
		fmt.Println("error from delete node ", err)
	}
	pretty.Println(nodes)
	time.Sleep(time.Second * 10)
	// simulate call from node after long delay after goroutine has deleted node
	if err := DeleteNode(&node2, true); err != nil {
		fmt.Println("error from delete node ", err)
	}
	pretty.Println(nodes)
	fmt.Println("done")
}

func DeleteNodeCleanUp() {
	fmt.Println("starting goroutine")
	defer fmt.Println("goroutine done .... returning")
	ticker := time.NewTicker(time.Second * 3)
	defer ticker.Stop()
	stop := time.NewTicker(time.Second * 30)
	defer stop.Stop()
	for {
		select {
		case <-ticker.C:
			fmt.Println("checking pending nodes")
			pretty.Println(nodes)
			for id, value := range nodes {
				fmt.Println("checking node ", id)
				if time.Since(value.LastModified) > 3*time.Second {
					if err := DeleteNode(&value, true); err != nil {
						fmt.Println("error deleting node ", id, err)
					} else {
						fmt.Println("deleted node ", id, " from goroutine")
					}
					fmt.Println("node ", id, " is ok", time.Since(value.LastModified).String)

				}
			}
		case <-stop.C:
			fmt.Println("stopping goroutine")
		}
	}
}

func DeleteNode(node *Node, fromNode bool) error {
	fmt.Println("deleteNode ", node.ID, fromNode)
	if !fromNode {
		newnode := node
		newnode.PendingDelete = true
		newnode.Action = "DELETE"
		newnode.LastModified = time.Now()
		if err := UpdateNode(node, newnode); err != nil {
			return err
		}
		return nil
	}
	if err := DeleteNodeByID(node); err != nil {
		return err
	}
	return nil
}

func UpdateNode(old, new *Node) error {
	fmt.Println("updating node ", new)
	nodes[new.ID] = *new
	return nil
}

func DeleteNodeByID(node *Node) error {
	fmt.Println("delete node by id", node.ID)
	if _, ok := nodes[node.ID]; !ok {
		return errors.New("node does not exist")
	}
	delete(nodes, node.ID)
	pretty.Println("deleteNodebyID", nodes)
	return nil
}
