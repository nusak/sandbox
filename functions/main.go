package main

import (
	"encoding/json"
	"log"

	"github.com/kr/pretty"
)

type User struct {
	ID   int
	Name string
}
type NewUser struct {
	ID   int
	Name string
	Age  int
}

type Operator func(map[int]string, any) map[int]string

type Op2 func(any) any

var junk = "hello"
var junk2 = ""

func main() {
	storage := make(map[int]string)
	original := User{Name: "bob"}
	storage = save(storage, original)
	//log.Println(decode(storage))
	//pretty.Println(storage)
	var newUser NewUser
	newUser.ID = 1
	//op := updateToNewUser
	//storage = migrate(storage, newUser, op)
	//pretty.Println(storage)
	op := update2

	junk = migrate2(op, junk).(string)
	//junk = update.(string)
	log.Println(junk, junk2)

}

func migrate2(op Op2, data any) any {
	return op(data)
}

func update2(in any) any {
	log.Println("migrate2", in)
	junk2 = "testing"
	return "world"
}

func migrate(store map[int]string, data any, op Operator) map[int]string {
	log.Println("migrate")
	pretty.Println(store, data)
	return op(store, data)
}

func decode(storage map[int]string) User {
	var user User
	if err := json.Unmarshal([]byte(storage[1]), &user); err != nil {
		log.Fatal(err)
	}
	return user
}
func save(storage map[int]string, data any) map[int]string {
	value, err := json.Marshal(data)
	if err != nil {
		log.Fatal("marshal error", err)
	}
	storage[1] = string(value)
	return storage
}

func updateToNewUser(storage map[int]string, data any) map[int]string {
	//store := []byte(storage[1])
	log.Println("updateToNewUser")
	pretty.Println(storage[1], data)
	if err := json.Unmarshal([]byte(storage[1]), &data); err != nil {
		log.Fatal("unmarshal error", err)
	}
	newData := data.(NewUser)
	newData.Age = 25
	bytes, err := json.Marshal(newData)
	if err != nil {
		log.Fatal("marshal error", err)
	}
	storage[1] = string(bytes)
	return storage
}
