package main

import (
	"fmt"
	"strings"
	"unicode"

	"github.com/hashicorp/go-version"
)

const MinVersion = "v0.17.0-testing"

func main() {

	//version1 := "v0.17.1-testing"
	//version2 := "v0.17.2"

	//v1, err := version.NewVersion(version1)
	//if err != nil {
	//	log.Fatal(err)
	//}
	//v2, err := version.NewVersion(version2)
	//if err != nil {
	//	log.Fatal(err)
	//}
	//if v1.GreaterThanOrEqual(v2) {
	//	fmt.Printf("%s is GreaterThanOrEqual than %s\n", v1, v2)
	//} else {
	//	fmt.Printf("%s is GreaterThanOrEqual than %s\n", v2, v1)
	//}

	//constraints, err := version.NewConstraint(">= " + version1)
	//if err != nil {
	//	log.Fatal(err)
	//}
	//fmt.Println("constraint", constraints)
	//fmt.Println("constraint check on ", v2.String(), constraints.Check(v2))

	//replace := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	//result := strings.ReplaceAll(version1, replace, "")
	//fmt.Println(result)

	//fmt.Println(IsVersionComptatible("v0.18.2-testing"))
	//fmt.Println(IsVersionComptatible("v0.17.1"))
	//fmt.Println(IsVersionComptatible("dev"))
	//fmt.Println(IsVersionComptatible("v0.14.0"))
	//fmt.Println(IsVersionComptatible("testing"))
	//fmt.Println(IsVersionComptatible("v0.18"))
	fmt.Println(IsVersionComptatible("v0.0.3"))
	//fmt.Println("triming", strings.TrimFunc("v0.18.2-testing", func(r rune) bool {
	//	return !unicode.IsNumber(r)
	//}))

}

func IsVersionComptatible(ver string) bool {
	// during dev, assume developers know what they are doing
	if ver == "dev" {
		return true
	}
	trimmed := strings.TrimFunc(ver, func(r rune) bool {
		return !unicode.IsNumber(r)
	})
	v, err := version.NewVersion(trimmed)
	if err != nil {
		return false
	}
	fmt.Println("testing ", v)
	constraint, err := version.NewConstraint(">= " + MinVersion)
	if err != nil {
		return false
	}
	return constraint.Check(v)

}
