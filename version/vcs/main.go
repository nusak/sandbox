package main

import (
	"fmt"
	"runtime/debug"

	"github.com/kr/pretty"
)

func main() {
	info, _ := debug.ReadBuildInfo()
	fmt.Println(info)
	pretty.Println(info.Settings)
	//for _, v := range info.Settings {
	//	if strings.Contains(v, revision) {
	//		commit, _ := strings.Split(v, " ")
	//		fmt.Println(commit[1])
	//	}
	//}
	if info, ok := debug.ReadBuildInfo(); ok {
		for _, setting := range info.Settings {
			if setting.Key == "vcs.revision" {
				fmt.Println(setting.Value)
			}
		}
	}

}
