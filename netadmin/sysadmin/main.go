package main

import (
	"log"
	"os/exec"
)

func main() {
	cmd := exec.Command("ip", "a", "add", "10.200.20.1", "dev", "wlan0")
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatal(string(out), " ", err)
	}
	log.Println("success")
}
