package main

import (
	"fmt"
	"log"
	"os/exec"

	"github.com/vishvananda/netlink"
	"golang.zx2c4.com/wireguard/wgctrl"
)

func main() {
	cmd := exec.Command("sudo", "ip", "a", "add", "10.200.20.1", "dev", "wlan0")
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatal(string(out), " ", err)
	}
	log.Println("success")
	wg, err := netlink.LinkByName("plexus0")
	if err != nil {
		log.Fatal("linkbyName ", err)
	}
	log.Println(wg.Attrs().Name)
	client, err := wgctrl.New()
	if err != nil {
		log.Fatal("wgctrl ", err)
	}
	device, err := client.Device("plexus0")
	if err != nil {
		log.Fatal("client device ", err)
	}
	fmt.Println("listen port ", device.ListenPort)
}
