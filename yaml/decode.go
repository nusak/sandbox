package main

import (
	"fmt"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Name    string
	Address string
}

func main() {
	config := Config{
		Name:    "hello",
		Address: "world",
	}

	fmt.Println("in memory", config)
	f, err := os.Open("./config.yml")
	if err != nil {
		log.Fatal(err)
	}
	if err := yaml.NewDecoder(f).Decode(&config); err != nil {
		log.Fatal(err)
	}
	log.Println("read append?", config)
	f.Close()
	f, _ = os.Open("./config.yml")
	config = Config{}
	if err := yaml.NewDecoder(f).Decode(&config); err != nil {
		log.Fatal(err)
	}
	log.Println("read init", config)
}
