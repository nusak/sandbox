package main

import (
	"log"
	"net"
	"os"
	"strconv"

	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
)

type Data struct {
	Name string
	IP   net.IPNet
}

func main() {
	ip, cidr, err := net.ParseCIDR("192.168.8.110/24")
	if err != nil {
		log.Fatal(err)
	}
	log.Println(ip, cidr)
	data := Data{
		Name: "test",
		IP:   *cidr,
	}
	f, err := os.OpenFile("/tmp/data.yml", os.O_CREATE|os.O_WRONLY|os.O_TRUNC, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	if err := yaml.NewEncoder(f).Encode(data); err != nil {
		log.Fatal(err)
	}
	f.Sync()

	input := "1"
	result, err := strconv.ParseBool(input)
	if err != nil {
		log.Fatal("input", input, "result", result, "error", err)
	}
	log.Println(result)
	_, cidr, _ = net.ParseCIDR("192.355.8.0/24")
	log.Println(cidr)

	g, err := os.Open("/tmp/data.yml")
	if err != nil {
		log.Fatal(err)
	}
	defer g.Close()
	var newdata Data
	if err := yaml.NewDecoder(g).Decode(&newdata); err != nil {
		log.Fatal(err)
	}
	log.Println(newdata)

	viper.SetConfigFile("/tmp/data.yml")
	viper.SetConfigType("yml")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("viper err", err)
	}
	var viperData Data
	//if err := viper.Unmarshal(&viperData); err != nil {
	//	log.Fatal("viper unmarshal", err)
	//}

	c := viper.AllSettings()
	b, _ := yaml.Marshal(c)
	yaml.Unmarshal(b, &viperData)
	log.Println("viperData", viperData)
	log.Println("yea")
}
