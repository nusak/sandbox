package main

import (
	"fmt"
	"log"
	"net/netip"
)

func main() {
	ip, err := netip.ParseAddr("192.168.1.1")
	if err != nil {
		log.Fatal(err)
	}
	if err != nil {
		log.Fatal(err)
	}
	end, err := netip.ParseAddr("192.168.1.255")
	c := make(chan netip.Addr)
	d := make(chan int)
	done := make(chan bool)

	go func() {
		nodata := 0
		ips := 0
		junk := 0
		for {
			select {
			case ip := <-c:
				ips++
				if ip == end.Prev() {
					fmt.Println("last ip received", ip)
				}
			case <-d:
				junk++
			case <-done:
				fmt.Println(nodata, ips, junk)
				return
			default:
				nodata++
			}
		}
	}()

	for {
		c <- ip
		ip = ip.Next()
		if ip == end {
			d <- 1
			break
		}
	}
	done <- true
}
