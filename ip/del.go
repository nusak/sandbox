package main

import (
	"log"
	"net"

	"github.com/vishvananda/netlink"
)

func main() {

	//	routes, err := netlink.RouteGet(net.ParseIP("10.51.21.254"))
	//	if err != nil {
	//		log.Fatal("route err ", err)
	//	}
	//	log.Println("success", routes)
	//	routes, err = netlink.RouteGet(net.ParseIP("10.51.20.1"))
	//	if err != nil {
	//		log.Fatal("route err ", err)
	//	}
	//	log.Println("success", routes)
	routes, err := netlink.RouteGet(net.ParseIP("10.10.100.2"))
	if err != nil {
		log.Println("route err ", err)
	}
	log.Println("success", routes)
	// deleting
	link, err := netlink.LinkByName("nm-netmaker")
	if err != nil {
		log.Fatal("link by nane ", err)
	}
	_, dest, err := net.ParseCIDR("10.10.100.2/32")
	if err != nil {
		log.Fatal("parse cider ", err)
	}
	route := &netlink.Route{
		LinkIndex: link.Attrs().Index,
		Dst:       dest,
	}
	log.Println(route)
	if err := netlink.RouteDel(&routes[0]); err != nil {
		log.Fatal("route del ", err)
	}
}
