package main

import (
	"fmt"
	"net"
	"net/netip"

	"github.com/c-robinson/iplib"
	"github.com/kr/pretty"
)

func main() {
	ip, network, err := iplib.ParseCIDR("192.168.0.10/30")
	if err != nil {
		panic(err)
	}
	pretty.Println("iplib", ip, "\n", network)
	fmt.Println(ip, network, network.FirstAddress(), network.LastAddress())
	for i := 0; i < 260; i++ {
		ip = iplib.IncrementIPBy(ip, 1)
		fmt.Println(ip, network.Contains(ip), network.(iplib.Net4).BroadcastAddress())
		if !network.Contains(ip) {
			break
		}
	}
	fmt.Println(ip, network.Contains(ip))
	//fmt.Println("iplib Increment", iplib.IncrementIPBy(ip, 1))
	ip, cidr, err := net.ParseCIDR("192.168.0.0/30")
	if err != nil {
		panic(err)
	}
	pretty.Println("net", ip, "\n", cidr)
	fmt.Println(ip, cidr)
	addr := netip.MustParseAddr("192.168.0.0")
	pretty.Println("netip", addr)
	fmt.Println(addr)
}
