package main

import (
	"fmt"
	"net"
)

func main() {
	var ip net.IP
	fmt.Println(ip)
	fmt.Println(ip.String(), ip.DefaultMask().String())
	if ip == nil {
		fmt.Println("ip is nil")
	}
}
