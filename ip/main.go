package main

import (
	"fmt"
	"log"
	"os"

	"github.com/vishvananda/netlink"
	"golang.zx2c4.com/wireguard/device"
	"golang.zx2c4.com/wireguard/tun"
)

func main() {
	var err error
	var dev tun.Device

	la := netlink.NewLinkAttrs()
	la.Name = "wg1"
	tunFdStr := os.Getenv("ENV_TUN_FD")
	if tunFdStr == "" {
		dev, err = tun.CreateTUN("wg1", device.DefaultMTU)
	}
	if err != nil {
		log.Fatal("error creating tun ", err)
	}

	fmt.Println("data", tunFdStr)
	fmt.Println("data", dev)
}
