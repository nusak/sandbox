package main

import (
	"fmt"
	"log"
	"net"

	"github.com/vishvananda/netlink"
)

func main() {
	links, err := netlink.LinkList()
	if err != nil {
		log.Fatal(err)
	}
	for _, link := range links {
		fmt.Println(link.Attrs().Name)
		addrs, err := netlink.AddrList(link, netlink.FAMILY_V4)
		if err != nil {
			log.Println("skipping", link.Attrs().Name)
			continue
		}
		for _, addr := range addrs {
			ip, cidr, err := net.ParseCIDR(addr.IPNet.String())
			if err != nil {
				log.Println(err)
				continue
			}
			fmt.Println(link.Attrs().Name, addr, ip, cidr)
		}
	}
}
