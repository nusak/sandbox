package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime/debug"
	"syscall"

	"github.com/caddyserver/certmagic"
	"github.com/gin-gonic/gin"
	"github.com/nats-io/nats-server/v2/server"
)

func main() {
	debug.SetTraceback("single")
	certmagic.DefaultACME.Agreed = true
	certmagic.DefaultACME.Email = "mkasun@nusak.ca"
	certmagic.DefaultACME.CA = certmagic.LetsEncryptProductionCA
	secure := false
	if _, ok := os.LookupEnv("SECURE"); ok {
		secure = true
		fmt.Println("using secure mode")
	} else {
		fmt.Println("using insecure mode")
	}

	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.String(200, "Hello, World!")
	})
	webserver := &http.Server{
		Addr:    ":8080",
		Handler: r,
	}
	opts := server.Options{
		Port:   4222,
		NoSigs: true,
	}
	if secure {
		tlsConfig, err := certmagic.TLS([]string{"certmagic.nusak.ca"})
		if err != nil {
			panic(err)
		}
		opts.TLSConfig = tlsConfig
		opts.Host = "certmagic.nusak.ca"
		webserver.TLSConfig = tlsConfig
		webserver.Addr = ":443"
		go func() {
			if err := webserver.ListenAndServeTLS("", ""); err != nil && err != http.ErrServerClosed {
				fmt.Println("listen: %s\n", err)
			}
		}()
	} else {
		go func() {
			if err := webserver.ListenAndServe(); err != nil && err != http.ErrServerClosed {
				fmt.Println("listen: %s\n", err)
			}
		}()
	}
	defer webserver.Shutdown(context.Background())
	s := server.New(&opts)
	go s.Start()
	defer s.Shutdown()
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	<-quit
}
