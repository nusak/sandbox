package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"

	"github.com/caddyserver/certmagic"
	"github.com/gin-gonic/gin"
)

func main() {
	wg := sync.WaitGroup{}
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	ctx, cancel := context.WithCancel(context.Background())
	go web(ctx, &wg)
	for {
		select {
		case <-quit:
			cancel()
			wg.Wait()
			return
		}
	}
}

func web(ctx context.Context, wg *sync.WaitGroup) {
	wg.Add(1)
	defer wg.Done()
	certmagic.DefaultACME.Agreed = true
	certmagic.DefaultACME.Email = "mkasun@nusak.ca"
	certmagic.DefaultACME.CA = certmagic.LetsEncryptProductionCA
	router := setupRouter()
	//tlsConfig, err := certmagic.TLS([]string{"plexus.testing.nusak.ca"})
	//if err != nil {
	//	log.Println(err)
	//	return
	//}
	//srv := &http.Server{
	//	Addr:      ":443",
	//	Handler:   router,
	//	TLSConfig: tlsConfig,
	//}

	go func() {
		//if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		if err := certmagic.HTTPS([]string{"plexus.testing.nusak.ca"}, router); err != nil {
			log.Printf("https error: %s\n", err)
			return
		}
	}()

	<-ctx.Done()
	//if err := srv.Shutdown(ctx); err != nil {
	//	log.Printf("https shutdown error: %s\n", err)
	//}
}

func setupRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "Hello World")
	})
	return router
}
