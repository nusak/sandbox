package main

import (
	"fmt"
	"log"
)

func main() {
	a := []string{"one", "two", "three"}
	b := []string{"one", "two", "three"}
	c := []string{"one", "two", "three"}
	fmt.Println(remove_element(a, "one"))
	fmt.Println(remove_element(b, "two"))
	fmt.Println(remove_element(c, "three"))
}

func remove_element(slice []string, element string) []string {
	log.Println(slice, len(slice), element)

	for i := len(slice) - 1; i >= 0; i-- {
		//for i, _ := range slice {
		//for i := 0; i < len(slice)-1; i++ {
		//log.Println(i)
		//fmt.Println(slice[i], element)
		if slice[i] == element {
			log.Println("removing element ", i)
			slice = append(slice[:i], slice[i+1:]...)
		}
	}
	return slice
}
