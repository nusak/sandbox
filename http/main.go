package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

type data struct {
	Content string
	Time    int
}

type response struct {
	Content string
}

func main() {
	http.HandleFunc("POST /data", handler)
	if err := http.ListenAndServe(":9091", nil); err != nil {
		log.Println("listenAndServe", err)
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	var data data
	w.Header().Set("Content-Type", "application/json")
	defer r.Body.Close()
	bytes, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println("read body", err)
		return
	}
	fmt.Println(string(bytes))
	if err := json.Unmarshal(bytes, &data); err != nil {
		log.Println("unmarshal", err)
		return
	}
	fmt.Println(data)
	payload, err := json.Marshal(response{Content: "done"})
	if err != nil {
		log.Println("marshal", err)
		return
	}
	w.Write(payload)
}
