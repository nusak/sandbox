package main

import (
	"log"

	"github.com/kr/pretty"
	"github.com/txn2/txeh"
)

func main() {
	config := txeh.HostsConfig{
		ReadFilePath:  "./hosts",
		WriteFilePath: "./hosts",
	}
	hosts, err := txeh.NewHosts(&config)
	if err != nil {
		panic(err)
	}

	lines := hosts.GetHostFileLines()
	last := len(*lines)
	newEntry := txeh.HostFileLine{
		OriginalLineNum: last,
		LineType:        30,
		Address:         "10.10.10.20",
		Hostnames:       []string{"host.network"},
		Comment:         "#netmaker-1",
	}
	*lines = append(*lines, newEntry)
	log.Println(hosts.RenderHostsFile())
	pretty.Println(lines)

	//hosts.Save()
	hosts.SaveAs("./test.hosts")
}
