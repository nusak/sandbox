package main

import (
	"log"

	"github.com/gravitl/netclient/ncutils"
	"github.com/gravitl/netclient/wireguard"
	"github.com/kr/pretty"
)

func main() {
	iface := wireguard.GetInterface()
	pretty.Println(iface)
	if iface.Name != ncutils.GetInterfaceName() {
		log.Fatal("nil interface")
	}
	log.Println("interface exists")
}
