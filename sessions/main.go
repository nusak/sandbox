package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/memstore"
	"github.com/gin-gonic/gin"
)

type User struct {
	Username string `json:"user"`
	Password string `json:"pass"`
}

func main() {
	router := SetupRouter()
	router.Run("127.0.0.1:8090")
}

func SetupRouter() *gin.Engine {
	store := memstore.NewStore([]byte("secret"))
	session := sessions.Sessions("golfballs", store)
	_ = sessions.Options{MaxAge: 300}

	router := gin.Default()
	router.Use(session)
	router.POST("/user", Login)
	router.GET("/", hello)

	return router
}

func hello(c *gin.Context) {
	session := sessions.Default(c)
	log.Println("session", session)
	if session.Get("loggedIn") == nil {
		c.JSON(http.StatusUnauthorized, gin.H{"message": "unauthorized"})
		return
	}
	fmt.Println(session.Get("rand"))
	session.Set("rand", rand.Float64())
	session.Set("message", "hello")
	session.Save()
	c.JSON(http.StatusOK, gin.H{"message": "hello"})
}

func Login(c *gin.Context) {
	var user User
	if err := c.BindJSON(&user); err != nil {
		log.Println("bind json err", err)
		c.JSON(http.StatusBadRequest, gin.H{"message": "Invalid data"})
		return
	}
	session := sessions.Default(c)
	session.Set("loggedIn", true)
	session.Set("message", "")
	sessions.Default(c).Options(sessions.Options{MaxAge: 300, Secure: true, SameSite: http.SameSiteStrictMode})
	session.Save()
	c.JSON(http.StatusOK, gin.H{"message": "login successful"})
}
